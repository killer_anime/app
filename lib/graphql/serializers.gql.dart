import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart' show StandardJsonPlugin;
import 'package:gql_code_builder/src/serializers/operation_serializer.dart'
    show OperationSerializer;
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart'
    show
        GMediaDisplayData,
        GMediaDisplayData_coverImage,
        GMediaDisplayData_nextAiringEpisode,
        GMediaDisplayData_title;
import 'package:killer_anime/graphql/fragments/MediaDisplay.req.gql.dart'
    show GMediaDisplayReq;
import 'package:killer_anime/graphql/fragments/MediaDisplay.var.gql.dart'
    show GMediaDisplayVars;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.data.gql.dart'
    show GMediaExternalLinksData, GMediaExternalLinksData_externalLinks;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.req.gql.dart'
    show GMediaExternalLinksReq;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.var.gql.dart'
    show GMediaExternalLinksVars;
import 'package:killer_anime/graphql/fragments/MediaListProgress.data.gql.dart'
    show GMediaListProgressData;
import 'package:killer_anime/graphql/fragments/MediaListProgress.req.gql.dart'
    show GMediaListProgressReq;
import 'package:killer_anime/graphql/fragments/MediaListProgress.var.gql.dart'
    show GMediaListProgressVars;
import 'package:killer_anime/graphql/local/configuration.data.gql.dart'
    show GappConfigData;
import 'package:killer_anime/graphql/local/configuration.req.gql.dart'
    show GappConfigReq;
import 'package:killer_anime/graphql/local/configuration.var.gql.dart'
    show GappConfigVars;
import 'package:killer_anime/graphql/mutations/updateProgress.data.gql.dart'
    show GUpdateProgressData, GUpdateProgressData_SaveMediaListEntry;
import 'package:killer_anime/graphql/mutations/updateProgress.req.gql.dart'
    show GUpdateProgressReq;
import 'package:killer_anime/graphql/mutations/updateProgress.var.gql.dart'
    show GUpdateProgressVars;
import 'package:killer_anime/graphql/queries/getListId.data.gql.dart'
    show GGetListIdData, GGetListIdData_MediaList;
import 'package:killer_anime/graphql/queries/getListId.req.gql.dart'
    show GGetListIdReq;
import 'package:killer_anime/graphql/queries/getListId.var.gql.dart'
    show GGetListIdVars;
import 'package:killer_anime/graphql/queries/getPage.data.gql.dart'
    show
        GGetPageData,
        GGetPageData_Page,
        GGetPageData_Page_media,
        GGetPageData_Page_media_coverImage,
        GGetPageData_Page_media_nextAiringEpisode,
        GGetPageData_Page_media_title;
import 'package:killer_anime/graphql/queries/getPage.req.gql.dart'
    show GGetPageReq;
import 'package:killer_anime/graphql/queries/getPage.var.gql.dart'
    show GGetPageVars;
import 'package:killer_anime/graphql/queries/getRecent.data.gql.dart'
    show
        GGetRecentData,
        GGetRecentData_Page,
        GGetRecentData_Page_media,
        GGetRecentData_Page_media_coverImage,
        GGetRecentData_Page_media_nextAiringEpisode,
        GGetRecentData_Page_media_title;
import 'package:killer_anime/graphql/queries/getRecent.req.gql.dart'
    show GGetRecentReq;
import 'package:killer_anime/graphql/queries/getRecent.var.gql.dart'
    show GGetRecentVars;
import 'package:killer_anime/graphql/queries/getWatching.data.gql.dart'
    show
        GGetWatchingData,
        GGetWatchingData_MediaListCollection,
        GGetWatchingData_MediaListCollection_lists,
        GGetWatchingData_MediaListCollection_lists_entries,
        GGetWatchingData_MediaListCollection_lists_entries_media,
        GGetWatchingData_MediaListCollection_lists_entries_media_coverImage,
        GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode,
        GGetWatchingData_MediaListCollection_lists_entries_media_title,
        GGetWatchingData_MediaListCollection_user,
        GGetWatchingData_Viewer;
import 'package:killer_anime/graphql/queries/getWatching.req.gql.dart'
    show GGetWatchingReq;
import 'package:killer_anime/graphql/queries/getWatching.var.gql.dart'
    show GGetWatchingVars;
import 'package:killer_anime/graphql/schema.schema.gql.dart'
    show
        GActivitySort,
        GActivityType,
        GAiringScheduleInput,
        GAiringSort,
        GAniChartHighlightInput,
        GCharacterNameInput,
        GCharacterRole,
        GCharacterSort,
        GCountryCode,
        GFuzzyDateInput,
        GFuzzyDateInt,
        GJson,
        GLikeableType,
        GMediaExternalLinkInput,
        GMediaFormat,
        GMediaListOptionsInput,
        GMediaListSort,
        GMediaListStatus,
        GMediaRankType,
        GMediaRelation,
        GMediaSeason,
        GMediaSort,
        GMediaSource,
        GMediaStatus,
        GMediaTitleInput,
        GMediaTrendSort,
        GMediaType,
        GModActionType,
        GModRole,
        GNotificationOptionInput,
        GNotificationType,
        GRecommendationRating,
        GRecommendationSort,
        GReviewRating,
        GReviewSort,
        GRevisionHistoryAction,
        GScoreFormat,
        GSiteTrendSort,
        GStaffLanguage,
        GStaffNameInput,
        GStaffSort,
        GStudioSort,
        GSubmissionSort,
        GSubmissionStatus,
        GThreadCommentSort,
        GThreadSort,
        GUserSort,
        GUserStaffNameLanguage,
        GUserStatisticsSort,
        GUserTitleLanguage;

part 'serializers.gql.g.dart';

final SerializersBuilder _serializersBuilder = _$serializers.toBuilder()
  ..add(OperationSerializer())
  ..addPlugin(StandardJsonPlugin());
@SerializersFor([
  GActivitySort,
  GActivityType,
  GAiringScheduleInput,
  GAiringSort,
  GAniChartHighlightInput,
  GCharacterNameInput,
  GCharacterRole,
  GCharacterSort,
  GCountryCode,
  GFuzzyDateInput,
  GFuzzyDateInt,
  GGetListIdData,
  GGetListIdData_MediaList,
  GGetListIdReq,
  GGetListIdVars,
  GGetPageData,
  GGetPageData_Page,
  GGetPageData_Page_media,
  GGetPageData_Page_media_coverImage,
  GGetPageData_Page_media_nextAiringEpisode,
  GGetPageData_Page_media_title,
  GGetPageReq,
  GGetPageVars,
  GGetRecentData,
  GGetRecentData_Page,
  GGetRecentData_Page_media,
  GGetRecentData_Page_media_coverImage,
  GGetRecentData_Page_media_nextAiringEpisode,
  GGetRecentData_Page_media_title,
  GGetRecentReq,
  GGetRecentVars,
  GGetWatchingData,
  GGetWatchingData_MediaListCollection,
  GGetWatchingData_MediaListCollection_lists,
  GGetWatchingData_MediaListCollection_lists_entries,
  GGetWatchingData_MediaListCollection_lists_entries_media,
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImage,
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode,
  GGetWatchingData_MediaListCollection_lists_entries_media_title,
  GGetWatchingData_MediaListCollection_user,
  GGetWatchingData_Viewer,
  GGetWatchingReq,
  GGetWatchingVars,
  GJson,
  GLikeableType,
  GMediaDisplayData,
  GMediaDisplayData_coverImage,
  GMediaDisplayData_nextAiringEpisode,
  GMediaDisplayData_title,
  GMediaDisplayReq,
  GMediaDisplayVars,
  GMediaExternalLinkInput,
  GMediaExternalLinksData,
  GMediaExternalLinksData_externalLinks,
  GMediaExternalLinksReq,
  GMediaExternalLinksVars,
  GMediaFormat,
  GMediaListOptionsInput,
  GMediaListProgressData,
  GMediaListProgressReq,
  GMediaListProgressVars,
  GMediaListSort,
  GMediaListStatus,
  GMediaRankType,
  GMediaRelation,
  GMediaSeason,
  GMediaSort,
  GMediaSource,
  GMediaStatus,
  GMediaTitleInput,
  GMediaTrendSort,
  GMediaType,
  GModActionType,
  GModRole,
  GNotificationOptionInput,
  GNotificationType,
  GRecommendationRating,
  GRecommendationSort,
  GReviewRating,
  GReviewSort,
  GRevisionHistoryAction,
  GScoreFormat,
  GSiteTrendSort,
  GStaffLanguage,
  GStaffNameInput,
  GStaffSort,
  GStudioSort,
  GSubmissionSort,
  GSubmissionStatus,
  GThreadCommentSort,
  GThreadSort,
  GUpdateProgressData,
  GUpdateProgressData_SaveMediaListEntry,
  GUpdateProgressReq,
  GUpdateProgressVars,
  GUserSort,
  GUserStaffNameLanguage,
  GUserStatisticsSort,
  GUserTitleLanguage,
  GappConfigData,
  GappConfigReq,
  GappConfigVars
])
final Serializers serializers = _serializersBuilder.build();
