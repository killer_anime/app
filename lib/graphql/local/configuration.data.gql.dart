// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'configuration.data.gql.g.dart';

abstract class GappConfig {
  String get G__typename;
  String? get id;
  bool? get external;
  Map<String, dynamic> toJson();
}

abstract class GappConfigData
    implements Built<GappConfigData, GappConfigDataBuilder>, GappConfig {
  GappConfigData._();

  factory GappConfigData([Function(GappConfigDataBuilder b) updates]) =
      _$GappConfigData;

  static void _initializeBuilder(GappConfigDataBuilder b) =>
      b..G__typename = 'Configuration';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get id;
  bool? get external;
  static Serializer<GappConfigData> get serializer =>
      _$gappConfigDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GappConfigData.serializer, this)
          as Map<String, dynamic>);
  static GappConfigData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GappConfigData.serializer, json);
}
