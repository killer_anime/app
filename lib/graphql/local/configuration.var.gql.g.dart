// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'configuration.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GappConfigVars> _$gappConfigVarsSerializer =
    new _$GappConfigVarsSerializer();

class _$GappConfigVarsSerializer
    implements StructuredSerializer<GappConfigVars> {
  @override
  final Iterable<Type> types = const [GappConfigVars, _$GappConfigVars];
  @override
  final String wireName = 'GappConfigVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GappConfigVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GappConfigVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GappConfigVarsBuilder().build();
  }
}

class _$GappConfigVars extends GappConfigVars {
  factory _$GappConfigVars([void Function(GappConfigVarsBuilder)? updates]) =>
      (new GappConfigVarsBuilder()..update(updates)).build();

  _$GappConfigVars._() : super._();

  @override
  GappConfigVars rebuild(void Function(GappConfigVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GappConfigVarsBuilder toBuilder() =>
      new GappConfigVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GappConfigVars;
  }

  @override
  int get hashCode {
    return 469520578;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GappConfigVars').toString();
  }
}

class GappConfigVarsBuilder
    implements Builder<GappConfigVars, GappConfigVarsBuilder> {
  _$GappConfigVars? _$v;

  GappConfigVarsBuilder();

  @override
  void replace(GappConfigVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GappConfigVars;
  }

  @override
  void update(void Function(GappConfigVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GappConfigVars build() {
    final _$result = _$v ?? new _$GappConfigVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
