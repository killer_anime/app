// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'configuration.var.gql.g.dart';

abstract class GappConfigVars
    implements Built<GappConfigVars, GappConfigVarsBuilder> {
  GappConfigVars._();

  factory GappConfigVars([Function(GappConfigVarsBuilder b) updates]) =
      _$GappConfigVars;

  static Serializer<GappConfigVars> get serializer =>
      _$gappConfigVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GappConfigVars.serializer, this)
          as Map<String, dynamic>);
  static GappConfigVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GappConfigVars.serializer, json);
}
