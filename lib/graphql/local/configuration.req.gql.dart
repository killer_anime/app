// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;
import 'package:killer_anime/graphql/local/configuration.ast.gql.dart' as _i4;
import 'package:killer_anime/graphql/local/configuration.data.gql.dart' as _i2;
import 'package:killer_anime/graphql/local/configuration.var.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'configuration.req.gql.g.dart';

abstract class GappConfigReq
    implements
        Built<GappConfigReq, GappConfigReqBuilder>,
        _i1.FragmentRequest<_i2.GappConfigData, _i3.GappConfigVars> {
  GappConfigReq._();

  factory GappConfigReq([Function(GappConfigReqBuilder b) updates]) =
      _$GappConfigReq;

  static void _initializeBuilder(GappConfigReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'appConfig';
  _i3.GappConfigVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GappConfigData? parseData(Map<String, dynamic> json) =>
      _i2.GappConfigData.fromJson(json);
  static Serializer<GappConfigReq> get serializer => _$gappConfigReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GappConfigReq.serializer, this)
          as Map<String, dynamic>);
  static GappConfigReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GappConfigReq.serializer, json);
}
