// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;
import 'package:killer_anime/graphql/mutations/updateProgress.ast.gql.dart'
    as _i5;
import 'package:killer_anime/graphql/mutations/updateProgress.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/mutations/updateProgress.var.gql.dart'
    as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'updateProgress.req.gql.g.dart';

abstract class GUpdateProgressReq
    implements
        Built<GUpdateProgressReq, GUpdateProgressReqBuilder>,
        _i1.OperationRequest<_i2.GUpdateProgressData, _i3.GUpdateProgressVars> {
  GUpdateProgressReq._();

  factory GUpdateProgressReq([Function(GUpdateProgressReqBuilder b) updates]) =
      _$GUpdateProgressReq;

  static void _initializeBuilder(GUpdateProgressReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'UpdateProgress')
    ..executeOnListen = true;
  _i3.GUpdateProgressVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GUpdateProgressData? Function(
      _i2.GUpdateProgressData?, _i2.GUpdateProgressData?)? get updateResult;
  _i2.GUpdateProgressData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GUpdateProgressData? parseData(Map<String, dynamic> json) =>
      _i2.GUpdateProgressData.fromJson(json);
  static Serializer<GUpdateProgressReq> get serializer =>
      _$gUpdateProgressReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GUpdateProgressReq.serializer, this)
          as Map<String, dynamic>);
  static GUpdateProgressReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GUpdateProgressReq.serializer, json);
}
