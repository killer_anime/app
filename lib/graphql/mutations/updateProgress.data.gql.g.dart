// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'updateProgress.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GUpdateProgressData> _$gUpdateProgressDataSerializer =
    new _$GUpdateProgressDataSerializer();
Serializer<GUpdateProgressData_SaveMediaListEntry>
    _$gUpdateProgressDataSaveMediaListEntrySerializer =
    new _$GUpdateProgressData_SaveMediaListEntrySerializer();

class _$GUpdateProgressDataSerializer
    implements StructuredSerializer<GUpdateProgressData> {
  @override
  final Iterable<Type> types = const [
    GUpdateProgressData,
    _$GUpdateProgressData
  ];
  @override
  final String wireName = 'GUpdateProgressData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GUpdateProgressData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.SaveMediaListEntry;
    if (value != null) {
      result
        ..add('SaveMediaListEntry')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GUpdateProgressData_SaveMediaListEntry)));
    }
    return result;
  }

  @override
  GUpdateProgressData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GUpdateProgressDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SaveMediaListEntry':
          result.SaveMediaListEntry.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GUpdateProgressData_SaveMediaListEntry))!
              as GUpdateProgressData_SaveMediaListEntry);
          break;
      }
    }

    return result.build();
  }
}

class _$GUpdateProgressData_SaveMediaListEntrySerializer
    implements StructuredSerializer<GUpdateProgressData_SaveMediaListEntry> {
  @override
  final Iterable<Type> types = const [
    GUpdateProgressData_SaveMediaListEntry,
    _$GUpdateProgressData_SaveMediaListEntry
  ];
  @override
  final String wireName = 'GUpdateProgressData_SaveMediaListEntry';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GUpdateProgressData_SaveMediaListEntry object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.progress;
    if (value != null) {
      result
        ..add('progress')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.GMediaListStatus)));
    }
    return result;
  }

  @override
  GUpdateProgressData_SaveMediaListEntry deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GUpdateProgressData_SaveMediaListEntryBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'progress':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i2.GMediaListStatus))
              as _i2.GMediaListStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$GUpdateProgressData extends GUpdateProgressData {
  @override
  final String G__typename;
  @override
  final GUpdateProgressData_SaveMediaListEntry? SaveMediaListEntry;

  factory _$GUpdateProgressData(
          [void Function(GUpdateProgressDataBuilder)? updates]) =>
      (new GUpdateProgressDataBuilder()..update(updates)).build();

  _$GUpdateProgressData._({required this.G__typename, this.SaveMediaListEntry})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GUpdateProgressData', 'G__typename');
  }

  @override
  GUpdateProgressData rebuild(
          void Function(GUpdateProgressDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GUpdateProgressDataBuilder toBuilder() =>
      new GUpdateProgressDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GUpdateProgressData &&
        G__typename == other.G__typename &&
        SaveMediaListEntry == other.SaveMediaListEntry;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), SaveMediaListEntry.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GUpdateProgressData')
          ..add('G__typename', G__typename)
          ..add('SaveMediaListEntry', SaveMediaListEntry))
        .toString();
  }
}

class GUpdateProgressDataBuilder
    implements Builder<GUpdateProgressData, GUpdateProgressDataBuilder> {
  _$GUpdateProgressData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GUpdateProgressData_SaveMediaListEntryBuilder? _SaveMediaListEntry;
  GUpdateProgressData_SaveMediaListEntryBuilder get SaveMediaListEntry =>
      _$this._SaveMediaListEntry ??=
          new GUpdateProgressData_SaveMediaListEntryBuilder();
  set SaveMediaListEntry(
          GUpdateProgressData_SaveMediaListEntryBuilder? SaveMediaListEntry) =>
      _$this._SaveMediaListEntry = SaveMediaListEntry;

  GUpdateProgressDataBuilder() {
    GUpdateProgressData._initializeBuilder(this);
  }

  GUpdateProgressDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _SaveMediaListEntry = $v.SaveMediaListEntry?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GUpdateProgressData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GUpdateProgressData;
  }

  @override
  void update(void Function(GUpdateProgressDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GUpdateProgressData build() {
    _$GUpdateProgressData _$result;
    try {
      _$result = _$v ??
          new _$GUpdateProgressData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GUpdateProgressData', 'G__typename'),
              SaveMediaListEntry: _SaveMediaListEntry?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'SaveMediaListEntry';
        _SaveMediaListEntry?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GUpdateProgressData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GUpdateProgressData_SaveMediaListEntry
    extends GUpdateProgressData_SaveMediaListEntry {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final int? progress;
  @override
  final _i2.GMediaListStatus? status;

  factory _$GUpdateProgressData_SaveMediaListEntry(
          [void Function(GUpdateProgressData_SaveMediaListEntryBuilder)?
              updates]) =>
      (new GUpdateProgressData_SaveMediaListEntryBuilder()..update(updates))
          .build();

  _$GUpdateProgressData_SaveMediaListEntry._(
      {required this.G__typename, required this.id, this.progress, this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GUpdateProgressData_SaveMediaListEntry', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GUpdateProgressData_SaveMediaListEntry', 'id');
  }

  @override
  GUpdateProgressData_SaveMediaListEntry rebuild(
          void Function(GUpdateProgressData_SaveMediaListEntryBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GUpdateProgressData_SaveMediaListEntryBuilder toBuilder() =>
      new GUpdateProgressData_SaveMediaListEntryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GUpdateProgressData_SaveMediaListEntry &&
        G__typename == other.G__typename &&
        id == other.id &&
        progress == other.progress &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), progress.hashCode),
        status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GUpdateProgressData_SaveMediaListEntry')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('progress', progress)
          ..add('status', status))
        .toString();
  }
}

class GUpdateProgressData_SaveMediaListEntryBuilder
    implements
        Builder<GUpdateProgressData_SaveMediaListEntry,
            GUpdateProgressData_SaveMediaListEntryBuilder> {
  _$GUpdateProgressData_SaveMediaListEntry? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _progress;
  int? get progress => _$this._progress;
  set progress(int? progress) => _$this._progress = progress;

  _i2.GMediaListStatus? _status;
  _i2.GMediaListStatus? get status => _$this._status;
  set status(_i2.GMediaListStatus? status) => _$this._status = status;

  GUpdateProgressData_SaveMediaListEntryBuilder() {
    GUpdateProgressData_SaveMediaListEntry._initializeBuilder(this);
  }

  GUpdateProgressData_SaveMediaListEntryBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _progress = $v.progress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GUpdateProgressData_SaveMediaListEntry other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GUpdateProgressData_SaveMediaListEntry;
  }

  @override
  void update(
      void Function(GUpdateProgressData_SaveMediaListEntryBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GUpdateProgressData_SaveMediaListEntry build() {
    final _$result = _$v ??
        new _$GUpdateProgressData_SaveMediaListEntry._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GUpdateProgressData_SaveMediaListEntry', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GUpdateProgressData_SaveMediaListEntry', 'id'),
            progress: progress,
            status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
