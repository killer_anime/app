// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'updateProgress.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GUpdateProgressVars> _$gUpdateProgressVarsSerializer =
    new _$GUpdateProgressVarsSerializer();

class _$GUpdateProgressVarsSerializer
    implements StructuredSerializer<GUpdateProgressVars> {
  @override
  final Iterable<Type> types = const [
    GUpdateProgressVars,
    _$GUpdateProgressVars
  ];
  @override
  final String wireName = 'GUpdateProgressVars';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GUpdateProgressVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'progress',
      serializers.serialize(object.progress,
          specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i1.GMediaListStatus)));
    }
    return result;
  }

  @override
  GUpdateProgressVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GUpdateProgressVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'progress':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i1.GMediaListStatus))
              as _i1.GMediaListStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$GUpdateProgressVars extends GUpdateProgressVars {
  @override
  final int id;
  @override
  final int progress;
  @override
  final _i1.GMediaListStatus? status;

  factory _$GUpdateProgressVars(
          [void Function(GUpdateProgressVarsBuilder)? updates]) =>
      (new GUpdateProgressVarsBuilder()..update(updates)).build();

  _$GUpdateProgressVars._(
      {required this.id, required this.progress, this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'GUpdateProgressVars', 'id');
    BuiltValueNullFieldError.checkNotNull(
        progress, 'GUpdateProgressVars', 'progress');
  }

  @override
  GUpdateProgressVars rebuild(
          void Function(GUpdateProgressVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GUpdateProgressVarsBuilder toBuilder() =>
      new GUpdateProgressVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GUpdateProgressVars &&
        id == other.id &&
        progress == other.progress &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, id.hashCode), progress.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GUpdateProgressVars')
          ..add('id', id)
          ..add('progress', progress)
          ..add('status', status))
        .toString();
  }
}

class GUpdateProgressVarsBuilder
    implements Builder<GUpdateProgressVars, GUpdateProgressVarsBuilder> {
  _$GUpdateProgressVars? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _progress;
  int? get progress => _$this._progress;
  set progress(int? progress) => _$this._progress = progress;

  _i1.GMediaListStatus? _status;
  _i1.GMediaListStatus? get status => _$this._status;
  set status(_i1.GMediaListStatus? status) => _$this._status = status;

  GUpdateProgressVarsBuilder();

  GUpdateProgressVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _progress = $v.progress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GUpdateProgressVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GUpdateProgressVars;
  }

  @override
  void update(void Function(GUpdateProgressVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GUpdateProgressVars build() {
    final _$result = _$v ??
        new _$GUpdateProgressVars._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GUpdateProgressVars', 'id'),
            progress: BuiltValueNullFieldError.checkNotNull(
                progress, 'GUpdateProgressVars', 'progress'),
            status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
