// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i1;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i2;

part 'updateProgress.var.gql.g.dart';

abstract class GUpdateProgressVars
    implements Built<GUpdateProgressVars, GUpdateProgressVarsBuilder> {
  GUpdateProgressVars._();

  factory GUpdateProgressVars(
      [Function(GUpdateProgressVarsBuilder b) updates]) = _$GUpdateProgressVars;

  int get id;
  int get progress;
  _i1.GMediaListStatus? get status;
  static Serializer<GUpdateProgressVars> get serializer =>
      _$gUpdateProgressVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GUpdateProgressVars.serializer, this)
          as Map<String, dynamic>);
  static GUpdateProgressVars? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GUpdateProgressVars.serializer, json);
}
