// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i2;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'updateProgress.data.gql.g.dart';

abstract class GUpdateProgressData
    implements Built<GUpdateProgressData, GUpdateProgressDataBuilder> {
  GUpdateProgressData._();

  factory GUpdateProgressData(
      [Function(GUpdateProgressDataBuilder b) updates]) = _$GUpdateProgressData;

  static void _initializeBuilder(GUpdateProgressDataBuilder b) =>
      b..G__typename = 'Mutation';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GUpdateProgressData_SaveMediaListEntry? get SaveMediaListEntry;
  static Serializer<GUpdateProgressData> get serializer =>
      _$gUpdateProgressDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GUpdateProgressData.serializer, this)
          as Map<String, dynamic>);
  static GUpdateProgressData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GUpdateProgressData.serializer, json);
}

abstract class GUpdateProgressData_SaveMediaListEntry
    implements
        Built<GUpdateProgressData_SaveMediaListEntry,
            GUpdateProgressData_SaveMediaListEntryBuilder> {
  GUpdateProgressData_SaveMediaListEntry._();

  factory GUpdateProgressData_SaveMediaListEntry(
          [Function(GUpdateProgressData_SaveMediaListEntryBuilder b) updates]) =
      _$GUpdateProgressData_SaveMediaListEntry;

  static void _initializeBuilder(
          GUpdateProgressData_SaveMediaListEntryBuilder b) =>
      b..G__typename = 'MediaList';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  int? get progress;
  _i2.GMediaListStatus? get status;
  static Serializer<GUpdateProgressData_SaveMediaListEntry> get serializer =>
      _$gUpdateProgressDataSaveMediaListEntrySerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GUpdateProgressData_SaveMediaListEntry.serializer, this)
      as Map<String, dynamic>);
  static GUpdateProgressData_SaveMediaListEntry? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GUpdateProgressData_SaveMediaListEntry.serializer, json);
}
