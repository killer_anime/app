// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getRecent.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetRecentData> _$gGetRecentDataSerializer =
    new _$GGetRecentDataSerializer();
Serializer<GGetRecentData_Page> _$gGetRecentDataPageSerializer =
    new _$GGetRecentData_PageSerializer();
Serializer<GGetRecentData_Page_media> _$gGetRecentDataPageMediaSerializer =
    new _$GGetRecentData_Page_mediaSerializer();
Serializer<GGetRecentData_Page_media_coverImage>
    _$gGetRecentDataPageMediaCoverImageSerializer =
    new _$GGetRecentData_Page_media_coverImageSerializer();
Serializer<GGetRecentData_Page_media_nextAiringEpisode>
    _$gGetRecentDataPageMediaNextAiringEpisodeSerializer =
    new _$GGetRecentData_Page_media_nextAiringEpisodeSerializer();
Serializer<GGetRecentData_Page_media_title>
    _$gGetRecentDataPageMediaTitleSerializer =
    new _$GGetRecentData_Page_media_titleSerializer();

class _$GGetRecentDataSerializer
    implements StructuredSerializer<GGetRecentData> {
  @override
  final Iterable<Type> types = const [GGetRecentData, _$GGetRecentData];
  @override
  final String wireName = 'GGetRecentData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetRecentData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.Page;
    if (value != null) {
      result
        ..add('Page')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GGetRecentData_Page)));
    }
    return result;
  }

  @override
  GGetRecentData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'Page':
          result.Page.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GGetRecentData_Page))!
              as GGetRecentData_Page);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData_PageSerializer
    implements StructuredSerializer<GGetRecentData_Page> {
  @override
  final Iterable<Type> types = const [
    GGetRecentData_Page,
    _$GGetRecentData_Page
  ];
  @override
  final String wireName = 'GGetRecentData_Page';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetRecentData_Page object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.media;
    if (value != null) {
      result
        ..add('media')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(GGetRecentData_Page_media)])));
    }
    return result;
  }

  @override
  GGetRecentData_Page deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentData_PageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'media':
          result.media.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GGetRecentData_Page_media)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData_Page_mediaSerializer
    implements StructuredSerializer<GGetRecentData_Page_media> {
  @override
  final Iterable<Type> types = const [
    GGetRecentData_Page_media,
    _$GGetRecentData_Page_media
  ];
  @override
  final String wireName = 'GGetRecentData_Page_media';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetRecentData_Page_media object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.coverImage;
    if (value != null) {
      result
        ..add('coverImage')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GGetRecentData_Page_media_coverImage)));
    }
    value = object.episodes;
    if (value != null) {
      result
        ..add('episodes')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.nextAiringEpisode;
    if (value != null) {
      result
        ..add('nextAiringEpisode')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GGetRecentData_Page_media_nextAiringEpisode)));
    }
    value = object.synonyms;
    if (value != null) {
      result
        ..add('synonyms')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i3.GMediaStatus)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GGetRecentData_Page_media_title)));
    }
    return result;
  }

  @override
  GGetRecentData_Page_media deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentData_Page_mediaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverImage':
          result.coverImage.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GGetRecentData_Page_media_coverImage))!
              as GGetRecentData_Page_media_coverImage);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'episodes':
          result.episodes = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'nextAiringEpisode':
          result.nextAiringEpisode.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetRecentData_Page_media_nextAiringEpisode))!
              as GGetRecentData_Page_media_nextAiringEpisode);
          break;
        case 'synonyms':
          result.synonyms.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i3.GMediaStatus))
              as _i3.GMediaStatus?;
          break;
        case 'title':
          result.title.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GGetRecentData_Page_media_title))!
              as GGetRecentData_Page_media_title);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData_Page_media_coverImageSerializer
    implements StructuredSerializer<GGetRecentData_Page_media_coverImage> {
  @override
  final Iterable<Type> types = const [
    GGetRecentData_Page_media_coverImage,
    _$GGetRecentData_Page_media_coverImage
  ];
  @override
  final String wireName = 'GGetRecentData_Page_media_coverImage';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetRecentData_Page_media_coverImage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.extraLarge;
    if (value != null) {
      result
        ..add('extraLarge')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.large;
    if (value != null) {
      result
        ..add('large')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.color;
    if (value != null) {
      result
        ..add('color')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetRecentData_Page_media_coverImage deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentData_Page_media_coverImageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'extraLarge':
          result.extraLarge = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'large':
          result.large = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'color':
          result.color = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData_Page_media_nextAiringEpisodeSerializer
    implements
        StructuredSerializer<GGetRecentData_Page_media_nextAiringEpisode> {
  @override
  final Iterable<Type> types = const [
    GGetRecentData_Page_media_nextAiringEpisode,
    _$GGetRecentData_Page_media_nextAiringEpisode
  ];
  @override
  final String wireName = 'GGetRecentData_Page_media_nextAiringEpisode';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GGetRecentData_Page_media_nextAiringEpisode object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'episode',
      serializers.serialize(object.episode, specifiedType: const FullType(int)),
      'timeUntilAiring',
      serializers.serialize(object.timeUntilAiring,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  GGetRecentData_Page_media_nextAiringEpisode deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentData_Page_media_nextAiringEpisodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'episode':
          result.episode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'timeUntilAiring':
          result.timeUntilAiring = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData_Page_media_titleSerializer
    implements StructuredSerializer<GGetRecentData_Page_media_title> {
  @override
  final Iterable<Type> types = const [
    GGetRecentData_Page_media_title,
    _$GGetRecentData_Page_media_title
  ];
  @override
  final String wireName = 'GGetRecentData_Page_media_title';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetRecentData_Page_media_title object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.userPreferred;
    if (value != null) {
      result
        ..add('userPreferred')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.romaji;
    if (value != null) {
      result
        ..add('romaji')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.english;
    if (value != null) {
      result
        ..add('english')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetRecentData_Page_media_title deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentData_Page_media_titleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userPreferred':
          result.userPreferred = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'romaji':
          result.romaji = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'english':
          result.english = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentData extends GGetRecentData {
  @override
  final String G__typename;
  @override
  final GGetRecentData_Page? Page;

  factory _$GGetRecentData([void Function(GGetRecentDataBuilder)? updates]) =>
      (new GGetRecentDataBuilder()..update(updates)).build();

  _$GGetRecentData._({required this.G__typename, this.Page}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetRecentData', 'G__typename');
  }

  @override
  GGetRecentData rebuild(void Function(GGetRecentDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentDataBuilder toBuilder() =>
      new GGetRecentDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData &&
        G__typename == other.G__typename &&
        Page == other.Page;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), Page.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentData')
          ..add('G__typename', G__typename)
          ..add('Page', Page))
        .toString();
  }
}

class GGetRecentDataBuilder
    implements Builder<GGetRecentData, GGetRecentDataBuilder> {
  _$GGetRecentData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetRecentData_PageBuilder? _Page;
  GGetRecentData_PageBuilder get Page =>
      _$this._Page ??= new GGetRecentData_PageBuilder();
  set Page(GGetRecentData_PageBuilder? Page) => _$this._Page = Page;

  GGetRecentDataBuilder() {
    GGetRecentData._initializeBuilder(this);
  }

  GGetRecentDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _Page = $v.Page?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData;
  }

  @override
  void update(void Function(GGetRecentDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData build() {
    _$GGetRecentData _$result;
    try {
      _$result = _$v ??
          new _$GGetRecentData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GGetRecentData', 'G__typename'),
              Page: _Page?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'Page';
        _Page?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetRecentData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetRecentData_Page extends GGetRecentData_Page {
  @override
  final String G__typename;
  @override
  final BuiltList<GGetRecentData_Page_media>? media;

  factory _$GGetRecentData_Page(
          [void Function(GGetRecentData_PageBuilder)? updates]) =>
      (new GGetRecentData_PageBuilder()..update(updates)).build();

  _$GGetRecentData_Page._({required this.G__typename, this.media}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetRecentData_Page', 'G__typename');
  }

  @override
  GGetRecentData_Page rebuild(
          void Function(GGetRecentData_PageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentData_PageBuilder toBuilder() =>
      new GGetRecentData_PageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData_Page &&
        G__typename == other.G__typename &&
        media == other.media;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), media.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentData_Page')
          ..add('G__typename', G__typename)
          ..add('media', media))
        .toString();
  }
}

class GGetRecentData_PageBuilder
    implements Builder<GGetRecentData_Page, GGetRecentData_PageBuilder> {
  _$GGetRecentData_Page? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  ListBuilder<GGetRecentData_Page_media>? _media;
  ListBuilder<GGetRecentData_Page_media> get media =>
      _$this._media ??= new ListBuilder<GGetRecentData_Page_media>();
  set media(ListBuilder<GGetRecentData_Page_media>? media) =>
      _$this._media = media;

  GGetRecentData_PageBuilder() {
    GGetRecentData_Page._initializeBuilder(this);
  }

  GGetRecentData_PageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _media = $v.media?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData_Page other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData_Page;
  }

  @override
  void update(void Function(GGetRecentData_PageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData_Page build() {
    _$GGetRecentData_Page _$result;
    try {
      _$result = _$v ??
          new _$GGetRecentData_Page._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GGetRecentData_Page', 'G__typename'),
              media: _media?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'media';
        _media?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetRecentData_Page', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetRecentData_Page_media extends GGetRecentData_Page_media {
  @override
  final String G__typename;
  @override
  final GGetRecentData_Page_media_coverImage? coverImage;
  @override
  final int id;
  @override
  final int? episodes;
  @override
  final GGetRecentData_Page_media_nextAiringEpisode? nextAiringEpisode;
  @override
  final BuiltList<String>? synonyms;
  @override
  final _i3.GMediaStatus? status;
  @override
  final GGetRecentData_Page_media_title? title;

  factory _$GGetRecentData_Page_media(
          [void Function(GGetRecentData_Page_mediaBuilder)? updates]) =>
      (new GGetRecentData_Page_mediaBuilder()..update(updates)).build();

  _$GGetRecentData_Page_media._(
      {required this.G__typename,
      this.coverImage,
      required this.id,
      this.episodes,
      this.nextAiringEpisode,
      this.synonyms,
      this.status,
      this.title})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetRecentData_Page_media', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GGetRecentData_Page_media', 'id');
  }

  @override
  GGetRecentData_Page_media rebuild(
          void Function(GGetRecentData_Page_mediaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentData_Page_mediaBuilder toBuilder() =>
      new GGetRecentData_Page_mediaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData_Page_media &&
        G__typename == other.G__typename &&
        coverImage == other.coverImage &&
        id == other.id &&
        episodes == other.episodes &&
        nextAiringEpisode == other.nextAiringEpisode &&
        synonyms == other.synonyms &&
        status == other.status &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc(0, G__typename.hashCode),
                                coverImage.hashCode),
                            id.hashCode),
                        episodes.hashCode),
                    nextAiringEpisode.hashCode),
                synonyms.hashCode),
            status.hashCode),
        title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentData_Page_media')
          ..add('G__typename', G__typename)
          ..add('coverImage', coverImage)
          ..add('id', id)
          ..add('episodes', episodes)
          ..add('nextAiringEpisode', nextAiringEpisode)
          ..add('synonyms', synonyms)
          ..add('status', status)
          ..add('title', title))
        .toString();
  }
}

class GGetRecentData_Page_mediaBuilder
    implements
        Builder<GGetRecentData_Page_media, GGetRecentData_Page_mediaBuilder> {
  _$GGetRecentData_Page_media? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetRecentData_Page_media_coverImageBuilder? _coverImage;
  GGetRecentData_Page_media_coverImageBuilder get coverImage =>
      _$this._coverImage ??= new GGetRecentData_Page_media_coverImageBuilder();
  set coverImage(GGetRecentData_Page_media_coverImageBuilder? coverImage) =>
      _$this._coverImage = coverImage;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _episodes;
  int? get episodes => _$this._episodes;
  set episodes(int? episodes) => _$this._episodes = episodes;

  GGetRecentData_Page_media_nextAiringEpisodeBuilder? _nextAiringEpisode;
  GGetRecentData_Page_media_nextAiringEpisodeBuilder get nextAiringEpisode =>
      _$this._nextAiringEpisode ??=
          new GGetRecentData_Page_media_nextAiringEpisodeBuilder();
  set nextAiringEpisode(
          GGetRecentData_Page_media_nextAiringEpisodeBuilder?
              nextAiringEpisode) =>
      _$this._nextAiringEpisode = nextAiringEpisode;

  ListBuilder<String>? _synonyms;
  ListBuilder<String> get synonyms =>
      _$this._synonyms ??= new ListBuilder<String>();
  set synonyms(ListBuilder<String>? synonyms) => _$this._synonyms = synonyms;

  _i3.GMediaStatus? _status;
  _i3.GMediaStatus? get status => _$this._status;
  set status(_i3.GMediaStatus? status) => _$this._status = status;

  GGetRecentData_Page_media_titleBuilder? _title;
  GGetRecentData_Page_media_titleBuilder get title =>
      _$this._title ??= new GGetRecentData_Page_media_titleBuilder();
  set title(GGetRecentData_Page_media_titleBuilder? title) =>
      _$this._title = title;

  GGetRecentData_Page_mediaBuilder() {
    GGetRecentData_Page_media._initializeBuilder(this);
  }

  GGetRecentData_Page_mediaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _coverImage = $v.coverImage?.toBuilder();
      _id = $v.id;
      _episodes = $v.episodes;
      _nextAiringEpisode = $v.nextAiringEpisode?.toBuilder();
      _synonyms = $v.synonyms?.toBuilder();
      _status = $v.status;
      _title = $v.title?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData_Page_media other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData_Page_media;
  }

  @override
  void update(void Function(GGetRecentData_Page_mediaBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData_Page_media build() {
    _$GGetRecentData_Page_media _$result;
    try {
      _$result = _$v ??
          new _$GGetRecentData_Page_media._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GGetRecentData_Page_media', 'G__typename'),
              coverImage: _coverImage?.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GGetRecentData_Page_media', 'id'),
              episodes: episodes,
              nextAiringEpisode: _nextAiringEpisode?.build(),
              synonyms: _synonyms?.build(),
              status: status,
              title: _title?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'coverImage';
        _coverImage?.build();

        _$failedField = 'nextAiringEpisode';
        _nextAiringEpisode?.build();
        _$failedField = 'synonyms';
        _synonyms?.build();

        _$failedField = 'title';
        _title?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetRecentData_Page_media', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetRecentData_Page_media_coverImage
    extends GGetRecentData_Page_media_coverImage {
  @override
  final String G__typename;
  @override
  final String? extraLarge;
  @override
  final String? large;
  @override
  final String? color;

  factory _$GGetRecentData_Page_media_coverImage(
          [void Function(GGetRecentData_Page_media_coverImageBuilder)?
              updates]) =>
      (new GGetRecentData_Page_media_coverImageBuilder()..update(updates))
          .build();

  _$GGetRecentData_Page_media_coverImage._(
      {required this.G__typename, this.extraLarge, this.large, this.color})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetRecentData_Page_media_coverImage', 'G__typename');
  }

  @override
  GGetRecentData_Page_media_coverImage rebuild(
          void Function(GGetRecentData_Page_media_coverImageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentData_Page_media_coverImageBuilder toBuilder() =>
      new GGetRecentData_Page_media_coverImageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData_Page_media_coverImage &&
        G__typename == other.G__typename &&
        extraLarge == other.extraLarge &&
        large == other.large &&
        color == other.color;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), extraLarge.hashCode),
            large.hashCode),
        color.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentData_Page_media_coverImage')
          ..add('G__typename', G__typename)
          ..add('extraLarge', extraLarge)
          ..add('large', large)
          ..add('color', color))
        .toString();
  }
}

class GGetRecentData_Page_media_coverImageBuilder
    implements
        Builder<GGetRecentData_Page_media_coverImage,
            GGetRecentData_Page_media_coverImageBuilder> {
  _$GGetRecentData_Page_media_coverImage? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _extraLarge;
  String? get extraLarge => _$this._extraLarge;
  set extraLarge(String? extraLarge) => _$this._extraLarge = extraLarge;

  String? _large;
  String? get large => _$this._large;
  set large(String? large) => _$this._large = large;

  String? _color;
  String? get color => _$this._color;
  set color(String? color) => _$this._color = color;

  GGetRecentData_Page_media_coverImageBuilder() {
    GGetRecentData_Page_media_coverImage._initializeBuilder(this);
  }

  GGetRecentData_Page_media_coverImageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _extraLarge = $v.extraLarge;
      _large = $v.large;
      _color = $v.color;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData_Page_media_coverImage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData_Page_media_coverImage;
  }

  @override
  void update(
      void Function(GGetRecentData_Page_media_coverImageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData_Page_media_coverImage build() {
    final _$result = _$v ??
        new _$GGetRecentData_Page_media_coverImage._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GGetRecentData_Page_media_coverImage', 'G__typename'),
            extraLarge: extraLarge,
            large: large,
            color: color);
    replace(_$result);
    return _$result;
  }
}

class _$GGetRecentData_Page_media_nextAiringEpisode
    extends GGetRecentData_Page_media_nextAiringEpisode {
  @override
  final String G__typename;
  @override
  final int episode;
  @override
  final int timeUntilAiring;

  factory _$GGetRecentData_Page_media_nextAiringEpisode(
          [void Function(GGetRecentData_Page_media_nextAiringEpisodeBuilder)?
              updates]) =>
      (new GGetRecentData_Page_media_nextAiringEpisodeBuilder()
            ..update(updates))
          .build();

  _$GGetRecentData_Page_media_nextAiringEpisode._(
      {required this.G__typename,
      required this.episode,
      required this.timeUntilAiring})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GGetRecentData_Page_media_nextAiringEpisode', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        episode, 'GGetRecentData_Page_media_nextAiringEpisode', 'episode');
    BuiltValueNullFieldError.checkNotNull(timeUntilAiring,
        'GGetRecentData_Page_media_nextAiringEpisode', 'timeUntilAiring');
  }

  @override
  GGetRecentData_Page_media_nextAiringEpisode rebuild(
          void Function(GGetRecentData_Page_media_nextAiringEpisodeBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentData_Page_media_nextAiringEpisodeBuilder toBuilder() =>
      new GGetRecentData_Page_media_nextAiringEpisodeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData_Page_media_nextAiringEpisode &&
        G__typename == other.G__typename &&
        episode == other.episode &&
        timeUntilAiring == other.timeUntilAiring;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, G__typename.hashCode), episode.hashCode),
        timeUntilAiring.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetRecentData_Page_media_nextAiringEpisode')
          ..add('G__typename', G__typename)
          ..add('episode', episode)
          ..add('timeUntilAiring', timeUntilAiring))
        .toString();
  }
}

class GGetRecentData_Page_media_nextAiringEpisodeBuilder
    implements
        Builder<GGetRecentData_Page_media_nextAiringEpisode,
            GGetRecentData_Page_media_nextAiringEpisodeBuilder> {
  _$GGetRecentData_Page_media_nextAiringEpisode? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _episode;
  int? get episode => _$this._episode;
  set episode(int? episode) => _$this._episode = episode;

  int? _timeUntilAiring;
  int? get timeUntilAiring => _$this._timeUntilAiring;
  set timeUntilAiring(int? timeUntilAiring) =>
      _$this._timeUntilAiring = timeUntilAiring;

  GGetRecentData_Page_media_nextAiringEpisodeBuilder() {
    GGetRecentData_Page_media_nextAiringEpisode._initializeBuilder(this);
  }

  GGetRecentData_Page_media_nextAiringEpisodeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _episode = $v.episode;
      _timeUntilAiring = $v.timeUntilAiring;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData_Page_media_nextAiringEpisode other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData_Page_media_nextAiringEpisode;
  }

  @override
  void update(
      void Function(GGetRecentData_Page_media_nextAiringEpisodeBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData_Page_media_nextAiringEpisode build() {
    final _$result = _$v ??
        new _$GGetRecentData_Page_media_nextAiringEpisode._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GGetRecentData_Page_media_nextAiringEpisode', 'G__typename'),
            episode: BuiltValueNullFieldError.checkNotNull(episode,
                'GGetRecentData_Page_media_nextAiringEpisode', 'episode'),
            timeUntilAiring: BuiltValueNullFieldError.checkNotNull(
                timeUntilAiring,
                'GGetRecentData_Page_media_nextAiringEpisode',
                'timeUntilAiring'));
    replace(_$result);
    return _$result;
  }
}

class _$GGetRecentData_Page_media_title
    extends GGetRecentData_Page_media_title {
  @override
  final String G__typename;
  @override
  final String? userPreferred;
  @override
  final String? romaji;
  @override
  final String? english;

  factory _$GGetRecentData_Page_media_title(
          [void Function(GGetRecentData_Page_media_titleBuilder)? updates]) =>
      (new GGetRecentData_Page_media_titleBuilder()..update(updates)).build();

  _$GGetRecentData_Page_media_title._(
      {required this.G__typename,
      this.userPreferred,
      this.romaji,
      this.english})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetRecentData_Page_media_title', 'G__typename');
  }

  @override
  GGetRecentData_Page_media_title rebuild(
          void Function(GGetRecentData_Page_media_titleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentData_Page_media_titleBuilder toBuilder() =>
      new GGetRecentData_Page_media_titleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentData_Page_media_title &&
        G__typename == other.G__typename &&
        userPreferred == other.userPreferred &&
        romaji == other.romaji &&
        english == other.english;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), userPreferred.hashCode),
            romaji.hashCode),
        english.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentData_Page_media_title')
          ..add('G__typename', G__typename)
          ..add('userPreferred', userPreferred)
          ..add('romaji', romaji)
          ..add('english', english))
        .toString();
  }
}

class GGetRecentData_Page_media_titleBuilder
    implements
        Builder<GGetRecentData_Page_media_title,
            GGetRecentData_Page_media_titleBuilder> {
  _$GGetRecentData_Page_media_title? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _userPreferred;
  String? get userPreferred => _$this._userPreferred;
  set userPreferred(String? userPreferred) =>
      _$this._userPreferred = userPreferred;

  String? _romaji;
  String? get romaji => _$this._romaji;
  set romaji(String? romaji) => _$this._romaji = romaji;

  String? _english;
  String? get english => _$this._english;
  set english(String? english) => _$this._english = english;

  GGetRecentData_Page_media_titleBuilder() {
    GGetRecentData_Page_media_title._initializeBuilder(this);
  }

  GGetRecentData_Page_media_titleBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _userPreferred = $v.userPreferred;
      _romaji = $v.romaji;
      _english = $v.english;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentData_Page_media_title other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentData_Page_media_title;
  }

  @override
  void update(void Function(GGetRecentData_Page_media_titleBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentData_Page_media_title build() {
    final _$result = _$v ??
        new _$GGetRecentData_Page_media_title._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GGetRecentData_Page_media_title', 'G__typename'),
            userPreferred: userPreferred,
            romaji: romaji,
            english: english);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
