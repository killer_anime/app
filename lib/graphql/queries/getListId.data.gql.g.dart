// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getListId.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetListIdData> _$gGetListIdDataSerializer =
    new _$GGetListIdDataSerializer();
Serializer<GGetListIdData_MediaList> _$gGetListIdDataMediaListSerializer =
    new _$GGetListIdData_MediaListSerializer();

class _$GGetListIdDataSerializer
    implements StructuredSerializer<GGetListIdData> {
  @override
  final Iterable<Type> types = const [GGetListIdData, _$GGetListIdData];
  @override
  final String wireName = 'GGetListIdData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetListIdData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.MediaList;
    if (value != null) {
      result
        ..add('MediaList')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GGetListIdData_MediaList)));
    }
    return result;
  }

  @override
  GGetListIdData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetListIdDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'MediaList':
          result.MediaList.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GGetListIdData_MediaList))!
              as GGetListIdData_MediaList);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetListIdData_MediaListSerializer
    implements StructuredSerializer<GGetListIdData_MediaList> {
  @override
  final Iterable<Type> types = const [
    GGetListIdData_MediaList,
    _$GGetListIdData_MediaList
  ];
  @override
  final String wireName = 'GGetListIdData_MediaList';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetListIdData_MediaList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'mediaId',
      serializers.serialize(object.mediaId, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  GGetListIdData_MediaList deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetListIdData_MediaListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'mediaId':
          result.mediaId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetListIdData extends GGetListIdData {
  @override
  final String G__typename;
  @override
  final GGetListIdData_MediaList? MediaList;

  factory _$GGetListIdData([void Function(GGetListIdDataBuilder)? updates]) =>
      (new GGetListIdDataBuilder()..update(updates)).build();

  _$GGetListIdData._({required this.G__typename, this.MediaList}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetListIdData', 'G__typename');
  }

  @override
  GGetListIdData rebuild(void Function(GGetListIdDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetListIdDataBuilder toBuilder() =>
      new GGetListIdDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetListIdData &&
        G__typename == other.G__typename &&
        MediaList == other.MediaList;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), MediaList.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetListIdData')
          ..add('G__typename', G__typename)
          ..add('MediaList', MediaList))
        .toString();
  }
}

class GGetListIdDataBuilder
    implements Builder<GGetListIdData, GGetListIdDataBuilder> {
  _$GGetListIdData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetListIdData_MediaListBuilder? _MediaList;
  GGetListIdData_MediaListBuilder get MediaList =>
      _$this._MediaList ??= new GGetListIdData_MediaListBuilder();
  set MediaList(GGetListIdData_MediaListBuilder? MediaList) =>
      _$this._MediaList = MediaList;

  GGetListIdDataBuilder() {
    GGetListIdData._initializeBuilder(this);
  }

  GGetListIdDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _MediaList = $v.MediaList?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetListIdData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetListIdData;
  }

  @override
  void update(void Function(GGetListIdDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetListIdData build() {
    _$GGetListIdData _$result;
    try {
      _$result = _$v ??
          new _$GGetListIdData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GGetListIdData', 'G__typename'),
              MediaList: _MediaList?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'MediaList';
        _MediaList?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetListIdData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetListIdData_MediaList extends GGetListIdData_MediaList {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final int mediaId;

  factory _$GGetListIdData_MediaList(
          [void Function(GGetListIdData_MediaListBuilder)? updates]) =>
      (new GGetListIdData_MediaListBuilder()..update(updates)).build();

  _$GGetListIdData_MediaList._(
      {required this.G__typename, required this.id, required this.mediaId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetListIdData_MediaList', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GGetListIdData_MediaList', 'id');
    BuiltValueNullFieldError.checkNotNull(
        mediaId, 'GGetListIdData_MediaList', 'mediaId');
  }

  @override
  GGetListIdData_MediaList rebuild(
          void Function(GGetListIdData_MediaListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetListIdData_MediaListBuilder toBuilder() =>
      new GGetListIdData_MediaListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetListIdData_MediaList &&
        G__typename == other.G__typename &&
        id == other.id &&
        mediaId == other.mediaId;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), mediaId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetListIdData_MediaList')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('mediaId', mediaId))
        .toString();
  }
}

class GGetListIdData_MediaListBuilder
    implements
        Builder<GGetListIdData_MediaList, GGetListIdData_MediaListBuilder> {
  _$GGetListIdData_MediaList? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _mediaId;
  int? get mediaId => _$this._mediaId;
  set mediaId(int? mediaId) => _$this._mediaId = mediaId;

  GGetListIdData_MediaListBuilder() {
    GGetListIdData_MediaList._initializeBuilder(this);
  }

  GGetListIdData_MediaListBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _mediaId = $v.mediaId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetListIdData_MediaList other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetListIdData_MediaList;
  }

  @override
  void update(void Function(GGetListIdData_MediaListBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetListIdData_MediaList build() {
    final _$result = _$v ??
        new _$GGetListIdData_MediaList._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GGetListIdData_MediaList', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GGetListIdData_MediaList', 'id'),
            mediaId: BuiltValueNullFieldError.checkNotNull(
                mediaId, 'GGetListIdData_MediaList', 'mediaId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
