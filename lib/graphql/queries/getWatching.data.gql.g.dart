// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getWatching.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetWatchingData> _$gGetWatchingDataSerializer =
    new _$GGetWatchingDataSerializer();
Serializer<GGetWatchingData_Viewer> _$gGetWatchingDataViewerSerializer =
    new _$GGetWatchingData_ViewerSerializer();
Serializer<GGetWatchingData_MediaListCollection>
    _$gGetWatchingDataMediaListCollectionSerializer =
    new _$GGetWatchingData_MediaListCollectionSerializer();
Serializer<GGetWatchingData_MediaListCollection_user>
    _$gGetWatchingDataMediaListCollectionUserSerializer =
    new _$GGetWatchingData_MediaListCollection_userSerializer();
Serializer<GGetWatchingData_MediaListCollection_lists>
    _$gGetWatchingDataMediaListCollectionListsSerializer =
    new _$GGetWatchingData_MediaListCollection_listsSerializer();
Serializer<GGetWatchingData_MediaListCollection_lists_entries>
    _$gGetWatchingDataMediaListCollectionListsEntriesSerializer =
    new _$GGetWatchingData_MediaListCollection_lists_entriesSerializer();
Serializer<GGetWatchingData_MediaListCollection_lists_entries_media>
    _$gGetWatchingDataMediaListCollectionListsEntriesMediaSerializer =
    new _$GGetWatchingData_MediaListCollection_lists_entries_mediaSerializer();
Serializer<GGetWatchingData_MediaListCollection_lists_entries_media_coverImage>
    _$gGetWatchingDataMediaListCollectionListsEntriesMediaCoverImageSerializer =
    new _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImageSerializer();
Serializer<
        GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode>
    _$gGetWatchingDataMediaListCollectionListsEntriesMediaNextAiringEpisodeSerializer =
    new _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeSerializer();
Serializer<GGetWatchingData_MediaListCollection_lists_entries_media_title>
    _$gGetWatchingDataMediaListCollectionListsEntriesMediaTitleSerializer =
    new _$GGetWatchingData_MediaListCollection_lists_entries_media_titleSerializer();

class _$GGetWatchingDataSerializer
    implements StructuredSerializer<GGetWatchingData> {
  @override
  final Iterable<Type> types = const [GGetWatchingData, _$GGetWatchingData];
  @override
  final String wireName = 'GGetWatchingData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetWatchingData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.Viewer;
    if (value != null) {
      result
        ..add('Viewer')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GGetWatchingData_Viewer)));
    }
    value = object.MediaListCollection;
    if (value != null) {
      result
        ..add('MediaListCollection')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GGetWatchingData_MediaListCollection)));
    }
    return result;
  }

  @override
  GGetWatchingData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'Viewer':
          result.Viewer.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GGetWatchingData_Viewer))!
              as GGetWatchingData_Viewer);
          break;
        case 'MediaListCollection':
          result.MediaListCollection.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GGetWatchingData_MediaListCollection))!
              as GGetWatchingData_MediaListCollection);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_ViewerSerializer
    implements StructuredSerializer<GGetWatchingData_Viewer> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_Viewer,
    _$GGetWatchingData_Viewer
  ];
  @override
  final String wireName = 'GGetWatchingData_Viewer';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetWatchingData_Viewer object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GGetWatchingData_Viewer deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingData_ViewerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollectionSerializer
    implements StructuredSerializer<GGetWatchingData_MediaListCollection> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection,
    _$GGetWatchingData_MediaListCollection
  ];
  @override
  final String wireName = 'GGetWatchingData_MediaListCollection';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetWatchingData_MediaListCollection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.user;
    if (value != null) {
      result
        ..add('user')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GGetWatchingData_MediaListCollection_user)));
    }
    value = object.lists;
    if (value != null) {
      result
        ..add('lists')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BuiltList, const [
              const FullType(GGetWatchingData_MediaListCollection_lists)
            ])));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingData_MediaListCollectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user':
          result.user.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetWatchingData_MediaListCollection_user))!
              as GGetWatchingData_MediaListCollection_user);
          break;
        case 'lists':
          result.lists.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GGetWatchingData_MediaListCollection_lists)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_userSerializer
    implements StructuredSerializer<GGetWatchingData_MediaListCollection_user> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_user,
    _$GGetWatchingData_MediaListCollection_user
  ];
  @override
  final String wireName = 'GGetWatchingData_MediaListCollection_user';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GGetWatchingData_MediaListCollection_user object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_user deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingData_MediaListCollection_userBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_listsSerializer
    implements
        StructuredSerializer<GGetWatchingData_MediaListCollection_lists> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists,
    _$GGetWatchingData_MediaListCollection_lists
  ];
  @override
  final String wireName = 'GGetWatchingData_MediaListCollection_lists';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GGetWatchingData_MediaListCollection_lists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.entries;
    if (value != null) {
      result
        ..add('entries')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BuiltList, const [
              const FullType(GGetWatchingData_MediaListCollection_lists_entries)
            ])));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingData_MediaListCollection_listsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'entries':
          result.entries.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(
                    GGetWatchingData_MediaListCollection_lists_entries)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entriesSerializer
    implements
        StructuredSerializer<
            GGetWatchingData_MediaListCollection_lists_entries> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists_entries,
    _$GGetWatchingData_MediaListCollection_lists_entries
  ];
  @override
  final String wireName = 'GGetWatchingData_MediaListCollection_lists_entries';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GGetWatchingData_MediaListCollection_lists_entries object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.progress;
    if (value != null) {
      result
        ..add('progress')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.GMediaListStatus)));
    }
    value = object.media;
    if (value != null) {
      result
        ..add('media')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GGetWatchingData_MediaListCollection_lists_entries_media)));
    }
    value = object.notes;
    if (value != null) {
      result
        ..add('notes')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GGetWatchingData_MediaListCollection_lists_entriesBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'progress':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i2.GMediaListStatus))
              as _i2.GMediaListStatus?;
          break;
        case 'media':
          result.media.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetWatchingData_MediaListCollection_lists_entries_media))!
              as GGetWatchingData_MediaListCollection_lists_entries_media);
          break;
        case 'notes':
          result.notes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_mediaSerializer
    implements
        StructuredSerializer<
            GGetWatchingData_MediaListCollection_lists_entries_media> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists_entries_media,
    _$GGetWatchingData_MediaListCollection_lists_entries_media
  ];
  @override
  final String wireName =
      'GGetWatchingData_MediaListCollection_lists_entries_media';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GGetWatchingData_MediaListCollection_lists_entries_media object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.coverImage;
    if (value != null) {
      result
        ..add('coverImage')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GGetWatchingData_MediaListCollection_lists_entries_media_coverImage)));
    }
    value = object.episodes;
    if (value != null) {
      result
        ..add('episodes')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.nextAiringEpisode;
    if (value != null) {
      result
        ..add('nextAiringEpisode')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode)));
    }
    value = object.synonyms;
    if (value != null) {
      result
        ..add('synonyms')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i2.GMediaStatus)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GGetWatchingData_MediaListCollection_lists_entries_media_title)));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverImage':
          result.coverImage.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetWatchingData_MediaListCollection_lists_entries_media_coverImage))!
              as GGetWatchingData_MediaListCollection_lists_entries_media_coverImage);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'episodes':
          result.episodes = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'nextAiringEpisode':
          result.nextAiringEpisode.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode))!
              as GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode);
          break;
        case 'synonyms':
          result.synonyms.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i2.GMediaStatus))
              as _i2.GMediaStatus?;
          break;
        case 'title':
          result.title.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GGetWatchingData_MediaListCollection_lists_entries_media_title))!
              as GGetWatchingData_MediaListCollection_lists_entries_media_title);
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImageSerializer
    implements
        StructuredSerializer<
            GGetWatchingData_MediaListCollection_lists_entries_media_coverImage> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists_entries_media_coverImage,
    _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
  ];
  @override
  final String wireName =
      'GGetWatchingData_MediaListCollection_lists_entries_media_coverImage';

  @override
  Iterable<Object?> serialize(
      Serializers serializers,
      GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
          object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.extraLarge;
    if (value != null) {
      result
        ..add('extraLarge')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.large;
    if (value != null) {
      result
        ..add('large')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.color;
    if (value != null) {
      result
        ..add('color')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
      deserialize(Serializers serializers, Iterable<Object?> serialized,
          {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'extraLarge':
          result.extraLarge = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'large':
          result.large = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'color':
          result.color = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeSerializer
    implements
        StructuredSerializer<
            GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode,
    _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
  ];
  @override
  final String wireName =
      'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode';

  @override
  Iterable<Object?> serialize(
      Serializers serializers,
      GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
          object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'episode',
      serializers.serialize(object.episode, specifiedType: const FullType(int)),
      'timeUntilAiring',
      serializers.serialize(object.timeUntilAiring,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
      deserialize(Serializers serializers, Iterable<Object?> serialized,
          {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'episode':
          result.episode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'timeUntilAiring':
          result.timeUntilAiring = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_titleSerializer
    implements
        StructuredSerializer<
            GGetWatchingData_MediaListCollection_lists_entries_media_title> {
  @override
  final Iterable<Type> types = const [
    GGetWatchingData_MediaListCollection_lists_entries_media_title,
    _$GGetWatchingData_MediaListCollection_lists_entries_media_title
  ];
  @override
  final String wireName =
      'GGetWatchingData_MediaListCollection_lists_entries_media_title';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GGetWatchingData_MediaListCollection_lists_entries_media_title object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.userPreferred;
    if (value != null) {
      result
        ..add('userPreferred')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.romaji;
    if (value != null) {
      result
        ..add('romaji')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.english;
    if (value != null) {
      result
        ..add('english')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_title deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userPreferred':
          result.userPreferred = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'romaji':
          result.romaji = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'english':
          result.english = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingData extends GGetWatchingData {
  @override
  final String G__typename;
  @override
  final GGetWatchingData_Viewer? Viewer;
  @override
  final GGetWatchingData_MediaListCollection? MediaListCollection;

  factory _$GGetWatchingData(
          [void Function(GGetWatchingDataBuilder)? updates]) =>
      (new GGetWatchingDataBuilder()..update(updates)).build();

  _$GGetWatchingData._(
      {required this.G__typename, this.Viewer, this.MediaListCollection})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetWatchingData', 'G__typename');
  }

  @override
  GGetWatchingData rebuild(void Function(GGetWatchingDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingDataBuilder toBuilder() =>
      new GGetWatchingDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData &&
        G__typename == other.G__typename &&
        Viewer == other.Viewer &&
        MediaListCollection == other.MediaListCollection;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, G__typename.hashCode), Viewer.hashCode),
        MediaListCollection.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetWatchingData')
          ..add('G__typename', G__typename)
          ..add('Viewer', Viewer)
          ..add('MediaListCollection', MediaListCollection))
        .toString();
  }
}

class GGetWatchingDataBuilder
    implements Builder<GGetWatchingData, GGetWatchingDataBuilder> {
  _$GGetWatchingData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetWatchingData_ViewerBuilder? _Viewer;
  GGetWatchingData_ViewerBuilder get Viewer =>
      _$this._Viewer ??= new GGetWatchingData_ViewerBuilder();
  set Viewer(GGetWatchingData_ViewerBuilder? Viewer) => _$this._Viewer = Viewer;

  GGetWatchingData_MediaListCollectionBuilder? _MediaListCollection;
  GGetWatchingData_MediaListCollectionBuilder get MediaListCollection =>
      _$this._MediaListCollection ??=
          new GGetWatchingData_MediaListCollectionBuilder();
  set MediaListCollection(
          GGetWatchingData_MediaListCollectionBuilder? MediaListCollection) =>
      _$this._MediaListCollection = MediaListCollection;

  GGetWatchingDataBuilder() {
    GGetWatchingData._initializeBuilder(this);
  }

  GGetWatchingDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _Viewer = $v.Viewer?.toBuilder();
      _MediaListCollection = $v.MediaListCollection?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData;
  }

  @override
  void update(void Function(GGetWatchingDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData build() {
    _$GGetWatchingData _$result;
    try {
      _$result = _$v ??
          new _$GGetWatchingData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GGetWatchingData', 'G__typename'),
              Viewer: _Viewer?.build(),
              MediaListCollection: _MediaListCollection?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'Viewer';
        _Viewer?.build();
        _$failedField = 'MediaListCollection';
        _MediaListCollection?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetWatchingData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_Viewer extends GGetWatchingData_Viewer {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final String name;

  factory _$GGetWatchingData_Viewer(
          [void Function(GGetWatchingData_ViewerBuilder)? updates]) =>
      (new GGetWatchingData_ViewerBuilder()..update(updates)).build();

  _$GGetWatchingData_Viewer._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetWatchingData_Viewer', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GGetWatchingData_Viewer', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GGetWatchingData_Viewer', 'name');
  }

  @override
  GGetWatchingData_Viewer rebuild(
          void Function(GGetWatchingData_ViewerBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_ViewerBuilder toBuilder() =>
      new GGetWatchingData_ViewerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_Viewer &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetWatchingData_Viewer')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GGetWatchingData_ViewerBuilder
    implements
        Builder<GGetWatchingData_Viewer, GGetWatchingData_ViewerBuilder> {
  _$GGetWatchingData_Viewer? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GGetWatchingData_ViewerBuilder() {
    GGetWatchingData_Viewer._initializeBuilder(this);
  }

  GGetWatchingData_ViewerBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_Viewer other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_Viewer;
  }

  @override
  void update(void Function(GGetWatchingData_ViewerBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_Viewer build() {
    final _$result = _$v ??
        new _$GGetWatchingData_Viewer._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GGetWatchingData_Viewer', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GGetWatchingData_Viewer', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GGetWatchingData_Viewer', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection
    extends GGetWatchingData_MediaListCollection {
  @override
  final String G__typename;
  @override
  final GGetWatchingData_MediaListCollection_user? user;
  @override
  final BuiltList<GGetWatchingData_MediaListCollection_lists>? lists;

  factory _$GGetWatchingData_MediaListCollection(
          [void Function(GGetWatchingData_MediaListCollectionBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollectionBuilder()..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection._(
      {required this.G__typename, this.user, this.lists})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GGetWatchingData_MediaListCollection', 'G__typename');
  }

  @override
  GGetWatchingData_MediaListCollection rebuild(
          void Function(GGetWatchingData_MediaListCollectionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollectionBuilder toBuilder() =>
      new GGetWatchingData_MediaListCollectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_MediaListCollection &&
        G__typename == other.G__typename &&
        user == other.user &&
        lists == other.lists;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), user.hashCode), lists.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetWatchingData_MediaListCollection')
          ..add('G__typename', G__typename)
          ..add('user', user)
          ..add('lists', lists))
        .toString();
  }
}

class GGetWatchingData_MediaListCollectionBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection,
            GGetWatchingData_MediaListCollectionBuilder> {
  _$GGetWatchingData_MediaListCollection? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetWatchingData_MediaListCollection_userBuilder? _user;
  GGetWatchingData_MediaListCollection_userBuilder get user =>
      _$this._user ??= new GGetWatchingData_MediaListCollection_userBuilder();
  set user(GGetWatchingData_MediaListCollection_userBuilder? user) =>
      _$this._user = user;

  ListBuilder<GGetWatchingData_MediaListCollection_lists>? _lists;
  ListBuilder<GGetWatchingData_MediaListCollection_lists> get lists =>
      _$this._lists ??=
          new ListBuilder<GGetWatchingData_MediaListCollection_lists>();
  set lists(ListBuilder<GGetWatchingData_MediaListCollection_lists>? lists) =>
      _$this._lists = lists;

  GGetWatchingData_MediaListCollectionBuilder() {
    GGetWatchingData_MediaListCollection._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollectionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _user = $v.user?.toBuilder();
      _lists = $v.lists?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_MediaListCollection other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_MediaListCollection;
  }

  @override
  void update(
      void Function(GGetWatchingData_MediaListCollectionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection build() {
    _$GGetWatchingData_MediaListCollection _$result;
    try {
      _$result = _$v ??
          new _$GGetWatchingData_MediaListCollection._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GGetWatchingData_MediaListCollection', 'G__typename'),
              user: _user?.build(),
              lists: _lists?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'user';
        _user?.build();
        _$failedField = 'lists';
        _lists?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetWatchingData_MediaListCollection',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_user
    extends GGetWatchingData_MediaListCollection_user {
  @override
  final String G__typename;
  @override
  final int id;

  factory _$GGetWatchingData_MediaListCollection_user(
          [void Function(GGetWatchingData_MediaListCollection_userBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_userBuilder()..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_user._(
      {required this.G__typename, required this.id})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GGetWatchingData_MediaListCollection_user', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GGetWatchingData_MediaListCollection_user', 'id');
  }

  @override
  GGetWatchingData_MediaListCollection_user rebuild(
          void Function(GGetWatchingData_MediaListCollection_userBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_userBuilder toBuilder() =>
      new GGetWatchingData_MediaListCollection_userBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_MediaListCollection_user &&
        G__typename == other.G__typename &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_user')
          ..add('G__typename', G__typename)
          ..add('id', id))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_userBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection_user,
            GGetWatchingData_MediaListCollection_userBuilder> {
  _$GGetWatchingData_MediaListCollection_user? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  GGetWatchingData_MediaListCollection_userBuilder() {
    GGetWatchingData_MediaListCollection_user._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_userBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_MediaListCollection_user other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_MediaListCollection_user;
  }

  @override
  void update(
      void Function(GGetWatchingData_MediaListCollection_userBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_user build() {
    final _$result = _$v ??
        new _$GGetWatchingData_MediaListCollection_user._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GGetWatchingData_MediaListCollection_user', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GGetWatchingData_MediaListCollection_user', 'id'));
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists
    extends GGetWatchingData_MediaListCollection_lists {
  @override
  final String G__typename;
  @override
  final String? name;
  @override
  final BuiltList<GGetWatchingData_MediaListCollection_lists_entries>? entries;

  factory _$GGetWatchingData_MediaListCollection_lists(
          [void Function(GGetWatchingData_MediaListCollection_listsBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_listsBuilder()..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists._(
      {required this.G__typename, this.name, this.entries})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GGetWatchingData_MediaListCollection_lists', 'G__typename');
  }

  @override
  GGetWatchingData_MediaListCollection_lists rebuild(
          void Function(GGetWatchingData_MediaListCollection_listsBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_listsBuilder toBuilder() =>
      new GGetWatchingData_MediaListCollection_listsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_MediaListCollection_lists &&
        G__typename == other.G__typename &&
        name == other.name &&
        entries == other.entries;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), name.hashCode), entries.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists')
          ..add('G__typename', G__typename)
          ..add('name', name)
          ..add('entries', entries))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_listsBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection_lists,
            GGetWatchingData_MediaListCollection_listsBuilder> {
  _$GGetWatchingData_MediaListCollection_lists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<GGetWatchingData_MediaListCollection_lists_entries>? _entries;
  ListBuilder<GGetWatchingData_MediaListCollection_lists_entries> get entries =>
      _$this._entries ??=
          new ListBuilder<GGetWatchingData_MediaListCollection_lists_entries>();
  set entries(
          ListBuilder<GGetWatchingData_MediaListCollection_lists_entries>?
              entries) =>
      _$this._entries = entries;

  GGetWatchingData_MediaListCollection_listsBuilder() {
    GGetWatchingData_MediaListCollection_lists._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_listsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _name = $v.name;
      _entries = $v.entries?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_MediaListCollection_lists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_MediaListCollection_lists;
  }

  @override
  void update(
      void Function(GGetWatchingData_MediaListCollection_listsBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists build() {
    _$GGetWatchingData_MediaListCollection_lists _$result;
    try {
      _$result = _$v ??
          new _$GGetWatchingData_MediaListCollection_lists._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GGetWatchingData_MediaListCollection_lists', 'G__typename'),
              name: name,
              entries: _entries?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'entries';
        _entries?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetWatchingData_MediaListCollection_lists',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries
    extends GGetWatchingData_MediaListCollection_lists_entries {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final int? progress;
  @override
  final _i2.GMediaListStatus? status;
  @override
  final GGetWatchingData_MediaListCollection_lists_entries_media? media;
  @override
  final String? notes;

  factory _$GGetWatchingData_MediaListCollection_lists_entries(
          [void Function(
                  GGetWatchingData_MediaListCollection_lists_entriesBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_lists_entriesBuilder()
            ..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists_entries._(
      {required this.G__typename,
      required this.id,
      this.progress,
      this.status,
      this.media,
      this.notes})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GGetWatchingData_MediaListCollection_lists_entries', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GGetWatchingData_MediaListCollection_lists_entries', 'id');
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries rebuild(
          void Function(
                  GGetWatchingData_MediaListCollection_lists_entriesBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_lists_entriesBuilder toBuilder() =>
      new GGetWatchingData_MediaListCollection_lists_entriesBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_MediaListCollection_lists_entries &&
        G__typename == other.G__typename &&
        id == other.id &&
        progress == other.progress &&
        status == other.status &&
        media == other.media &&
        notes == other.notes;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    progress.hashCode),
                status.hashCode),
            media.hashCode),
        notes.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists_entries')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('progress', progress)
          ..add('status', status)
          ..add('media', media)
          ..add('notes', notes))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_lists_entriesBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection_lists_entries,
            GGetWatchingData_MediaListCollection_lists_entriesBuilder> {
  _$GGetWatchingData_MediaListCollection_lists_entries? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _progress;
  int? get progress => _$this._progress;
  set progress(int? progress) => _$this._progress = progress;

  _i2.GMediaListStatus? _status;
  _i2.GMediaListStatus? get status => _$this._status;
  set status(_i2.GMediaListStatus? status) => _$this._status = status;

  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder? _media;
  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder get media =>
      _$this._media ??=
          new GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder();
  set media(
          GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder?
              media) =>
      _$this._media = media;

  String? _notes;
  String? get notes => _$this._notes;
  set notes(String? notes) => _$this._notes = notes;

  GGetWatchingData_MediaListCollection_lists_entriesBuilder() {
    GGetWatchingData_MediaListCollection_lists_entries._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_lists_entriesBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _progress = $v.progress;
      _status = $v.status;
      _media = $v.media?.toBuilder();
      _notes = $v.notes;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_MediaListCollection_lists_entries other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_MediaListCollection_lists_entries;
  }

  @override
  void update(
      void Function(GGetWatchingData_MediaListCollection_lists_entriesBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists_entries build() {
    _$GGetWatchingData_MediaListCollection_lists_entries _$result;
    try {
      _$result = _$v ??
          new _$GGetWatchingData_MediaListCollection_lists_entries._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename,
                  'GGetWatchingData_MediaListCollection_lists_entries',
                  'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(id,
                  'GGetWatchingData_MediaListCollection_lists_entries', 'id'),
              progress: progress,
              status: status,
              media: _media?.build(),
              notes: notes);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'media';
        _media?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetWatchingData_MediaListCollection_lists_entries',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media
    extends GGetWatchingData_MediaListCollection_lists_entries_media {
  @override
  final String G__typename;
  @override
  final GGetWatchingData_MediaListCollection_lists_entries_media_coverImage?
      coverImage;
  @override
  final int id;
  @override
  final int? episodes;
  @override
  final GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode?
      nextAiringEpisode;
  @override
  final BuiltList<String>? synonyms;
  @override
  final _i2.GMediaStatus? status;
  @override
  final GGetWatchingData_MediaListCollection_lists_entries_media_title? title;

  factory _$GGetWatchingData_MediaListCollection_lists_entries_media(
          [void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder()
            ..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists_entries_media._(
      {required this.G__typename,
      this.coverImage,
      required this.id,
      this.episodes,
      this.nextAiringEpisode,
      this.synonyms,
      this.status,
      this.title})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename,
        'GGetWatchingData_MediaListCollection_lists_entries_media',
        'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GGetWatchingData_MediaListCollection_lists_entries_media', 'id');
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media rebuild(
          void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder toBuilder() =>
      new GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingData_MediaListCollection_lists_entries_media &&
        G__typename == other.G__typename &&
        coverImage == other.coverImage &&
        id == other.id &&
        episodes == other.episodes &&
        nextAiringEpisode == other.nextAiringEpisode &&
        synonyms == other.synonyms &&
        status == other.status &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc(0, G__typename.hashCode),
                                coverImage.hashCode),
                            id.hashCode),
                        episodes.hashCode),
                    nextAiringEpisode.hashCode),
                synonyms.hashCode),
            status.hashCode),
        title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists_entries_media')
          ..add('G__typename', G__typename)
          ..add('coverImage', coverImage)
          ..add('id', id)
          ..add('episodes', episodes)
          ..add('nextAiringEpisode', nextAiringEpisode)
          ..add('synonyms', synonyms)
          ..add('status', status)
          ..add('title', title))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection_lists_entries_media,
            GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder> {
  _$GGetWatchingData_MediaListCollection_lists_entries_media? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder?
      _coverImage;
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
      get coverImage => _$this._coverImage ??=
          new GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder();
  set coverImage(
          GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder?
              coverImage) =>
      _$this._coverImage = coverImage;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _episodes;
  int? get episodes => _$this._episodes;
  set episodes(int? episodes) => _$this._episodes = episodes;

  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder?
      _nextAiringEpisode;
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
      get nextAiringEpisode => _$this._nextAiringEpisode ??=
          new GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder();
  set nextAiringEpisode(
          GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder?
              nextAiringEpisode) =>
      _$this._nextAiringEpisode = nextAiringEpisode;

  ListBuilder<String>? _synonyms;
  ListBuilder<String> get synonyms =>
      _$this._synonyms ??= new ListBuilder<String>();
  set synonyms(ListBuilder<String>? synonyms) => _$this._synonyms = synonyms;

  _i2.GMediaStatus? _status;
  _i2.GMediaStatus? get status => _$this._status;
  set status(_i2.GMediaStatus? status) => _$this._status = status;

  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder? _title;
  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
      get title => _$this._title ??=
          new GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder();
  set title(
          GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder?
              title) =>
      _$this._title = title;

  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder() {
    GGetWatchingData_MediaListCollection_lists_entries_media._initializeBuilder(
        this);
  }

  GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _coverImage = $v.coverImage?.toBuilder();
      _id = $v.id;
      _episodes = $v.episodes;
      _nextAiringEpisode = $v.nextAiringEpisode?.toBuilder();
      _synonyms = $v.synonyms?.toBuilder();
      _status = $v.status;
      _title = $v.title?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingData_MediaListCollection_lists_entries_media other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingData_MediaListCollection_lists_entries_media;
  }

  @override
  void update(
      void Function(
              GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists_entries_media build() {
    _$GGetWatchingData_MediaListCollection_lists_entries_media _$result;
    try {
      _$result = _$v ??
          new _$GGetWatchingData_MediaListCollection_lists_entries_media._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename,
                  'GGetWatchingData_MediaListCollection_lists_entries_media',
                  'G__typename'),
              coverImage: _coverImage?.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id,
                  'GGetWatchingData_MediaListCollection_lists_entries_media',
                  'id'),
              episodes: episodes,
              nextAiringEpisode: _nextAiringEpisode?.build(),
              synonyms: _synonyms?.build(),
              status: status,
              title: _title?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'coverImage';
        _coverImage?.build();

        _$failedField = 'nextAiringEpisode';
        _nextAiringEpisode?.build();
        _$failedField = 'synonyms';
        _synonyms?.build();

        _$failedField = 'title';
        _title?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GGetWatchingData_MediaListCollection_lists_entries_media',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
    extends GGetWatchingData_MediaListCollection_lists_entries_media_coverImage {
  @override
  final String G__typename;
  @override
  final String? extraLarge;
  @override
  final String? large;
  @override
  final String? color;

  factory _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage(
          [void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder()
            ..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage._(
      {required this.G__typename, this.extraLarge, this.large, this.color})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename,
        'GGetWatchingData_MediaListCollection_lists_entries_media_coverImage',
        'G__typename');
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImage rebuild(
          void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
      toBuilder() =>
          new GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is GGetWatchingData_MediaListCollection_lists_entries_media_coverImage &&
        G__typename == other.G__typename &&
        extraLarge == other.extraLarge &&
        large == other.large &&
        color == other.color;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), extraLarge.hashCode),
            large.hashCode),
        color.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists_entries_media_coverImage')
          ..add('G__typename', G__typename)
          ..add('extraLarge', extraLarge)
          ..add('large', large)
          ..add('color', color))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
    implements
        Builder<
            GGetWatchingData_MediaListCollection_lists_entries_media_coverImage,
            GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder> {
  _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _extraLarge;
  String? get extraLarge => _$this._extraLarge;
  set extraLarge(String? extraLarge) => _$this._extraLarge = extraLarge;

  String? _large;
  String? get large => _$this._large;
  set large(String? large) => _$this._large = large;

  String? _color;
  String? get color => _$this._color;
  set color(String? color) => _$this._color = color;

  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder() {
    GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
        ._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _extraLarge = $v.extraLarge;
      _large = $v.large;
      _color = $v.color;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage;
  }

  @override
  void update(
      void Function(
              GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
      build() {
    final _$result = _$v ??
        new _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
                ._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GGetWatchingData_MediaListCollection_lists_entries_media_coverImage',
                'G__typename'),
            extraLarge: extraLarge,
            large: large,
            color: color);
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
    extends GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode {
  @override
  final String G__typename;
  @override
  final int episode;
  @override
  final int timeUntilAiring;

  factory _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode(
          [void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder()
            ..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode._(
      {required this.G__typename,
      required this.episode,
      required this.timeUntilAiring})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename,
        'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
        'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        episode,
        'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
        'episode');
    BuiltValueNullFieldError.checkNotNull(
        timeUntilAiring,
        'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
        'timeUntilAiring');
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
      rebuild(
              void Function(
                      GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder)
                  updates) =>
          (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
      toBuilder() =>
          new GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode &&
        G__typename == other.G__typename &&
        episode == other.episode &&
        timeUntilAiring == other.timeUntilAiring;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, G__typename.hashCode), episode.hashCode),
        timeUntilAiring.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode')
          ..add('G__typename', G__typename)
          ..add('episode', episode)
          ..add('timeUntilAiring', timeUntilAiring))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
    implements
        Builder<
            GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode,
            GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder> {
  _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode?
      _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _episode;
  int? get episode => _$this._episode;
  set episode(int? episode) => _$this._episode = episode;

  int? _timeUntilAiring;
  int? get timeUntilAiring => _$this._timeUntilAiring;
  set timeUntilAiring(int? timeUntilAiring) =>
      _$this._timeUntilAiring = timeUntilAiring;

  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder() {
    GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
        ._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _episode = $v.episode;
      _timeUntilAiring = $v.timeUntilAiring;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
          other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode;
  }

  @override
  void update(
      void Function(
              GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
      build() {
    final _$result = _$v ??
        new _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
                ._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
                'G__typename'),
            episode: BuiltValueNullFieldError.checkNotNull(
                episode,
                'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
                'episode'),
            timeUntilAiring: BuiltValueNullFieldError.checkNotNull(
                timeUntilAiring,
                'GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode',
                'timeUntilAiring'));
    replace(_$result);
    return _$result;
  }
}

class _$GGetWatchingData_MediaListCollection_lists_entries_media_title
    extends GGetWatchingData_MediaListCollection_lists_entries_media_title {
  @override
  final String G__typename;
  @override
  final String? userPreferred;
  @override
  final String? romaji;
  @override
  final String? english;

  factory _$GGetWatchingData_MediaListCollection_lists_entries_media_title(
          [void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder)?
              updates]) =>
      (new GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder()
            ..update(updates))
          .build();

  _$GGetWatchingData_MediaListCollection_lists_entries_media_title._(
      {required this.G__typename,
      this.userPreferred,
      this.romaji,
      this.english})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename,
        'GGetWatchingData_MediaListCollection_lists_entries_media_title',
        'G__typename');
  }

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_title rebuild(
          void Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
      toBuilder() =>
          new GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder()
            ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other
            is GGetWatchingData_MediaListCollection_lists_entries_media_title &&
        G__typename == other.G__typename &&
        userPreferred == other.userPreferred &&
        romaji == other.romaji &&
        english == other.english;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), userPreferred.hashCode),
            romaji.hashCode),
        english.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GGetWatchingData_MediaListCollection_lists_entries_media_title')
          ..add('G__typename', G__typename)
          ..add('userPreferred', userPreferred)
          ..add('romaji', romaji)
          ..add('english', english))
        .toString();
  }
}

class GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
    implements
        Builder<GGetWatchingData_MediaListCollection_lists_entries_media_title,
            GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder> {
  _$GGetWatchingData_MediaListCollection_lists_entries_media_title? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _userPreferred;
  String? get userPreferred => _$this._userPreferred;
  set userPreferred(String? userPreferred) =>
      _$this._userPreferred = userPreferred;

  String? _romaji;
  String? get romaji => _$this._romaji;
  set romaji(String? romaji) => _$this._romaji = romaji;

  String? _english;
  String? get english => _$this._english;
  set english(String? english) => _$this._english = english;

  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder() {
    GGetWatchingData_MediaListCollection_lists_entries_media_title
        ._initializeBuilder(this);
  }

  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
      get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _userPreferred = $v.userPreferred;
      _romaji = $v.romaji;
      _english = $v.english;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(
      GGetWatchingData_MediaListCollection_lists_entries_media_title other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other
        as _$GGetWatchingData_MediaListCollection_lists_entries_media_title;
  }

  @override
  void update(
      void Function(
              GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingData_MediaListCollection_lists_entries_media_title build() {
    final _$result = _$v ??
        new _$GGetWatchingData_MediaListCollection_lists_entries_media_title._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GGetWatchingData_MediaListCollection_lists_entries_media_title',
                'G__typename'),
            userPreferred: userPreferred,
            romaji: romaji,
            english: english);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
