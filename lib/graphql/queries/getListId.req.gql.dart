// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;
import 'package:killer_anime/graphql/queries/getListId.ast.gql.dart' as _i5;
import 'package:killer_anime/graphql/queries/getListId.data.gql.dart' as _i2;
import 'package:killer_anime/graphql/queries/getListId.var.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'getListId.req.gql.g.dart';

abstract class GGetListIdReq
    implements
        Built<GGetListIdReq, GGetListIdReqBuilder>,
        _i1.OperationRequest<_i2.GGetListIdData, _i3.GGetListIdVars> {
  GGetListIdReq._();

  factory GGetListIdReq([Function(GGetListIdReqBuilder b) updates]) =
      _$GGetListIdReq;

  static void _initializeBuilder(GGetListIdReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'GetListId')
    ..executeOnListen = true;
  _i3.GGetListIdVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GGetListIdData? Function(_i2.GGetListIdData?, _i2.GGetListIdData?)?
      get updateResult;
  _i2.GGetListIdData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GGetListIdData? parseData(Map<String, dynamic> json) =>
      _i2.GGetListIdData.fromJson(json);
  static Serializer<GGetListIdReq> get serializer => _$gGetListIdReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GGetListIdReq.serializer, this)
          as Map<String, dynamic>);
  static GGetListIdReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GGetListIdReq.serializer, json);
}
