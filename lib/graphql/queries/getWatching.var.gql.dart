// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getWatching.var.gql.g.dart';

abstract class GGetWatchingVars
    implements Built<GGetWatchingVars, GGetWatchingVarsBuilder> {
  GGetWatchingVars._();

  factory GGetWatchingVars([Function(GGetWatchingVarsBuilder b) updates]) =
      _$GGetWatchingVars;

  int? get id;
  static Serializer<GGetWatchingVars> get serializer =>
      _$gGetWatchingVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetWatchingVars.serializer, this)
          as Map<String, dynamic>);
  static GGetWatchingVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetWatchingVars.serializer, json);
}
