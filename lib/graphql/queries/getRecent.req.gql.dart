// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;
import 'package:killer_anime/graphql/queries/getRecent.ast.gql.dart' as _i5;
import 'package:killer_anime/graphql/queries/getRecent.data.gql.dart' as _i2;
import 'package:killer_anime/graphql/queries/getRecent.var.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'getRecent.req.gql.g.dart';

abstract class GGetRecentReq
    implements
        Built<GGetRecentReq, GGetRecentReqBuilder>,
        _i1.OperationRequest<_i2.GGetRecentData, _i3.GGetRecentVars> {
  GGetRecentReq._();

  factory GGetRecentReq([Function(GGetRecentReqBuilder b) updates]) =
      _$GGetRecentReq;

  static void _initializeBuilder(GGetRecentReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'GetRecent')
    ..executeOnListen = true;
  _i3.GGetRecentVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GGetRecentData? Function(_i2.GGetRecentData?, _i2.GGetRecentData?)?
      get updateResult;
  _i2.GGetRecentData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GGetRecentData? parseData(Map<String, dynamic> json) =>
      _i2.GGetRecentData.fromJson(json);
  static Serializer<GGetRecentReq> get serializer => _$gGetRecentReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GGetRecentReq.serializer, this)
          as Map<String, dynamic>);
  static GGetRecentReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GGetRecentReq.serializer, json);
}
