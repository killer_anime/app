// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getListId.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetListIdVars> _$gGetListIdVarsSerializer =
    new _$GGetListIdVarsSerializer();

class _$GGetListIdVarsSerializer
    implements StructuredSerializer<GGetListIdVars> {
  @override
  final Iterable<Type> types = const [GGetListIdVars, _$GGetListIdVars];
  @override
  final String wireName = 'GGetListIdVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetListIdVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.mediaId;
    if (value != null) {
      result
        ..add('mediaId')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GGetListIdVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetListIdVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'mediaId':
          result.mediaId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetListIdVars extends GGetListIdVars {
  @override
  final int? mediaId;

  factory _$GGetListIdVars([void Function(GGetListIdVarsBuilder)? updates]) =>
      (new GGetListIdVarsBuilder()..update(updates)).build();

  _$GGetListIdVars._({this.mediaId}) : super._();

  @override
  GGetListIdVars rebuild(void Function(GGetListIdVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetListIdVarsBuilder toBuilder() =>
      new GGetListIdVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetListIdVars && mediaId == other.mediaId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, mediaId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetListIdVars')
          ..add('mediaId', mediaId))
        .toString();
  }
}

class GGetListIdVarsBuilder
    implements Builder<GGetListIdVars, GGetListIdVarsBuilder> {
  _$GGetListIdVars? _$v;

  int? _mediaId;
  int? get mediaId => _$this._mediaId;
  set mediaId(int? mediaId) => _$this._mediaId = mediaId;

  GGetListIdVarsBuilder();

  GGetListIdVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _mediaId = $v.mediaId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetListIdVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetListIdVars;
  }

  @override
  void update(void Function(GGetListIdVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetListIdVars build() {
    final _$result = _$v ?? new _$GGetListIdVars._(mediaId: mediaId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
