// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:gql/ast.dart' as _i1;
import 'package:killer_anime/graphql/fragments/MediaDisplay.ast.gql.dart'
    as _i2;

const GetPage = _i1.OperationDefinitionNode(
    type: _i1.OperationType.query,
    name: _i1.NameNode(value: 'GetPage'),
    variableDefinitions: [
      _i1.VariableDefinitionNode(
          variable: _i1.VariableNode(name: _i1.NameNode(value: 'page')),
          type: _i1.NamedTypeNode(
              name: _i1.NameNode(value: 'Int'), isNonNull: false),
          defaultValue: _i1.DefaultValueNode(value: null),
          directives: []),
      _i1.VariableDefinitionNode(
          variable: _i1.VariableNode(name: _i1.NameNode(value: 'search')),
          type: _i1.NamedTypeNode(
              name: _i1.NameNode(value: 'String'), isNonNull: false),
          defaultValue: _i1.DefaultValueNode(value: null),
          directives: [])
    ],
    directives: [],
    selectionSet: _i1.SelectionSetNode(selections: [
      _i1.FieldNode(
          name: _i1.NameNode(value: 'Page'),
          alias: null,
          arguments: [
            _i1.ArgumentNode(
                name: _i1.NameNode(value: 'page'),
                value: _i1.VariableNode(name: _i1.NameNode(value: 'page'))),
            _i1.ArgumentNode(
                name: _i1.NameNode(value: 'perPage'),
                value: _i1.IntValueNode(value: '25'))
          ],
          directives: [],
          selectionSet: _i1.SelectionSetNode(selections: [
            _i1.FieldNode(
                name: _i1.NameNode(value: 'media'),
                alias: null,
                arguments: [
                  _i1.ArgumentNode(
                      name: _i1.NameNode(value: 'sort'),
                      value: _i1.ListValueNode(values: [
                        _i1.EnumValueNode(
                            name: _i1.NameNode(value: 'POPULARITY_DESC')),
                        _i1.EnumValueNode(
                            name: _i1.NameNode(value: 'TRENDING_DESC'))
                      ])),
                  _i1.ArgumentNode(
                      name: _i1.NameNode(value: 'search'),
                      value: _i1.VariableNode(
                          name: _i1.NameNode(value: 'search'))),
                  _i1.ArgumentNode(
                      name: _i1.NameNode(value: 'type'),
                      value:
                          _i1.EnumValueNode(name: _i1.NameNode(value: 'ANIME')))
                ],
                directives: [],
                selectionSet: _i1.SelectionSetNode(selections: [
                  _i1.FragmentSpreadNode(
                      name: _i1.NameNode(value: 'MediaDisplay'), directives: [])
                ]))
          ]))
    ]));
const document = _i1.DocumentNode(definitions: [GetPage, _i2.MediaDisplay]);
