// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart'
    as _i3;
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i2;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getWatching.data.gql.g.dart';

abstract class GGetWatchingData
    implements Built<GGetWatchingData, GGetWatchingDataBuilder> {
  GGetWatchingData._();

  factory GGetWatchingData([Function(GGetWatchingDataBuilder b) updates]) =
      _$GGetWatchingData;

  static void _initializeBuilder(GGetWatchingDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetWatchingData_Viewer? get Viewer;
  GGetWatchingData_MediaListCollection? get MediaListCollection;
  static Serializer<GGetWatchingData> get serializer =>
      _$gGetWatchingDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetWatchingData.serializer, this)
          as Map<String, dynamic>);
  static GGetWatchingData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetWatchingData.serializer, json);
}

abstract class GGetWatchingData_Viewer
    implements Built<GGetWatchingData_Viewer, GGetWatchingData_ViewerBuilder> {
  GGetWatchingData_Viewer._();

  factory GGetWatchingData_Viewer(
          [Function(GGetWatchingData_ViewerBuilder b) updates]) =
      _$GGetWatchingData_Viewer;

  static void _initializeBuilder(GGetWatchingData_ViewerBuilder b) =>
      b..G__typename = 'User';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  String get name;
  static Serializer<GGetWatchingData_Viewer> get serializer =>
      _$gGetWatchingDataViewerSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetWatchingData_Viewer.serializer, this)
          as Map<String, dynamic>);
  static GGetWatchingData_Viewer? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetWatchingData_Viewer.serializer, json);
}

abstract class GGetWatchingData_MediaListCollection
    implements
        Built<GGetWatchingData_MediaListCollection,
            GGetWatchingData_MediaListCollectionBuilder> {
  GGetWatchingData_MediaListCollection._();

  factory GGetWatchingData_MediaListCollection(
          [Function(GGetWatchingData_MediaListCollectionBuilder b) updates]) =
      _$GGetWatchingData_MediaListCollection;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollectionBuilder b) =>
      b..G__typename = 'MediaListCollection';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetWatchingData_MediaListCollection_user? get user;
  BuiltList<GGetWatchingData_MediaListCollection_lists>? get lists;
  static Serializer<GGetWatchingData_MediaListCollection> get serializer =>
      _$gGetWatchingDataMediaListCollectionSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GGetWatchingData_MediaListCollection.serializer, this)
      as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection.serializer, json);
}

abstract class GGetWatchingData_MediaListCollection_user
    implements
        Built<GGetWatchingData_MediaListCollection_user,
            GGetWatchingData_MediaListCollection_userBuilder> {
  GGetWatchingData_MediaListCollection_user._();

  factory GGetWatchingData_MediaListCollection_user(
      [Function(GGetWatchingData_MediaListCollection_userBuilder b)
          updates]) = _$GGetWatchingData_MediaListCollection_user;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_userBuilder b) =>
      b..G__typename = 'User';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  static Serializer<GGetWatchingData_MediaListCollection_user> get serializer =>
      _$gGetWatchingDataMediaListCollectionUserSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GGetWatchingData_MediaListCollection_user.serializer, this)
      as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_user? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_user.serializer, json);
}

abstract class GGetWatchingData_MediaListCollection_lists
    implements
        Built<GGetWatchingData_MediaListCollection_lists,
            GGetWatchingData_MediaListCollection_listsBuilder> {
  GGetWatchingData_MediaListCollection_lists._();

  factory GGetWatchingData_MediaListCollection_lists(
      [Function(GGetWatchingData_MediaListCollection_listsBuilder b)
          updates]) = _$GGetWatchingData_MediaListCollection_lists;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_listsBuilder b) =>
      b..G__typename = 'MediaListGroup';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get name;
  BuiltList<GGetWatchingData_MediaListCollection_lists_entries>? get entries;
  static Serializer<GGetWatchingData_MediaListCollection_lists>
      get serializer => _$gGetWatchingDataMediaListCollectionListsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GGetWatchingData_MediaListCollection_lists.serializer, this)
      as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists.serializer, json);
}

abstract class GGetWatchingData_MediaListCollection_lists_entries
    implements
        Built<GGetWatchingData_MediaListCollection_lists_entries,
            GGetWatchingData_MediaListCollection_lists_entriesBuilder> {
  GGetWatchingData_MediaListCollection_lists_entries._();

  factory GGetWatchingData_MediaListCollection_lists_entries(
      [Function(GGetWatchingData_MediaListCollection_lists_entriesBuilder b)
          updates]) = _$GGetWatchingData_MediaListCollection_lists_entries;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_lists_entriesBuilder b) =>
      b..G__typename = 'MediaList';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  int? get progress;
  _i2.GMediaListStatus? get status;
  GGetWatchingData_MediaListCollection_lists_entries_media? get media;
  String? get notes;
  static Serializer<GGetWatchingData_MediaListCollection_lists_entries>
      get serializer =>
          _$gGetWatchingDataMediaListCollectionListsEntriesSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GGetWatchingData_MediaListCollection_lists_entries.serializer, this)
      as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists_entries? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists_entries.serializer, json);
}

abstract class GGetWatchingData_MediaListCollection_lists_entries_media
    implements
        Built<GGetWatchingData_MediaListCollection_lists_entries_media,
            GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder>,
        _i3.GMediaDisplay {
  GGetWatchingData_MediaListCollection_lists_entries_media._();

  factory GGetWatchingData_MediaListCollection_lists_entries_media(
      [Function(
              GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder b)
          updates]) = _$GGetWatchingData_MediaListCollection_lists_entries_media;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_lists_entries_mediaBuilder b) =>
      b..G__typename = 'Media';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImage?
      get coverImage;
  int get id;
  int? get episodes;
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode?
      get nextAiringEpisode;
  BuiltList<String>? get synonyms;
  _i2.GMediaStatus? get status;
  GGetWatchingData_MediaListCollection_lists_entries_media_title? get title;
  static Serializer<GGetWatchingData_MediaListCollection_lists_entries_media>
      get serializer =>
          _$gGetWatchingDataMediaListCollectionListsEntriesMediaSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GGetWatchingData_MediaListCollection_lists_entries_media.serializer,
      this) as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists_entries_media? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists_entries_media.serializer,
          json);
}

abstract class GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
    implements
        Built<
            GGetWatchingData_MediaListCollection_lists_entries_media_coverImage,
            GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder>,
        _i3.GMediaDisplay_coverImage {
  GGetWatchingData_MediaListCollection_lists_entries_media_coverImage._();

  factory GGetWatchingData_MediaListCollection_lists_entries_media_coverImage(
          [Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
                      b)
              updates]) =
      _$GGetWatchingData_MediaListCollection_lists_entries_media_coverImage;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_lists_entries_media_coverImageBuilder
              b) =>
      b..G__typename = 'MediaCoverImage';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get extraLarge;
  String? get large;
  String? get color;
  static Serializer<
          GGetWatchingData_MediaListCollection_lists_entries_media_coverImage>
      get serializer =>
          _$gGetWatchingDataMediaListCollectionListsEntriesMediaCoverImageSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
          .serializer,
      this) as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists_entries_media_coverImage?
      fromJson(Map<String, dynamic> json) => _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
              .serializer,
          json);
}

abstract class GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
    implements
        Built<
            GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode,
            GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder>,
        _i3.GMediaDisplay_nextAiringEpisode {
  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode._();

  factory GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode(
          [Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
                      b)
              updates]) =
      _$GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisodeBuilder
              b) =>
      b..G__typename = 'AiringSchedule';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get episode;
  int get timeUntilAiring;
  static Serializer<
          GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode>
      get serializer =>
          _$gGetWatchingDataMediaListCollectionListsEntriesMediaNextAiringEpisodeSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
          .serializer,
      this) as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode?
      fromJson(Map<String, dynamic> json) => _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
              .serializer,
          json);
}

abstract class GGetWatchingData_MediaListCollection_lists_entries_media_title
    implements
        Built<GGetWatchingData_MediaListCollection_lists_entries_media_title,
            GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder>,
        _i3.GMediaDisplay_title {
  GGetWatchingData_MediaListCollection_lists_entries_media_title._();

  factory GGetWatchingData_MediaListCollection_lists_entries_media_title(
          [Function(
                  GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
                      b)
              updates]) =
      _$GGetWatchingData_MediaListCollection_lists_entries_media_title;

  static void _initializeBuilder(
          GGetWatchingData_MediaListCollection_lists_entries_media_titleBuilder
              b) =>
      b..G__typename = 'MediaTitle';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get userPreferred;
  String? get romaji;
  String? get english;
  static Serializer<
          GGetWatchingData_MediaListCollection_lists_entries_media_title>
      get serializer =>
          _$gGetWatchingDataMediaListCollectionListsEntriesMediaTitleSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GGetWatchingData_MediaListCollection_lists_entries_media_title.serializer,
      this) as Map<String, dynamic>);
  static GGetWatchingData_MediaListCollection_lists_entries_media_title?
      fromJson(Map<String, dynamic> json) => _i1.serializers.deserializeWith(
          GGetWatchingData_MediaListCollection_lists_entries_media_title
              .serializer,
          json);
}
