// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getPage.var.gql.g.dart';

abstract class GGetPageVars
    implements Built<GGetPageVars, GGetPageVarsBuilder> {
  GGetPageVars._();

  factory GGetPageVars([Function(GGetPageVarsBuilder b) updates]) =
      _$GGetPageVars;

  int? get page;
  String? get search;
  static Serializer<GGetPageVars> get serializer => _$gGetPageVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetPageVars.serializer, this)
          as Map<String, dynamic>);
  static GGetPageVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetPageVars.serializer, json);
}
