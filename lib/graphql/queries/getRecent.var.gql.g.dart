// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getRecent.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetRecentVars> _$gGetRecentVarsSerializer =
    new _$GGetRecentVarsSerializer();

class _$GGetRecentVarsSerializer
    implements StructuredSerializer<GGetRecentVars> {
  @override
  final Iterable<Type> types = const [GGetRecentVars, _$GGetRecentVars];
  @override
  final String wireName = 'GGetRecentVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetRecentVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.page;
    if (value != null) {
      result
        ..add('page')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GGetRecentVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetRecentVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'page':
          result.page = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetRecentVars extends GGetRecentVars {
  @override
  final int? page;

  factory _$GGetRecentVars([void Function(GGetRecentVarsBuilder)? updates]) =>
      (new GGetRecentVarsBuilder()..update(updates)).build();

  _$GGetRecentVars._({this.page}) : super._();

  @override
  GGetRecentVars rebuild(void Function(GGetRecentVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetRecentVarsBuilder toBuilder() =>
      new GGetRecentVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetRecentVars && page == other.page;
  }

  @override
  int get hashCode {
    return $jf($jc(0, page.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetRecentVars')..add('page', page))
        .toString();
  }
}

class GGetRecentVarsBuilder
    implements Builder<GGetRecentVars, GGetRecentVarsBuilder> {
  _$GGetRecentVars? _$v;

  int? _page;
  int? get page => _$this._page;
  set page(int? page) => _$this._page = page;

  GGetRecentVarsBuilder();

  GGetRecentVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _page = $v.page;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetRecentVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetRecentVars;
  }

  @override
  void update(void Function(GGetRecentVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetRecentVars build() {
    final _$result = _$v ?? new _$GGetRecentVars._(page: page);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
