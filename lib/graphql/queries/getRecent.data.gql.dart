// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getRecent.data.gql.g.dart';

abstract class GGetRecentData
    implements Built<GGetRecentData, GGetRecentDataBuilder> {
  GGetRecentData._();

  factory GGetRecentData([Function(GGetRecentDataBuilder b) updates]) =
      _$GGetRecentData;

  static void _initializeBuilder(GGetRecentDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetRecentData_Page? get Page;
  static Serializer<GGetRecentData> get serializer =>
      _$gGetRecentDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetRecentData.serializer, this)
          as Map<String, dynamic>);
  static GGetRecentData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetRecentData.serializer, json);
}

abstract class GGetRecentData_Page
    implements Built<GGetRecentData_Page, GGetRecentData_PageBuilder> {
  GGetRecentData_Page._();

  factory GGetRecentData_Page(
      [Function(GGetRecentData_PageBuilder b) updates]) = _$GGetRecentData_Page;

  static void _initializeBuilder(GGetRecentData_PageBuilder b) =>
      b..G__typename = 'Page';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GGetRecentData_Page_media>? get media;
  static Serializer<GGetRecentData_Page> get serializer =>
      _$gGetRecentDataPageSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetRecentData_Page.serializer, this)
          as Map<String, dynamic>);
  static GGetRecentData_Page? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetRecentData_Page.serializer, json);
}

abstract class GGetRecentData_Page_media
    implements
        Built<GGetRecentData_Page_media, GGetRecentData_Page_mediaBuilder>,
        _i2.GMediaDisplay {
  GGetRecentData_Page_media._();

  factory GGetRecentData_Page_media(
          [Function(GGetRecentData_Page_mediaBuilder b) updates]) =
      _$GGetRecentData_Page_media;

  static void _initializeBuilder(GGetRecentData_Page_mediaBuilder b) =>
      b..G__typename = 'Media';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetRecentData_Page_media_coverImage? get coverImage;
  int get id;
  int? get episodes;
  GGetRecentData_Page_media_nextAiringEpisode? get nextAiringEpisode;
  BuiltList<String>? get synonyms;
  _i3.GMediaStatus? get status;
  GGetRecentData_Page_media_title? get title;
  static Serializer<GGetRecentData_Page_media> get serializer =>
      _$gGetRecentDataPageMediaSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetRecentData_Page_media.serializer, this)
          as Map<String, dynamic>);
  static GGetRecentData_Page_media? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GGetRecentData_Page_media.serializer, json);
}

abstract class GGetRecentData_Page_media_coverImage
    implements
        Built<GGetRecentData_Page_media_coverImage,
            GGetRecentData_Page_media_coverImageBuilder>,
        _i2.GMediaDisplay_coverImage {
  GGetRecentData_Page_media_coverImage._();

  factory GGetRecentData_Page_media_coverImage(
          [Function(GGetRecentData_Page_media_coverImageBuilder b) updates]) =
      _$GGetRecentData_Page_media_coverImage;

  static void _initializeBuilder(
          GGetRecentData_Page_media_coverImageBuilder b) =>
      b..G__typename = 'MediaCoverImage';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get extraLarge;
  String? get large;
  String? get color;
  static Serializer<GGetRecentData_Page_media_coverImage> get serializer =>
      _$gGetRecentDataPageMediaCoverImageSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GGetRecentData_Page_media_coverImage.serializer, this)
      as Map<String, dynamic>);
  static GGetRecentData_Page_media_coverImage? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetRecentData_Page_media_coverImage.serializer, json);
}

abstract class GGetRecentData_Page_media_nextAiringEpisode
    implements
        Built<GGetRecentData_Page_media_nextAiringEpisode,
            GGetRecentData_Page_media_nextAiringEpisodeBuilder>,
        _i2.GMediaDisplay_nextAiringEpisode {
  GGetRecentData_Page_media_nextAiringEpisode._();

  factory GGetRecentData_Page_media_nextAiringEpisode(
      [Function(GGetRecentData_Page_media_nextAiringEpisodeBuilder b)
          updates]) = _$GGetRecentData_Page_media_nextAiringEpisode;

  static void _initializeBuilder(
          GGetRecentData_Page_media_nextAiringEpisodeBuilder b) =>
      b..G__typename = 'AiringSchedule';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get episode;
  int get timeUntilAiring;
  static Serializer<GGetRecentData_Page_media_nextAiringEpisode>
      get serializer => _$gGetRecentDataPageMediaNextAiringEpisodeSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GGetRecentData_Page_media_nextAiringEpisode.serializer, this)
      as Map<String, dynamic>);
  static GGetRecentData_Page_media_nextAiringEpisode? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetRecentData_Page_media_nextAiringEpisode.serializer, json);
}

abstract class GGetRecentData_Page_media_title
    implements
        Built<GGetRecentData_Page_media_title,
            GGetRecentData_Page_media_titleBuilder>,
        _i2.GMediaDisplay_title {
  GGetRecentData_Page_media_title._();

  factory GGetRecentData_Page_media_title(
          [Function(GGetRecentData_Page_media_titleBuilder b) updates]) =
      _$GGetRecentData_Page_media_title;

  static void _initializeBuilder(GGetRecentData_Page_media_titleBuilder b) =>
      b..G__typename = 'MediaTitle';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get userPreferred;
  String? get romaji;
  String? get english;
  static Serializer<GGetRecentData_Page_media_title> get serializer =>
      _$gGetRecentDataPageMediaTitleSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GGetRecentData_Page_media_title.serializer, this)
      as Map<String, dynamic>);
  static GGetRecentData_Page_media_title? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GGetRecentData_Page_media_title.serializer, json);
}
