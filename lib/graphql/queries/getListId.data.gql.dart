// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getListId.data.gql.g.dart';

abstract class GGetListIdData
    implements Built<GGetListIdData, GGetListIdDataBuilder> {
  GGetListIdData._();

  factory GGetListIdData([Function(GGetListIdDataBuilder b) updates]) =
      _$GGetListIdData;

  static void _initializeBuilder(GGetListIdDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetListIdData_MediaList? get MediaList;
  static Serializer<GGetListIdData> get serializer =>
      _$gGetListIdDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetListIdData.serializer, this)
          as Map<String, dynamic>);
  static GGetListIdData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetListIdData.serializer, json);
}

abstract class GGetListIdData_MediaList
    implements
        Built<GGetListIdData_MediaList, GGetListIdData_MediaListBuilder> {
  GGetListIdData_MediaList._();

  factory GGetListIdData_MediaList(
          [Function(GGetListIdData_MediaListBuilder b) updates]) =
      _$GGetListIdData_MediaList;

  static void _initializeBuilder(GGetListIdData_MediaListBuilder b) =>
      b..G__typename = 'MediaList';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  int get mediaId;
  static Serializer<GGetListIdData_MediaList> get serializer =>
      _$gGetListIdDataMediaListSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetListIdData_MediaList.serializer, this)
          as Map<String, dynamic>);
  static GGetListIdData_MediaList? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GGetListIdData_MediaList.serializer, json);
}
