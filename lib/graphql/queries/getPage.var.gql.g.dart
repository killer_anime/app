// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getPage.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetPageVars> _$gGetPageVarsSerializer =
    new _$GGetPageVarsSerializer();

class _$GGetPageVarsSerializer implements StructuredSerializer<GGetPageVars> {
  @override
  final Iterable<Type> types = const [GGetPageVars, _$GGetPageVars];
  @override
  final String wireName = 'GGetPageVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetPageVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.page;
    if (value != null) {
      result
        ..add('page')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.search;
    if (value != null) {
      result
        ..add('search')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GGetPageVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetPageVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'page':
          result.page = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'search':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetPageVars extends GGetPageVars {
  @override
  final int? page;
  @override
  final String? search;

  factory _$GGetPageVars([void Function(GGetPageVarsBuilder)? updates]) =>
      (new GGetPageVarsBuilder()..update(updates)).build();

  _$GGetPageVars._({this.page, this.search}) : super._();

  @override
  GGetPageVars rebuild(void Function(GGetPageVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetPageVarsBuilder toBuilder() => new GGetPageVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetPageVars &&
        page == other.page &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, page.hashCode), search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetPageVars')
          ..add('page', page)
          ..add('search', search))
        .toString();
  }
}

class GGetPageVarsBuilder
    implements Builder<GGetPageVars, GGetPageVarsBuilder> {
  _$GGetPageVars? _$v;

  int? _page;
  int? get page => _$this._page;
  set page(int? page) => _$this._page = page;

  String? _search;
  String? get search => _$this._search;
  set search(String? search) => _$this._search = search;

  GGetPageVarsBuilder();

  GGetPageVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _page = $v.page;
      _search = $v.search;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetPageVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetPageVars;
  }

  @override
  void update(void Function(GGetPageVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetPageVars build() {
    final _$result = _$v ?? new _$GGetPageVars._(page: page, search: search);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
