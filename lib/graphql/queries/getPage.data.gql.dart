// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getPage.data.gql.g.dart';

abstract class GGetPageData
    implements Built<GGetPageData, GGetPageDataBuilder> {
  GGetPageData._();

  factory GGetPageData([Function(GGetPageDataBuilder b) updates]) =
      _$GGetPageData;

  static void _initializeBuilder(GGetPageDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetPageData_Page? get Page;
  static Serializer<GGetPageData> get serializer => _$gGetPageDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetPageData.serializer, this)
          as Map<String, dynamic>);
  static GGetPageData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetPageData.serializer, json);
}

abstract class GGetPageData_Page
    implements Built<GGetPageData_Page, GGetPageData_PageBuilder> {
  GGetPageData_Page._();

  factory GGetPageData_Page([Function(GGetPageData_PageBuilder b) updates]) =
      _$GGetPageData_Page;

  static void _initializeBuilder(GGetPageData_PageBuilder b) =>
      b..G__typename = 'Page';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GGetPageData_Page_media>? get media;
  static Serializer<GGetPageData_Page> get serializer =>
      _$gGetPageDataPageSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetPageData_Page.serializer, this)
          as Map<String, dynamic>);
  static GGetPageData_Page? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetPageData_Page.serializer, json);
}

abstract class GGetPageData_Page_media
    implements
        Built<GGetPageData_Page_media, GGetPageData_Page_mediaBuilder>,
        _i2.GMediaDisplay {
  GGetPageData_Page_media._();

  factory GGetPageData_Page_media(
          [Function(GGetPageData_Page_mediaBuilder b) updates]) =
      _$GGetPageData_Page_media;

  static void _initializeBuilder(GGetPageData_Page_mediaBuilder b) =>
      b..G__typename = 'Media';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GGetPageData_Page_media_coverImage? get coverImage;
  int get id;
  int? get episodes;
  GGetPageData_Page_media_nextAiringEpisode? get nextAiringEpisode;
  BuiltList<String>? get synonyms;
  _i3.GMediaStatus? get status;
  GGetPageData_Page_media_title? get title;
  static Serializer<GGetPageData_Page_media> get serializer =>
      _$gGetPageDataPageMediaSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetPageData_Page_media.serializer, this)
          as Map<String, dynamic>);
  static GGetPageData_Page_media? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetPageData_Page_media.serializer, json);
}

abstract class GGetPageData_Page_media_coverImage
    implements
        Built<GGetPageData_Page_media_coverImage,
            GGetPageData_Page_media_coverImageBuilder>,
        _i2.GMediaDisplay_coverImage {
  GGetPageData_Page_media_coverImage._();

  factory GGetPageData_Page_media_coverImage(
          [Function(GGetPageData_Page_media_coverImageBuilder b) updates]) =
      _$GGetPageData_Page_media_coverImage;

  static void _initializeBuilder(GGetPageData_Page_media_coverImageBuilder b) =>
      b..G__typename = 'MediaCoverImage';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get extraLarge;
  String? get large;
  String? get color;
  static Serializer<GGetPageData_Page_media_coverImage> get serializer =>
      _$gGetPageDataPageMediaCoverImageSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GGetPageData_Page_media_coverImage.serializer, this)
      as Map<String, dynamic>);
  static GGetPageData_Page_media_coverImage? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GGetPageData_Page_media_coverImage.serializer, json);
}

abstract class GGetPageData_Page_media_nextAiringEpisode
    implements
        Built<GGetPageData_Page_media_nextAiringEpisode,
            GGetPageData_Page_media_nextAiringEpisodeBuilder>,
        _i2.GMediaDisplay_nextAiringEpisode {
  GGetPageData_Page_media_nextAiringEpisode._();

  factory GGetPageData_Page_media_nextAiringEpisode(
      [Function(GGetPageData_Page_media_nextAiringEpisodeBuilder b)
          updates]) = _$GGetPageData_Page_media_nextAiringEpisode;

  static void _initializeBuilder(
          GGetPageData_Page_media_nextAiringEpisodeBuilder b) =>
      b..G__typename = 'AiringSchedule';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get episode;
  int get timeUntilAiring;
  static Serializer<GGetPageData_Page_media_nextAiringEpisode> get serializer =>
      _$gGetPageDataPageMediaNextAiringEpisodeSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GGetPageData_Page_media_nextAiringEpisode.serializer, this)
      as Map<String, dynamic>);
  static GGetPageData_Page_media_nextAiringEpisode? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GGetPageData_Page_media_nextAiringEpisode.serializer, json);
}

abstract class GGetPageData_Page_media_title
    implements
        Built<GGetPageData_Page_media_title,
            GGetPageData_Page_media_titleBuilder>,
        _i2.GMediaDisplay_title {
  GGetPageData_Page_media_title._();

  factory GGetPageData_Page_media_title(
          [Function(GGetPageData_Page_media_titleBuilder b) updates]) =
      _$GGetPageData_Page_media_title;

  static void _initializeBuilder(GGetPageData_Page_media_titleBuilder b) =>
      b..G__typename = 'MediaTitle';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get userPreferred;
  String? get romaji;
  String? get english;
  static Serializer<GGetPageData_Page_media_title> get serializer =>
      _$gGetPageDataPageMediaTitleSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GGetPageData_Page_media_title.serializer, this) as Map<String, dynamic>);
  static GGetPageData_Page_media_title? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GGetPageData_Page_media_title.serializer, json);
}
