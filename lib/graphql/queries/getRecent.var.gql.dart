// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getRecent.var.gql.g.dart';

abstract class GGetRecentVars
    implements Built<GGetRecentVars, GGetRecentVarsBuilder> {
  GGetRecentVars._();

  factory GGetRecentVars([Function(GGetRecentVarsBuilder b) updates]) =
      _$GGetRecentVars;

  int? get page;
  static Serializer<GGetRecentVars> get serializer =>
      _$gGetRecentVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetRecentVars.serializer, this)
          as Map<String, dynamic>);
  static GGetRecentVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetRecentVars.serializer, json);
}
