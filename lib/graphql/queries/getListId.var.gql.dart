// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'getListId.var.gql.g.dart';

abstract class GGetListIdVars
    implements Built<GGetListIdVars, GGetListIdVarsBuilder> {
  GGetListIdVars._();

  factory GGetListIdVars([Function(GGetListIdVarsBuilder b) updates]) =
      _$GGetListIdVars;

  int? get mediaId;
  static Serializer<GGetListIdVars> get serializer =>
      _$gGetListIdVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GGetListIdVars.serializer, this)
          as Map<String, dynamic>);
  static GGetListIdVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GGetListIdVars.serializer, json);
}
