// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getWatching.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GGetWatchingVars> _$gGetWatchingVarsSerializer =
    new _$GGetWatchingVarsSerializer();

class _$GGetWatchingVarsSerializer
    implements StructuredSerializer<GGetWatchingVars> {
  @override
  final Iterable<Type> types = const [GGetWatchingVars, _$GGetWatchingVars];
  @override
  final String wireName = 'GGetWatchingVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GGetWatchingVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GGetWatchingVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GGetWatchingVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GGetWatchingVars extends GGetWatchingVars {
  @override
  final int? id;

  factory _$GGetWatchingVars(
          [void Function(GGetWatchingVarsBuilder)? updates]) =>
      (new GGetWatchingVarsBuilder()..update(updates)).build();

  _$GGetWatchingVars._({this.id}) : super._();

  @override
  GGetWatchingVars rebuild(void Function(GGetWatchingVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GGetWatchingVarsBuilder toBuilder() =>
      new GGetWatchingVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GGetWatchingVars && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GGetWatchingVars')..add('id', id))
        .toString();
  }
}

class GGetWatchingVarsBuilder
    implements Builder<GGetWatchingVars, GGetWatchingVarsBuilder> {
  _$GGetWatchingVars? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  GGetWatchingVarsBuilder();

  GGetWatchingVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GGetWatchingVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GGetWatchingVars;
  }

  @override
  void update(void Function(GGetWatchingVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GGetWatchingVars build() {
    final _$result = _$v ?? new _$GGetWatchingVars._(id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
