// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;
import 'package:killer_anime/graphql/queries/getPage.ast.gql.dart' as _i5;
import 'package:killer_anime/graphql/queries/getPage.data.gql.dart' as _i2;
import 'package:killer_anime/graphql/queries/getPage.var.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'getPage.req.gql.g.dart';

abstract class GGetPageReq
    implements
        Built<GGetPageReq, GGetPageReqBuilder>,
        _i1.OperationRequest<_i2.GGetPageData, _i3.GGetPageVars> {
  GGetPageReq._();

  factory GGetPageReq([Function(GGetPageReqBuilder b) updates]) = _$GGetPageReq;

  static void _initializeBuilder(GGetPageReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'GetPage')
    ..executeOnListen = true;
  _i3.GGetPageVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GGetPageData? Function(_i2.GGetPageData?, _i2.GGetPageData?)?
      get updateResult;
  _i2.GGetPageData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GGetPageData? parseData(Map<String, dynamic> json) =>
      _i2.GGetPageData.fromJson(json);
  static Serializer<GGetPageReq> get serializer => _$gGetPageReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GGetPageReq.serializer, this)
          as Map<String, dynamic>);
  static GGetPageReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GGetPageReq.serializer, json);
}
