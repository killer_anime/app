// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;
import 'package:killer_anime/graphql/queries/getWatching.ast.gql.dart' as _i5;
import 'package:killer_anime/graphql/queries/getWatching.data.gql.dart' as _i2;
import 'package:killer_anime/graphql/queries/getWatching.var.gql.dart' as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'getWatching.req.gql.g.dart';

abstract class GGetWatchingReq
    implements
        Built<GGetWatchingReq, GGetWatchingReqBuilder>,
        _i1.OperationRequest<_i2.GGetWatchingData, _i3.GGetWatchingVars> {
  GGetWatchingReq._();

  factory GGetWatchingReq([Function(GGetWatchingReqBuilder b) updates]) =
      _$GGetWatchingReq;

  static void _initializeBuilder(GGetWatchingReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'GetWatching')
    ..executeOnListen = true;
  _i3.GGetWatchingVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GGetWatchingData? Function(_i2.GGetWatchingData?, _i2.GGetWatchingData?)?
      get updateResult;
  _i2.GGetWatchingData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GGetWatchingData? parseData(Map<String, dynamic> json) =>
      _i2.GGetWatchingData.fromJson(json);
  static Serializer<GGetWatchingReq> get serializer =>
      _$gGetWatchingReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GGetWatchingReq.serializer, this)
          as Map<String, dynamic>);
  static GGetWatchingReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GGetWatchingReq.serializer, json);
}
