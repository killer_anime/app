// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;
import 'package:killer_anime/graphql/fragments/MediaDisplay.ast.gql.dart'
    as _i4;
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/fragments/MediaDisplay.var.gql.dart'
    as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'MediaDisplay.req.gql.g.dart';

abstract class GMediaDisplayReq
    implements
        Built<GMediaDisplayReq, GMediaDisplayReqBuilder>,
        _i1.FragmentRequest<_i2.GMediaDisplayData, _i3.GMediaDisplayVars> {
  GMediaDisplayReq._();

  factory GMediaDisplayReq([Function(GMediaDisplayReqBuilder b) updates]) =
      _$GMediaDisplayReq;

  static void _initializeBuilder(GMediaDisplayReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'MediaDisplay';
  _i3.GMediaDisplayVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GMediaDisplayData? parseData(Map<String, dynamic> json) =>
      _i2.GMediaDisplayData.fromJson(json);
  static Serializer<GMediaDisplayReq> get serializer =>
      _$gMediaDisplayReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GMediaDisplayReq.serializer, this)
          as Map<String, dynamic>);
  static GMediaDisplayReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GMediaDisplayReq.serializer, json);
}
