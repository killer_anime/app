// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.ast.gql.dart'
    as _i4;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.var.gql.dart'
    as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'MediaExternalLinks.req.gql.g.dart';

abstract class GMediaExternalLinksReq
    implements
        Built<GMediaExternalLinksReq, GMediaExternalLinksReqBuilder>,
        _i1.FragmentRequest<_i2.GMediaExternalLinksData,
            _i3.GMediaExternalLinksVars> {
  GMediaExternalLinksReq._();

  factory GMediaExternalLinksReq(
          [Function(GMediaExternalLinksReqBuilder b) updates]) =
      _$GMediaExternalLinksReq;

  static void _initializeBuilder(GMediaExternalLinksReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'MediaExternalLinks';
  _i3.GMediaExternalLinksVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GMediaExternalLinksData? parseData(Map<String, dynamic> json) =>
      _i2.GMediaExternalLinksData.fromJson(json);
  static Serializer<GMediaExternalLinksReq> get serializer =>
      _$gMediaExternalLinksReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GMediaExternalLinksReq.serializer, this)
          as Map<String, dynamic>);
  static GMediaExternalLinksReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GMediaExternalLinksReq.serializer, json);
}
