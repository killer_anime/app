// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'MediaListProgress.data.gql.g.dart';

abstract class GMediaListProgress {
  String get G__typename;
  int get id;
  int? get progress;
  Map<String, dynamic> toJson();
}

abstract class GMediaListProgressData
    implements
        Built<GMediaListProgressData, GMediaListProgressDataBuilder>,
        GMediaListProgress {
  GMediaListProgressData._();

  factory GMediaListProgressData(
          [Function(GMediaListProgressDataBuilder b) updates]) =
      _$GMediaListProgressData;

  static void _initializeBuilder(GMediaListProgressDataBuilder b) =>
      b..G__typename = 'MediaList';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  int? get progress;
  static Serializer<GMediaListProgressData> get serializer =>
      _$gMediaListProgressDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GMediaListProgressData.serializer, this)
          as Map<String, dynamic>);
  static GMediaListProgressData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GMediaListProgressData.serializer, json);
}
