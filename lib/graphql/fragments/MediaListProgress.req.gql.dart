// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;
import 'package:killer_anime/graphql/fragments/MediaListProgress.ast.gql.dart'
    as _i4;
import 'package:killer_anime/graphql/fragments/MediaListProgress.data.gql.dart'
    as _i2;
import 'package:killer_anime/graphql/fragments/MediaListProgress.var.gql.dart'
    as _i3;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i6;

part 'MediaListProgress.req.gql.g.dart';

abstract class GMediaListProgressReq
    implements
        Built<GMediaListProgressReq, GMediaListProgressReqBuilder>,
        _i1.FragmentRequest<_i2.GMediaListProgressData,
            _i3.GMediaListProgressVars> {
  GMediaListProgressReq._();

  factory GMediaListProgressReq(
          [Function(GMediaListProgressReqBuilder b) updates]) =
      _$GMediaListProgressReq;

  static void _initializeBuilder(GMediaListProgressReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'MediaListProgress';
  _i3.GMediaListProgressVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GMediaListProgressData? parseData(Map<String, dynamic> json) =>
      _i2.GMediaListProgressData.fromJson(json);
  static Serializer<GMediaListProgressReq> get serializer =>
      _$gMediaListProgressReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GMediaListProgressReq.serializer, this)
          as Map<String, dynamic>);
  static GMediaListProgressReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GMediaListProgressReq.serializer, json);
}
