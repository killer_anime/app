// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'MediaExternalLinks.var.gql.g.dart';

abstract class GMediaExternalLinksVars
    implements Built<GMediaExternalLinksVars, GMediaExternalLinksVarsBuilder> {
  GMediaExternalLinksVars._();

  factory GMediaExternalLinksVars(
          [Function(GMediaExternalLinksVarsBuilder b) updates]) =
      _$GMediaExternalLinksVars;

  static Serializer<GMediaExternalLinksVars> get serializer =>
      _$gMediaExternalLinksVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GMediaExternalLinksVars.serializer, this)
          as Map<String, dynamic>);
  static GMediaExternalLinksVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GMediaExternalLinksVars.serializer, json);
}
