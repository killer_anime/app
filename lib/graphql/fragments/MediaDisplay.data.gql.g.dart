// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaDisplay.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaDisplayData> _$gMediaDisplayDataSerializer =
    new _$GMediaDisplayDataSerializer();
Serializer<GMediaDisplayData_coverImage>
    _$gMediaDisplayDataCoverImageSerializer =
    new _$GMediaDisplayData_coverImageSerializer();
Serializer<GMediaDisplayData_nextAiringEpisode>
    _$gMediaDisplayDataNextAiringEpisodeSerializer =
    new _$GMediaDisplayData_nextAiringEpisodeSerializer();
Serializer<GMediaDisplayData_title> _$gMediaDisplayDataTitleSerializer =
    new _$GMediaDisplayData_titleSerializer();

class _$GMediaDisplayDataSerializer
    implements StructuredSerializer<GMediaDisplayData> {
  @override
  final Iterable<Type> types = const [GMediaDisplayData, _$GMediaDisplayData];
  @override
  final String wireName = 'GMediaDisplayData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GMediaDisplayData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.coverImage;
    if (value != null) {
      result
        ..add('coverImage')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GMediaDisplayData_coverImage)));
    }
    value = object.episodes;
    if (value != null) {
      result
        ..add('episodes')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.nextAiringEpisode;
    if (value != null) {
      result
        ..add('nextAiringEpisode')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(GMediaDisplayData_nextAiringEpisode)));
    }
    value = object.synonyms;
    if (value != null) {
      result
        ..add('synonyms')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(_i1.GMediaStatus)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GMediaDisplayData_title)));
    }
    return result;
  }

  @override
  GMediaDisplayData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaDisplayDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverImage':
          result.coverImage.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GMediaDisplayData_coverImage))!
              as GMediaDisplayData_coverImage);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'episodes':
          result.episodes = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'nextAiringEpisode':
          result.nextAiringEpisode.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GMediaDisplayData_nextAiringEpisode))!
              as GMediaDisplayData_nextAiringEpisode);
          break;
        case 'synonyms':
          result.synonyms.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(_i1.GMediaStatus))
              as _i1.GMediaStatus?;
          break;
        case 'title':
          result.title.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GMediaDisplayData_title))!
              as GMediaDisplayData_title);
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaDisplayData_coverImageSerializer
    implements StructuredSerializer<GMediaDisplayData_coverImage> {
  @override
  final Iterable<Type> types = const [
    GMediaDisplayData_coverImage,
    _$GMediaDisplayData_coverImage
  ];
  @override
  final String wireName = 'GMediaDisplayData_coverImage';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaDisplayData_coverImage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.extraLarge;
    if (value != null) {
      result
        ..add('extraLarge')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.large;
    if (value != null) {
      result
        ..add('large')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.color;
    if (value != null) {
      result
        ..add('color')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GMediaDisplayData_coverImage deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaDisplayData_coverImageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'extraLarge':
          result.extraLarge = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'large':
          result.large = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'color':
          result.color = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaDisplayData_nextAiringEpisodeSerializer
    implements StructuredSerializer<GMediaDisplayData_nextAiringEpisode> {
  @override
  final Iterable<Type> types = const [
    GMediaDisplayData_nextAiringEpisode,
    _$GMediaDisplayData_nextAiringEpisode
  ];
  @override
  final String wireName = 'GMediaDisplayData_nextAiringEpisode';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaDisplayData_nextAiringEpisode object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'episode',
      serializers.serialize(object.episode, specifiedType: const FullType(int)),
      'timeUntilAiring',
      serializers.serialize(object.timeUntilAiring,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  GMediaDisplayData_nextAiringEpisode deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaDisplayData_nextAiringEpisodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'episode':
          result.episode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'timeUntilAiring':
          result.timeUntilAiring = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaDisplayData_titleSerializer
    implements StructuredSerializer<GMediaDisplayData_title> {
  @override
  final Iterable<Type> types = const [
    GMediaDisplayData_title,
    _$GMediaDisplayData_title
  ];
  @override
  final String wireName = 'GMediaDisplayData_title';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaDisplayData_title object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.userPreferred;
    if (value != null) {
      result
        ..add('userPreferred')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.romaji;
    if (value != null) {
      result
        ..add('romaji')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.english;
    if (value != null) {
      result
        ..add('english')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GMediaDisplayData_title deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaDisplayData_titleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userPreferred':
          result.userPreferred = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'romaji':
          result.romaji = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'english':
          result.english = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaDisplayData extends GMediaDisplayData {
  @override
  final String G__typename;
  @override
  final GMediaDisplayData_coverImage? coverImage;
  @override
  final int id;
  @override
  final int? episodes;
  @override
  final GMediaDisplayData_nextAiringEpisode? nextAiringEpisode;
  @override
  final BuiltList<String>? synonyms;
  @override
  final _i1.GMediaStatus? status;
  @override
  final GMediaDisplayData_title? title;

  factory _$GMediaDisplayData(
          [void Function(GMediaDisplayDataBuilder)? updates]) =>
      (new GMediaDisplayDataBuilder()..update(updates)).build();

  _$GMediaDisplayData._(
      {required this.G__typename,
      this.coverImage,
      required this.id,
      this.episodes,
      this.nextAiringEpisode,
      this.synonyms,
      this.status,
      this.title})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaDisplayData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GMediaDisplayData', 'id');
  }

  @override
  GMediaDisplayData rebuild(void Function(GMediaDisplayDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaDisplayDataBuilder toBuilder() =>
      new GMediaDisplayDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaDisplayData &&
        G__typename == other.G__typename &&
        coverImage == other.coverImage &&
        id == other.id &&
        episodes == other.episodes &&
        nextAiringEpisode == other.nextAiringEpisode &&
        synonyms == other.synonyms &&
        status == other.status &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc(0, G__typename.hashCode),
                                coverImage.hashCode),
                            id.hashCode),
                        episodes.hashCode),
                    nextAiringEpisode.hashCode),
                synonyms.hashCode),
            status.hashCode),
        title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaDisplayData')
          ..add('G__typename', G__typename)
          ..add('coverImage', coverImage)
          ..add('id', id)
          ..add('episodes', episodes)
          ..add('nextAiringEpisode', nextAiringEpisode)
          ..add('synonyms', synonyms)
          ..add('status', status)
          ..add('title', title))
        .toString();
  }
}

class GMediaDisplayDataBuilder
    implements Builder<GMediaDisplayData, GMediaDisplayDataBuilder> {
  _$GMediaDisplayData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GMediaDisplayData_coverImageBuilder? _coverImage;
  GMediaDisplayData_coverImageBuilder get coverImage =>
      _$this._coverImage ??= new GMediaDisplayData_coverImageBuilder();
  set coverImage(GMediaDisplayData_coverImageBuilder? coverImage) =>
      _$this._coverImage = coverImage;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _episodes;
  int? get episodes => _$this._episodes;
  set episodes(int? episodes) => _$this._episodes = episodes;

  GMediaDisplayData_nextAiringEpisodeBuilder? _nextAiringEpisode;
  GMediaDisplayData_nextAiringEpisodeBuilder get nextAiringEpisode =>
      _$this._nextAiringEpisode ??=
          new GMediaDisplayData_nextAiringEpisodeBuilder();
  set nextAiringEpisode(
          GMediaDisplayData_nextAiringEpisodeBuilder? nextAiringEpisode) =>
      _$this._nextAiringEpisode = nextAiringEpisode;

  ListBuilder<String>? _synonyms;
  ListBuilder<String> get synonyms =>
      _$this._synonyms ??= new ListBuilder<String>();
  set synonyms(ListBuilder<String>? synonyms) => _$this._synonyms = synonyms;

  _i1.GMediaStatus? _status;
  _i1.GMediaStatus? get status => _$this._status;
  set status(_i1.GMediaStatus? status) => _$this._status = status;

  GMediaDisplayData_titleBuilder? _title;
  GMediaDisplayData_titleBuilder get title =>
      _$this._title ??= new GMediaDisplayData_titleBuilder();
  set title(GMediaDisplayData_titleBuilder? title) => _$this._title = title;

  GMediaDisplayDataBuilder() {
    GMediaDisplayData._initializeBuilder(this);
  }

  GMediaDisplayDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _coverImage = $v.coverImage?.toBuilder();
      _id = $v.id;
      _episodes = $v.episodes;
      _nextAiringEpisode = $v.nextAiringEpisode?.toBuilder();
      _synonyms = $v.synonyms?.toBuilder();
      _status = $v.status;
      _title = $v.title?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaDisplayData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaDisplayData;
  }

  @override
  void update(void Function(GMediaDisplayDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaDisplayData build() {
    _$GMediaDisplayData _$result;
    try {
      _$result = _$v ??
          new _$GMediaDisplayData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GMediaDisplayData', 'G__typename'),
              coverImage: _coverImage?.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GMediaDisplayData', 'id'),
              episodes: episodes,
              nextAiringEpisode: _nextAiringEpisode?.build(),
              synonyms: _synonyms?.build(),
              status: status,
              title: _title?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'coverImage';
        _coverImage?.build();

        _$failedField = 'nextAiringEpisode';
        _nextAiringEpisode?.build();
        _$failedField = 'synonyms';
        _synonyms?.build();

        _$failedField = 'title';
        _title?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GMediaDisplayData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GMediaDisplayData_coverImage extends GMediaDisplayData_coverImage {
  @override
  final String G__typename;
  @override
  final String? extraLarge;
  @override
  final String? large;
  @override
  final String? color;

  factory _$GMediaDisplayData_coverImage(
          [void Function(GMediaDisplayData_coverImageBuilder)? updates]) =>
      (new GMediaDisplayData_coverImageBuilder()..update(updates)).build();

  _$GMediaDisplayData_coverImage._(
      {required this.G__typename, this.extraLarge, this.large, this.color})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaDisplayData_coverImage', 'G__typename');
  }

  @override
  GMediaDisplayData_coverImage rebuild(
          void Function(GMediaDisplayData_coverImageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaDisplayData_coverImageBuilder toBuilder() =>
      new GMediaDisplayData_coverImageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaDisplayData_coverImage &&
        G__typename == other.G__typename &&
        extraLarge == other.extraLarge &&
        large == other.large &&
        color == other.color;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), extraLarge.hashCode),
            large.hashCode),
        color.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaDisplayData_coverImage')
          ..add('G__typename', G__typename)
          ..add('extraLarge', extraLarge)
          ..add('large', large)
          ..add('color', color))
        .toString();
  }
}

class GMediaDisplayData_coverImageBuilder
    implements
        Builder<GMediaDisplayData_coverImage,
            GMediaDisplayData_coverImageBuilder> {
  _$GMediaDisplayData_coverImage? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _extraLarge;
  String? get extraLarge => _$this._extraLarge;
  set extraLarge(String? extraLarge) => _$this._extraLarge = extraLarge;

  String? _large;
  String? get large => _$this._large;
  set large(String? large) => _$this._large = large;

  String? _color;
  String? get color => _$this._color;
  set color(String? color) => _$this._color = color;

  GMediaDisplayData_coverImageBuilder() {
    GMediaDisplayData_coverImage._initializeBuilder(this);
  }

  GMediaDisplayData_coverImageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _extraLarge = $v.extraLarge;
      _large = $v.large;
      _color = $v.color;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaDisplayData_coverImage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaDisplayData_coverImage;
  }

  @override
  void update(void Function(GMediaDisplayData_coverImageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaDisplayData_coverImage build() {
    final _$result = _$v ??
        new _$GMediaDisplayData_coverImage._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GMediaDisplayData_coverImage', 'G__typename'),
            extraLarge: extraLarge,
            large: large,
            color: color);
    replace(_$result);
    return _$result;
  }
}

class _$GMediaDisplayData_nextAiringEpisode
    extends GMediaDisplayData_nextAiringEpisode {
  @override
  final String G__typename;
  @override
  final int episode;
  @override
  final int timeUntilAiring;

  factory _$GMediaDisplayData_nextAiringEpisode(
          [void Function(GMediaDisplayData_nextAiringEpisodeBuilder)?
              updates]) =>
      (new GMediaDisplayData_nextAiringEpisodeBuilder()..update(updates))
          .build();

  _$GMediaDisplayData_nextAiringEpisode._(
      {required this.G__typename,
      required this.episode,
      required this.timeUntilAiring})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaDisplayData_nextAiringEpisode', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        episode, 'GMediaDisplayData_nextAiringEpisode', 'episode');
    BuiltValueNullFieldError.checkNotNull(timeUntilAiring,
        'GMediaDisplayData_nextAiringEpisode', 'timeUntilAiring');
  }

  @override
  GMediaDisplayData_nextAiringEpisode rebuild(
          void Function(GMediaDisplayData_nextAiringEpisodeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaDisplayData_nextAiringEpisodeBuilder toBuilder() =>
      new GMediaDisplayData_nextAiringEpisodeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaDisplayData_nextAiringEpisode &&
        G__typename == other.G__typename &&
        episode == other.episode &&
        timeUntilAiring == other.timeUntilAiring;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, G__typename.hashCode), episode.hashCode),
        timeUntilAiring.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaDisplayData_nextAiringEpisode')
          ..add('G__typename', G__typename)
          ..add('episode', episode)
          ..add('timeUntilAiring', timeUntilAiring))
        .toString();
  }
}

class GMediaDisplayData_nextAiringEpisodeBuilder
    implements
        Builder<GMediaDisplayData_nextAiringEpisode,
            GMediaDisplayData_nextAiringEpisodeBuilder> {
  _$GMediaDisplayData_nextAiringEpisode? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _episode;
  int? get episode => _$this._episode;
  set episode(int? episode) => _$this._episode = episode;

  int? _timeUntilAiring;
  int? get timeUntilAiring => _$this._timeUntilAiring;
  set timeUntilAiring(int? timeUntilAiring) =>
      _$this._timeUntilAiring = timeUntilAiring;

  GMediaDisplayData_nextAiringEpisodeBuilder() {
    GMediaDisplayData_nextAiringEpisode._initializeBuilder(this);
  }

  GMediaDisplayData_nextAiringEpisodeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _episode = $v.episode;
      _timeUntilAiring = $v.timeUntilAiring;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaDisplayData_nextAiringEpisode other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaDisplayData_nextAiringEpisode;
  }

  @override
  void update(
      void Function(GMediaDisplayData_nextAiringEpisodeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaDisplayData_nextAiringEpisode build() {
    final _$result = _$v ??
        new _$GMediaDisplayData_nextAiringEpisode._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GMediaDisplayData_nextAiringEpisode', 'G__typename'),
            episode: BuiltValueNullFieldError.checkNotNull(
                episode, 'GMediaDisplayData_nextAiringEpisode', 'episode'),
            timeUntilAiring: BuiltValueNullFieldError.checkNotNull(
                timeUntilAiring,
                'GMediaDisplayData_nextAiringEpisode',
                'timeUntilAiring'));
    replace(_$result);
    return _$result;
  }
}

class _$GMediaDisplayData_title extends GMediaDisplayData_title {
  @override
  final String G__typename;
  @override
  final String? userPreferred;
  @override
  final String? romaji;
  @override
  final String? english;

  factory _$GMediaDisplayData_title(
          [void Function(GMediaDisplayData_titleBuilder)? updates]) =>
      (new GMediaDisplayData_titleBuilder()..update(updates)).build();

  _$GMediaDisplayData_title._(
      {required this.G__typename,
      this.userPreferred,
      this.romaji,
      this.english})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaDisplayData_title', 'G__typename');
  }

  @override
  GMediaDisplayData_title rebuild(
          void Function(GMediaDisplayData_titleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaDisplayData_titleBuilder toBuilder() =>
      new GMediaDisplayData_titleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaDisplayData_title &&
        G__typename == other.G__typename &&
        userPreferred == other.userPreferred &&
        romaji == other.romaji &&
        english == other.english;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), userPreferred.hashCode),
            romaji.hashCode),
        english.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaDisplayData_title')
          ..add('G__typename', G__typename)
          ..add('userPreferred', userPreferred)
          ..add('romaji', romaji)
          ..add('english', english))
        .toString();
  }
}

class GMediaDisplayData_titleBuilder
    implements
        Builder<GMediaDisplayData_title, GMediaDisplayData_titleBuilder> {
  _$GMediaDisplayData_title? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _userPreferred;
  String? get userPreferred => _$this._userPreferred;
  set userPreferred(String? userPreferred) =>
      _$this._userPreferred = userPreferred;

  String? _romaji;
  String? get romaji => _$this._romaji;
  set romaji(String? romaji) => _$this._romaji = romaji;

  String? _english;
  String? get english => _$this._english;
  set english(String? english) => _$this._english = english;

  GMediaDisplayData_titleBuilder() {
    GMediaDisplayData_title._initializeBuilder(this);
  }

  GMediaDisplayData_titleBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _userPreferred = $v.userPreferred;
      _romaji = $v.romaji;
      _english = $v.english;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaDisplayData_title other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaDisplayData_title;
  }

  @override
  void update(void Function(GMediaDisplayData_titleBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaDisplayData_title build() {
    final _$result = _$v ??
        new _$GMediaDisplayData_title._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GMediaDisplayData_title', 'G__typename'),
            userPreferred: userPreferred,
            romaji: romaji,
            english: english);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
