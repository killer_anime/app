// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'MediaExternalLinks.data.gql.g.dart';

abstract class GMediaExternalLinks {
  String get G__typename;
  int get id;
  BuiltList<GMediaExternalLinks_externalLinks>? get externalLinks;
  Map<String, dynamic> toJson();
}

abstract class GMediaExternalLinks_externalLinks {
  String get G__typename;
  String get site;
  String get url;
  Map<String, dynamic> toJson();
}

abstract class GMediaExternalLinksData
    implements
        Built<GMediaExternalLinksData, GMediaExternalLinksDataBuilder>,
        GMediaExternalLinks {
  GMediaExternalLinksData._();

  factory GMediaExternalLinksData(
          [Function(GMediaExternalLinksDataBuilder b) updates]) =
      _$GMediaExternalLinksData;

  static void _initializeBuilder(GMediaExternalLinksDataBuilder b) =>
      b..G__typename = 'Media';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get id;
  BuiltList<GMediaExternalLinksData_externalLinks>? get externalLinks;
  static Serializer<GMediaExternalLinksData> get serializer =>
      _$gMediaExternalLinksDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GMediaExternalLinksData.serializer, this)
          as Map<String, dynamic>);
  static GMediaExternalLinksData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GMediaExternalLinksData.serializer, json);
}

abstract class GMediaExternalLinksData_externalLinks
    implements
        Built<GMediaExternalLinksData_externalLinks,
            GMediaExternalLinksData_externalLinksBuilder>,
        GMediaExternalLinks_externalLinks {
  GMediaExternalLinksData_externalLinks._();

  factory GMediaExternalLinksData_externalLinks(
          [Function(GMediaExternalLinksData_externalLinksBuilder b) updates]) =
      _$GMediaExternalLinksData_externalLinks;

  static void _initializeBuilder(
          GMediaExternalLinksData_externalLinksBuilder b) =>
      b..G__typename = 'MediaExternalLink';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get site;
  String get url;
  static Serializer<GMediaExternalLinksData_externalLinks> get serializer =>
      _$gMediaExternalLinksDataExternalLinksSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GMediaExternalLinksData_externalLinks.serializer, this)
      as Map<String, dynamic>);
  static GMediaExternalLinksData_externalLinks? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GMediaExternalLinksData_externalLinks.serializer, json);
}
