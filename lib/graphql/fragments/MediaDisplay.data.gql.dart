// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/schema.schema.gql.dart' as _i1;
import 'package:killer_anime/graphql/serializers.gql.dart' as _i2;

part 'MediaDisplay.data.gql.g.dart';

abstract class GMediaDisplay {
  String get G__typename;
  GMediaDisplay_coverImage? get coverImage;
  int get id;
  int? get episodes;
  GMediaDisplay_nextAiringEpisode? get nextAiringEpisode;
  BuiltList<String>? get synonyms;
  _i1.GMediaStatus? get status;
  GMediaDisplay_title? get title;
  Map<String, dynamic> toJson();
}

abstract class GMediaDisplay_coverImage {
  String get G__typename;
  String? get extraLarge;
  String? get large;
  String? get color;
  Map<String, dynamic> toJson();
}

abstract class GMediaDisplay_nextAiringEpisode {
  String get G__typename;
  int get episode;
  int get timeUntilAiring;
  Map<String, dynamic> toJson();
}

abstract class GMediaDisplay_title {
  String get G__typename;
  String? get userPreferred;
  String? get romaji;
  String? get english;
  Map<String, dynamic> toJson();
}

abstract class GMediaDisplayData
    implements
        Built<GMediaDisplayData, GMediaDisplayDataBuilder>,
        GMediaDisplay {
  GMediaDisplayData._();

  factory GMediaDisplayData([Function(GMediaDisplayDataBuilder b) updates]) =
      _$GMediaDisplayData;

  static void _initializeBuilder(GMediaDisplayDataBuilder b) =>
      b..G__typename = 'Media';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GMediaDisplayData_coverImage? get coverImage;
  int get id;
  int? get episodes;
  GMediaDisplayData_nextAiringEpisode? get nextAiringEpisode;
  BuiltList<String>? get synonyms;
  _i1.GMediaStatus? get status;
  GMediaDisplayData_title? get title;
  static Serializer<GMediaDisplayData> get serializer =>
      _$gMediaDisplayDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GMediaDisplayData.serializer, this)
          as Map<String, dynamic>);
  static GMediaDisplayData? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GMediaDisplayData.serializer, json);
}

abstract class GMediaDisplayData_coverImage
    implements
        Built<GMediaDisplayData_coverImage,
            GMediaDisplayData_coverImageBuilder>,
        GMediaDisplay_coverImage {
  GMediaDisplayData_coverImage._();

  factory GMediaDisplayData_coverImage(
          [Function(GMediaDisplayData_coverImageBuilder b) updates]) =
      _$GMediaDisplayData_coverImage;

  static void _initializeBuilder(GMediaDisplayData_coverImageBuilder b) =>
      b..G__typename = 'MediaCoverImage';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get extraLarge;
  String? get large;
  String? get color;
  static Serializer<GMediaDisplayData_coverImage> get serializer =>
      _$gMediaDisplayDataCoverImageSerializer;
  Map<String, dynamic> toJson() => (_i2.serializers.serializeWith(
      GMediaDisplayData_coverImage.serializer, this) as Map<String, dynamic>);
  static GMediaDisplayData_coverImage? fromJson(Map<String, dynamic> json) =>
      _i2.serializers
          .deserializeWith(GMediaDisplayData_coverImage.serializer, json);
}

abstract class GMediaDisplayData_nextAiringEpisode
    implements
        Built<GMediaDisplayData_nextAiringEpisode,
            GMediaDisplayData_nextAiringEpisodeBuilder>,
        GMediaDisplay_nextAiringEpisode {
  GMediaDisplayData_nextAiringEpisode._();

  factory GMediaDisplayData_nextAiringEpisode(
          [Function(GMediaDisplayData_nextAiringEpisodeBuilder b) updates]) =
      _$GMediaDisplayData_nextAiringEpisode;

  static void _initializeBuilder(
          GMediaDisplayData_nextAiringEpisodeBuilder b) =>
      b..G__typename = 'AiringSchedule';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int get episode;
  int get timeUntilAiring;
  static Serializer<GMediaDisplayData_nextAiringEpisode> get serializer =>
      _$gMediaDisplayDataNextAiringEpisodeSerializer;
  Map<String, dynamic> toJson() => (_i2.serializers
          .serializeWith(GMediaDisplayData_nextAiringEpisode.serializer, this)
      as Map<String, dynamic>);
  static GMediaDisplayData_nextAiringEpisode? fromJson(
          Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(
          GMediaDisplayData_nextAiringEpisode.serializer, json);
}

abstract class GMediaDisplayData_title
    implements
        Built<GMediaDisplayData_title, GMediaDisplayData_titleBuilder>,
        GMediaDisplay_title {
  GMediaDisplayData_title._();

  factory GMediaDisplayData_title(
          [Function(GMediaDisplayData_titleBuilder b) updates]) =
      _$GMediaDisplayData_title;

  static void _initializeBuilder(GMediaDisplayData_titleBuilder b) =>
      b..G__typename = 'MediaTitle';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String? get userPreferred;
  String? get romaji;
  String? get english;
  static Serializer<GMediaDisplayData_title> get serializer =>
      _$gMediaDisplayDataTitleSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GMediaDisplayData_title.serializer, this)
          as Map<String, dynamic>);
  static GMediaDisplayData_title? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GMediaDisplayData_title.serializer, json);
}
