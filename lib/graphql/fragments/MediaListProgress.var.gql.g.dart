// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaListProgress.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaListProgressVars> _$gMediaListProgressVarsSerializer =
    new _$GMediaListProgressVarsSerializer();

class _$GMediaListProgressVarsSerializer
    implements StructuredSerializer<GMediaListProgressVars> {
  @override
  final Iterable<Type> types = const [
    GMediaListProgressVars,
    _$GMediaListProgressVars
  ];
  @override
  final String wireName = 'GMediaListProgressVars';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaListProgressVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GMediaListProgressVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GMediaListProgressVarsBuilder().build();
  }
}

class _$GMediaListProgressVars extends GMediaListProgressVars {
  factory _$GMediaListProgressVars(
          [void Function(GMediaListProgressVarsBuilder)? updates]) =>
      (new GMediaListProgressVarsBuilder()..update(updates)).build();

  _$GMediaListProgressVars._() : super._();

  @override
  GMediaListProgressVars rebuild(
          void Function(GMediaListProgressVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaListProgressVarsBuilder toBuilder() =>
      new GMediaListProgressVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaListProgressVars;
  }

  @override
  int get hashCode {
    return 770195637;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GMediaListProgressVars').toString();
  }
}

class GMediaListProgressVarsBuilder
    implements Builder<GMediaListProgressVars, GMediaListProgressVarsBuilder> {
  _$GMediaListProgressVars? _$v;

  GMediaListProgressVarsBuilder();

  @override
  void replace(GMediaListProgressVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaListProgressVars;
  }

  @override
  void update(void Function(GMediaListProgressVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaListProgressVars build() {
    final _$result = _$v ?? new _$GMediaListProgressVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
