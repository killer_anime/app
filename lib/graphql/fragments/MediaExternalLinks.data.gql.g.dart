// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaExternalLinks.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaExternalLinksData> _$gMediaExternalLinksDataSerializer =
    new _$GMediaExternalLinksDataSerializer();
Serializer<GMediaExternalLinksData_externalLinks>
    _$gMediaExternalLinksDataExternalLinksSerializer =
    new _$GMediaExternalLinksData_externalLinksSerializer();

class _$GMediaExternalLinksDataSerializer
    implements StructuredSerializer<GMediaExternalLinksData> {
  @override
  final Iterable<Type> types = const [
    GMediaExternalLinksData,
    _$GMediaExternalLinksData
  ];
  @override
  final String wireName = 'GMediaExternalLinksData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaExternalLinksData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.externalLinks;
    if (value != null) {
      result
        ..add('externalLinks')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BuiltList, const [
              const FullType(GMediaExternalLinksData_externalLinks)
            ])));
    }
    return result;
  }

  @override
  GMediaExternalLinksData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaExternalLinksDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'externalLinks':
          result.externalLinks.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GMediaExternalLinksData_externalLinks)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaExternalLinksData_externalLinksSerializer
    implements StructuredSerializer<GMediaExternalLinksData_externalLinks> {
  @override
  final Iterable<Type> types = const [
    GMediaExternalLinksData_externalLinks,
    _$GMediaExternalLinksData_externalLinks
  ];
  @override
  final String wireName = 'GMediaExternalLinksData_externalLinks';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaExternalLinksData_externalLinks object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'site',
      serializers.serialize(object.site, specifiedType: const FullType(String)),
      'url',
      serializers.serialize(object.url, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GMediaExternalLinksData_externalLinks deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaExternalLinksData_externalLinksBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'site':
          result.site = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaExternalLinksData extends GMediaExternalLinksData {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final BuiltList<GMediaExternalLinksData_externalLinks>? externalLinks;

  factory _$GMediaExternalLinksData(
          [void Function(GMediaExternalLinksDataBuilder)? updates]) =>
      (new GMediaExternalLinksDataBuilder()..update(updates)).build();

  _$GMediaExternalLinksData._(
      {required this.G__typename, required this.id, this.externalLinks})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaExternalLinksData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GMediaExternalLinksData', 'id');
  }

  @override
  GMediaExternalLinksData rebuild(
          void Function(GMediaExternalLinksDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaExternalLinksDataBuilder toBuilder() =>
      new GMediaExternalLinksDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaExternalLinksData &&
        G__typename == other.G__typename &&
        id == other.id &&
        externalLinks == other.externalLinks;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, G__typename.hashCode), id.hashCode),
        externalLinks.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaExternalLinksData')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('externalLinks', externalLinks))
        .toString();
  }
}

class GMediaExternalLinksDataBuilder
    implements
        Builder<GMediaExternalLinksData, GMediaExternalLinksDataBuilder> {
  _$GMediaExternalLinksData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  ListBuilder<GMediaExternalLinksData_externalLinks>? _externalLinks;
  ListBuilder<GMediaExternalLinksData_externalLinks> get externalLinks =>
      _$this._externalLinks ??=
          new ListBuilder<GMediaExternalLinksData_externalLinks>();
  set externalLinks(
          ListBuilder<GMediaExternalLinksData_externalLinks>? externalLinks) =>
      _$this._externalLinks = externalLinks;

  GMediaExternalLinksDataBuilder() {
    GMediaExternalLinksData._initializeBuilder(this);
  }

  GMediaExternalLinksDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _externalLinks = $v.externalLinks?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaExternalLinksData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaExternalLinksData;
  }

  @override
  void update(void Function(GMediaExternalLinksDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaExternalLinksData build() {
    _$GMediaExternalLinksData _$result;
    try {
      _$result = _$v ??
          new _$GMediaExternalLinksData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GMediaExternalLinksData', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GMediaExternalLinksData', 'id'),
              externalLinks: _externalLinks?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'externalLinks';
        _externalLinks?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GMediaExternalLinksData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GMediaExternalLinksData_externalLinks
    extends GMediaExternalLinksData_externalLinks {
  @override
  final String G__typename;
  @override
  final String site;
  @override
  final String url;

  factory _$GMediaExternalLinksData_externalLinks(
          [void Function(GMediaExternalLinksData_externalLinksBuilder)?
              updates]) =>
      (new GMediaExternalLinksData_externalLinksBuilder()..update(updates))
          .build();

  _$GMediaExternalLinksData_externalLinks._(
      {required this.G__typename, required this.site, required this.url})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaExternalLinksData_externalLinks', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        site, 'GMediaExternalLinksData_externalLinks', 'site');
    BuiltValueNullFieldError.checkNotNull(
        url, 'GMediaExternalLinksData_externalLinks', 'url');
  }

  @override
  GMediaExternalLinksData_externalLinks rebuild(
          void Function(GMediaExternalLinksData_externalLinksBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaExternalLinksData_externalLinksBuilder toBuilder() =>
      new GMediaExternalLinksData_externalLinksBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaExternalLinksData_externalLinks &&
        G__typename == other.G__typename &&
        site == other.site &&
        url == other.url;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), site.hashCode), url.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaExternalLinksData_externalLinks')
          ..add('G__typename', G__typename)
          ..add('site', site)
          ..add('url', url))
        .toString();
  }
}

class GMediaExternalLinksData_externalLinksBuilder
    implements
        Builder<GMediaExternalLinksData_externalLinks,
            GMediaExternalLinksData_externalLinksBuilder> {
  _$GMediaExternalLinksData_externalLinks? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _site;
  String? get site => _$this._site;
  set site(String? site) => _$this._site = site;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  GMediaExternalLinksData_externalLinksBuilder() {
    GMediaExternalLinksData_externalLinks._initializeBuilder(this);
  }

  GMediaExternalLinksData_externalLinksBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _site = $v.site;
      _url = $v.url;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaExternalLinksData_externalLinks other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaExternalLinksData_externalLinks;
  }

  @override
  void update(
      void Function(GMediaExternalLinksData_externalLinksBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaExternalLinksData_externalLinks build() {
    final _$result = _$v ??
        new _$GMediaExternalLinksData_externalLinks._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GMediaExternalLinksData_externalLinks', 'G__typename'),
            site: BuiltValueNullFieldError.checkNotNull(
                site, 'GMediaExternalLinksData_externalLinks', 'site'),
            url: BuiltValueNullFieldError.checkNotNull(
                url, 'GMediaExternalLinksData_externalLinks', 'url'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
