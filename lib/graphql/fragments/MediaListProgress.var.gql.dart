// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'MediaListProgress.var.gql.g.dart';

abstract class GMediaListProgressVars
    implements Built<GMediaListProgressVars, GMediaListProgressVarsBuilder> {
  GMediaListProgressVars._();

  factory GMediaListProgressVars(
          [Function(GMediaListProgressVarsBuilder b) updates]) =
      _$GMediaListProgressVars;

  static Serializer<GMediaListProgressVars> get serializer =>
      _$gMediaListProgressVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GMediaListProgressVars.serializer, this)
          as Map<String, dynamic>);
  static GMediaListProgressVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GMediaListProgressVars.serializer, json);
}
