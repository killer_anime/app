// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaDisplay.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaDisplayVars> _$gMediaDisplayVarsSerializer =
    new _$GMediaDisplayVarsSerializer();

class _$GMediaDisplayVarsSerializer
    implements StructuredSerializer<GMediaDisplayVars> {
  @override
  final Iterable<Type> types = const [GMediaDisplayVars, _$GMediaDisplayVars];
  @override
  final String wireName = 'GMediaDisplayVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GMediaDisplayVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GMediaDisplayVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GMediaDisplayVarsBuilder().build();
  }
}

class _$GMediaDisplayVars extends GMediaDisplayVars {
  factory _$GMediaDisplayVars(
          [void Function(GMediaDisplayVarsBuilder)? updates]) =>
      (new GMediaDisplayVarsBuilder()..update(updates)).build();

  _$GMediaDisplayVars._() : super._();

  @override
  GMediaDisplayVars rebuild(void Function(GMediaDisplayVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaDisplayVarsBuilder toBuilder() =>
      new GMediaDisplayVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaDisplayVars;
  }

  @override
  int get hashCode {
    return 215527664;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GMediaDisplayVars').toString();
  }
}

class GMediaDisplayVarsBuilder
    implements Builder<GMediaDisplayVars, GMediaDisplayVarsBuilder> {
  _$GMediaDisplayVars? _$v;

  GMediaDisplayVarsBuilder();

  @override
  void replace(GMediaDisplayVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaDisplayVars;
  }

  @override
  void update(void Function(GMediaDisplayVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaDisplayVars build() {
    final _$result = _$v ?? new _$GMediaDisplayVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
