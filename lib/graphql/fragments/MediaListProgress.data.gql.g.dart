// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaListProgress.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaListProgressData> _$gMediaListProgressDataSerializer =
    new _$GMediaListProgressDataSerializer();

class _$GMediaListProgressDataSerializer
    implements StructuredSerializer<GMediaListProgressData> {
  @override
  final Iterable<Type> types = const [
    GMediaListProgressData,
    _$GMediaListProgressData
  ];
  @override
  final String wireName = 'GMediaListProgressData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaListProgressData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.progress;
    if (value != null) {
      result
        ..add('progress')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GMediaListProgressData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GMediaListProgressDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'progress':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GMediaListProgressData extends GMediaListProgressData {
  @override
  final String G__typename;
  @override
  final int id;
  @override
  final int? progress;

  factory _$GMediaListProgressData(
          [void Function(GMediaListProgressDataBuilder)? updates]) =>
      (new GMediaListProgressDataBuilder()..update(updates)).build();

  _$GMediaListProgressData._(
      {required this.G__typename, required this.id, this.progress})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GMediaListProgressData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GMediaListProgressData', 'id');
  }

  @override
  GMediaListProgressData rebuild(
          void Function(GMediaListProgressDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaListProgressDataBuilder toBuilder() =>
      new GMediaListProgressDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaListProgressData &&
        G__typename == other.G__typename &&
        id == other.id &&
        progress == other.progress;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), progress.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GMediaListProgressData')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('progress', progress))
        .toString();
  }
}

class GMediaListProgressDataBuilder
    implements Builder<GMediaListProgressData, GMediaListProgressDataBuilder> {
  _$GMediaListProgressData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _progress;
  int? get progress => _$this._progress;
  set progress(int? progress) => _$this._progress = progress;

  GMediaListProgressDataBuilder() {
    GMediaListProgressData._initializeBuilder(this);
  }

  GMediaListProgressDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _progress = $v.progress;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GMediaListProgressData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaListProgressData;
  }

  @override
  void update(void Function(GMediaListProgressDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaListProgressData build() {
    final _$result = _$v ??
        new _$GMediaListProgressData._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GMediaListProgressData', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GMediaListProgressData', 'id'),
            progress: progress);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
