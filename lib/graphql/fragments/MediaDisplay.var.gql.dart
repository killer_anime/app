// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:killer_anime/graphql/serializers.gql.dart' as _i1;

part 'MediaDisplay.var.gql.g.dart';

abstract class GMediaDisplayVars
    implements Built<GMediaDisplayVars, GMediaDisplayVarsBuilder> {
  GMediaDisplayVars._();

  factory GMediaDisplayVars([Function(GMediaDisplayVarsBuilder b) updates]) =
      _$GMediaDisplayVars;

  static Serializer<GMediaDisplayVars> get serializer =>
      _$gMediaDisplayVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GMediaDisplayVars.serializer, this)
          as Map<String, dynamic>);
  static GMediaDisplayVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GMediaDisplayVars.serializer, json);
}
