// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MediaExternalLinks.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GMediaExternalLinksVars> _$gMediaExternalLinksVarsSerializer =
    new _$GMediaExternalLinksVarsSerializer();

class _$GMediaExternalLinksVarsSerializer
    implements StructuredSerializer<GMediaExternalLinksVars> {
  @override
  final Iterable<Type> types = const [
    GMediaExternalLinksVars,
    _$GMediaExternalLinksVars
  ];
  @override
  final String wireName = 'GMediaExternalLinksVars';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GMediaExternalLinksVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GMediaExternalLinksVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GMediaExternalLinksVarsBuilder().build();
  }
}

class _$GMediaExternalLinksVars extends GMediaExternalLinksVars {
  factory _$GMediaExternalLinksVars(
          [void Function(GMediaExternalLinksVarsBuilder)? updates]) =>
      (new GMediaExternalLinksVarsBuilder()..update(updates)).build();

  _$GMediaExternalLinksVars._() : super._();

  @override
  GMediaExternalLinksVars rebuild(
          void Function(GMediaExternalLinksVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GMediaExternalLinksVarsBuilder toBuilder() =>
      new GMediaExternalLinksVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GMediaExternalLinksVars;
  }

  @override
  int get hashCode {
    return 577202226;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GMediaExternalLinksVars').toString();
  }
}

class GMediaExternalLinksVarsBuilder
    implements
        Builder<GMediaExternalLinksVars, GMediaExternalLinksVarsBuilder> {
  _$GMediaExternalLinksVars? _$v;

  GMediaExternalLinksVarsBuilder();

  @override
  void replace(GMediaExternalLinksVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GMediaExternalLinksVars;
  }

  @override
  void update(void Function(GMediaExternalLinksVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GMediaExternalLinksVars build() {
    final _$result = _$v ?? new _$GMediaExternalLinksVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
