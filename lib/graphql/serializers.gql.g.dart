// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(GActivitySort.serializer)
      ..add(GActivityType.serializer)
      ..add(GAiringScheduleInput.serializer)
      ..add(GAiringSort.serializer)
      ..add(GAniChartHighlightInput.serializer)
      ..add(GCharacterNameInput.serializer)
      ..add(GCharacterRole.serializer)
      ..add(GCharacterSort.serializer)
      ..add(GCountryCode.serializer)
      ..add(GFuzzyDateInput.serializer)
      ..add(GFuzzyDateInt.serializer)
      ..add(GGetListIdData.serializer)
      ..add(GGetListIdData_MediaList.serializer)
      ..add(GGetListIdReq.serializer)
      ..add(GGetListIdVars.serializer)
      ..add(GGetPageData.serializer)
      ..add(GGetPageData_Page.serializer)
      ..add(GGetPageData_Page_media.serializer)
      ..add(GGetPageData_Page_media_coverImage.serializer)
      ..add(GGetPageData_Page_media_nextAiringEpisode.serializer)
      ..add(GGetPageData_Page_media_title.serializer)
      ..add(GGetPageReq.serializer)
      ..add(GGetPageVars.serializer)
      ..add(GGetRecentData.serializer)
      ..add(GGetRecentData_Page.serializer)
      ..add(GGetRecentData_Page_media.serializer)
      ..add(GGetRecentData_Page_media_coverImage.serializer)
      ..add(GGetRecentData_Page_media_nextAiringEpisode.serializer)
      ..add(GGetRecentData_Page_media_title.serializer)
      ..add(GGetRecentReq.serializer)
      ..add(GGetRecentVars.serializer)
      ..add(GGetWatchingData.serializer)
      ..add(GGetWatchingData_MediaListCollection.serializer)
      ..add(GGetWatchingData_MediaListCollection_lists.serializer)
      ..add(GGetWatchingData_MediaListCollection_lists_entries.serializer)
      ..add(GGetWatchingData_MediaListCollection_lists_entries_media.serializer)
      ..add(GGetWatchingData_MediaListCollection_lists_entries_media_coverImage
          .serializer)
      ..add(
          GGetWatchingData_MediaListCollection_lists_entries_media_nextAiringEpisode
              .serializer)
      ..add(GGetWatchingData_MediaListCollection_lists_entries_media_title
          .serializer)
      ..add(GGetWatchingData_MediaListCollection_user.serializer)
      ..add(GGetWatchingData_Viewer.serializer)
      ..add(GGetWatchingReq.serializer)
      ..add(GGetWatchingVars.serializer)
      ..add(GJson.serializer)
      ..add(GLikeableType.serializer)
      ..add(GMediaDisplayData.serializer)
      ..add(GMediaDisplayData_coverImage.serializer)
      ..add(GMediaDisplayData_nextAiringEpisode.serializer)
      ..add(GMediaDisplayData_title.serializer)
      ..add(GMediaDisplayReq.serializer)
      ..add(GMediaDisplayVars.serializer)
      ..add(GMediaExternalLinkInput.serializer)
      ..add(GMediaExternalLinksData.serializer)
      ..add(GMediaExternalLinksData_externalLinks.serializer)
      ..add(GMediaExternalLinksReq.serializer)
      ..add(GMediaExternalLinksVars.serializer)
      ..add(GMediaFormat.serializer)
      ..add(GMediaListOptionsInput.serializer)
      ..add(GMediaListProgressData.serializer)
      ..add(GMediaListProgressReq.serializer)
      ..add(GMediaListProgressVars.serializer)
      ..add(GMediaListSort.serializer)
      ..add(GMediaListStatus.serializer)
      ..add(GMediaRankType.serializer)
      ..add(GMediaRelation.serializer)
      ..add(GMediaSeason.serializer)
      ..add(GMediaSort.serializer)
      ..add(GMediaSource.serializer)
      ..add(GMediaStatus.serializer)
      ..add(GMediaTitleInput.serializer)
      ..add(GMediaTrendSort.serializer)
      ..add(GMediaType.serializer)
      ..add(GModActionType.serializer)
      ..add(GModRole.serializer)
      ..add(GNotificationOptionInput.serializer)
      ..add(GNotificationType.serializer)
      ..add(GRecommendationRating.serializer)
      ..add(GRecommendationSort.serializer)
      ..add(GReviewRating.serializer)
      ..add(GReviewSort.serializer)
      ..add(GRevisionHistoryAction.serializer)
      ..add(GScoreFormat.serializer)
      ..add(GSiteTrendSort.serializer)
      ..add(GStaffLanguage.serializer)
      ..add(GStaffNameInput.serializer)
      ..add(GStaffSort.serializer)
      ..add(GStudioSort.serializer)
      ..add(GSubmissionSort.serializer)
      ..add(GSubmissionStatus.serializer)
      ..add(GThreadCommentSort.serializer)
      ..add(GThreadSort.serializer)
      ..add(GUpdateProgressData.serializer)
      ..add(GUpdateProgressData_SaveMediaListEntry.serializer)
      ..add(GUpdateProgressReq.serializer)
      ..add(GUpdateProgressVars.serializer)
      ..add(GUserSort.serializer)
      ..add(GUserStaffNameLanguage.serializer)
      ..add(GUserStatisticsSort.serializer)
      ..add(GUserTitleLanguage.serializer)
      ..add(GappConfigData.serializer)
      ..add(GappConfigReq.serializer)
      ..add(GappConfigVars.serializer)
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GGetPageData_Page_media)]),
          () => new ListBuilder<GGetPageData_Page_media>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GGetRecentData_Page_media)]),
          () => new ListBuilder<GGetRecentData_Page_media>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(GGetWatchingData_MediaListCollection_lists)
          ]),
          () => new ListBuilder<GGetWatchingData_MediaListCollection_lists>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [
            const FullType(GGetWatchingData_MediaListCollection_lists_entries)
          ]),
          () => new ListBuilder<
              GGetWatchingData_MediaListCollection_lists_entries>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GMediaExternalLinksData_externalLinks)]),
          () => new ListBuilder<GMediaExternalLinksData_externalLinks>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
