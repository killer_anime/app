import 'package:url_launcher/url_launcher.dart';

final identifier = '6140';

Future openAuth() async {
  await launch("https://anilist.co/api/v2/oauth/authorize?client_id=6140&response_type=token", forceSafariVC: false);
}