import 'dart:async';

import 'package:ferry/typed_links.dart';
import "package:gql_exec/gql_exec.dart";
import 'package:gql_http_link/gql_http_link.dart';

class HttpAuthLink extends Link {
  FutureOr<String?> Function() getToken;
  String graphQLEndpoint;

  late Link _link;
  late FutureOr<Request> Function(Request request) transformer;

  HttpAuthLink(
    this.getToken,
    this.graphQLEndpoint,
  ) {
    _link = Link.from([
      HttpLink(graphQLEndpoint),
    ]);
    transformer = HttpAuthLink.transform(getToken);
  }

  static FutureOr<Request> Function(Request request) transform(
      FutureOr<String?> Function() getToken,
      ) =>
          (Request request) async {
        final token = await getToken();
        return request.updateContextEntry<HttpLinkHeaders>(
              (headers) => HttpLinkHeaders(
            headers: <String, String>{
              ...headers?.headers ?? <String, String>{},
              if (token != null && token.isNotEmpty) 'Authorization': token,
            },
          ),
        );
      };

  @override
  Stream<Response> request(
    Request request, [
    NextLink? forward,
  ]) async* {
    final req = await transformer(request);
    yield* _link.request(req, forward);

  }
}
