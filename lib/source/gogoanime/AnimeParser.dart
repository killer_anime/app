import 'package:html/dom.dart';
import 'BasicParser.dart';

class Source {
  String? title;
  List<String> tags;
  Uri link;

  Source(this.title, this.link, { this.tags = const [] });

  Source.linkOnly(this.link, { this.tags = const [] });
}

class AnimeParser extends BasicParser {
  AnimeParser(Uri link) : super(link);
  final regex = RegExp("\\\((.+?)\\\)");

  @override
  List<Source> parseHTML(Document? body) {
    List<Source> list = [];

    if (body != null) {
      final div = body.getElementsByClassName('items');
      // There should only one of the list
      if (div.length == 1) {
        final items = div.first;
        // check if items contains an error message
        if (!items.text.contains('Sorry')) {
          // It can be an empty list
          if (items.hasChildNodes()) {
            items.nodes.forEach((element) {
              // Only parse elements, no Text
              if (element is Element) {
                var link =
                    element.getElementsByClassName('name').first.firstChild;
                if (link != null) {
                  if (link.attributes['title'] != null) {
                    var title = link.attributes['title']?.trim();
                    var tags = regex.allMatches(title!).map((e) => e[1]!).toList();
                    title = title.replaceAll(regex, "");
                    list.add(Source(title,
                        Uri.parse(link.attributes['href']!), tags: tags));
                  }else if(link.attributes['href'] != null){
                    list.add(Source.linkOnly(Uri.parse('https://gogoanime.pe' + link.attributes['href']!)));
                  }
                }
              }
            });
          }
        }
      }
    }

    return list;
  }
}
