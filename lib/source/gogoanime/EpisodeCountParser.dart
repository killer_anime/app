import 'package:html/dom.dart';

import 'BasicParser.dart';

class EpisodeCountParser extends BasicParser {
  EpisodeCountParser(Uri link) : super(link);
  String? animeId;
  String firstEpisode = "0";
  String lastEpisode = "0";

  @override
  void parseHTML(Document? body) {
    final movieClass = body?.getElementById('movie_id');
    animeId = movieClass?.attributes['value'];
    final episodeClass = body
        ?.getElementById('episode_page')
        ?.getElementsByTagName("a")
        .map((e) => e.text);
    if(episodeClass != null) {
      firstEpisode = episodeClass.first.split("-")[0];
      lastEpisode = episodeClass.last.split("-")[1];
    }else{
      throw "Can't find episode class";
    }
  }
}
