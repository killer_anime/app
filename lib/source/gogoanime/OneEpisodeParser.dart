import 'package:html/dom.dart';

import 'BasicParser.dart';

class OneEpisodeInfo {
  String? currentEpisode;


  List<String> servers = [];

  OneEpisodeInfo(Element? e) {
    // Get all video servers
    currentEpisode = e?.nodes[1].text;
    final server = e?.getElementsByClassName('anime_muti_link').first;
    final serverList = server?.nodes[0];
    serverList?.nodes.forEach((element) {
      if (element.runtimeType == Element) {
        final node = element.nodes[1];
        if (node.attributes['data-video'] != null) {
          servers.add(node.attributes['data-video']!);
        }
      }
    });

    final episode =
        e?.getElementsByClassName('anime_video_body_episodes').first;

    print(episode?.text);
  }
}

/// This parses `VideoServer` and some other info for a `single` episode
class OneEpisodeParser extends BasicParser {
  OneEpisodeParser(
    Uri link,
  ) : super(link);

  @override
  OneEpisodeInfo parseHTML(Document? body) {
    final div = body?.getElementsByClassName('anime_video_body');
    var episodeInfo = OneEpisodeInfo(div?.first);

    return episodeInfo;
  }
}
