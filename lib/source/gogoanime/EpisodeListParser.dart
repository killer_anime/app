import 'package:html/dom.dart';

import 'BasicParser.dart';

class EpisodeListParser extends BasicParser {
  // a valid link, http://gogoanimee.net//load-list-episode?ep_start=0&ep_end=23&id=8634&default_ep=0
  EpisodeListParser(
    Uri link
  ) : super(link);

  @override
  List<String> parseHTML(Document? body) {
    List<String> list = [];
    final episodeClass = body?.getElementById('episode_related')?.getElementsByClassName("name");
    episodeClass?.forEach((element) {
      if (element.runtimeType == Element) {
        var title = element.nodes[1].text?.trim();
        if(title != null) {
          list.add(title);
        }
      }
    });
    return list;
  }
}
