import 'dart:async';
import 'dart:io';

import 'package:ferry/ferry.dart';
import 'package:ferry_hive_store/ferry_hive_store.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:killer_anime/pages/global_search.dart';
import 'package:killer_anime/pages/watching.dart';
import 'package:uni_links/uni_links.dart';
import 'package:get_it/get_it.dart';

import 'elements/load_episode_source.dart';
import 'http_auth_link.dart';
import 'pages/home.dart';

String? loggedIn() {
  final tokenBox = GetIt.I<Box<String>>(instanceName: "token");

  var token = tokenBox.get('access_token');
  if (token == null || token.isEmpty || JwtDecoder.isExpired(token)) {
    tokenBox.clear();
    return null;
  }
  return token;
}

Future<Client> initClient(FutureOr<String?> Function() getToken) async {
  final box = await Hive.openBox("graphql");

  final store = HiveStore(box);

  final cache = Cache(store: store);

  final link = HttpAuthLink(getToken, 'https://graphql.anilist.co');

  final client = Client(
    link: link,
    cache: cache,
  );

  // box.toMap().forEach((key, value) {
  //   print("$key -> $value");
  // });

  return client;
}

final getIt = GetIt.instance;

void main() async {
  await Hive.initFlutter();
  var tokenBox = await Hive.openBox<String>('token');
  getIt.registerSingleton<Box<String>>(tokenBox, instanceName: "token");

  if (kDebugMode) {
    try {
      if (Platform.isLinux) {
        var t =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzMTNhYzg0ZWJkMzViZmU0MGU5YWFiYmE0ODgxMTZhOWY5YWNhMWQ1NGQwOTdjZDE3MTU5ZWVlYjBhOTZkMWU4MTkzNTJkZDdjYzQxNzlhIn0.eyJhdWQiOiI2MTQwIiwianRpIjoiYTMxM2FjODRlYmQzNWJmZTQwZTlhYWJiYTQ4ODExNmE5ZjlhY2ExZDU0ZDA5N2NkMTcxNTllZWViMGE5NmQxZTgxOTM1MmRkN2NjNDE3OWEiLCJpYXQiOjE2Mjc0MjEwNTcsIm5iZiI6MTYyNzQyMTA1NywiZXhwIjoxNjU4OTU3MDU3LCJzdWIiOiI1NDczNDQ2Iiwic2NvcGVzIjpbXX0.bR7gHrUkHcrWrjZh2AvDRHtOKd9hEwX7ATHyCEfuy3ZzKBjqzTvfTGxpZVQAKW9cexqpEw6-2ueGub2oqvm7cZ-YryHAToxoB35Ri-bT5KrRSAYeK-3VjSQ8OQQwtD0cEDeIgl9abkxs-QhHe340K6uGimSCI8RQ-_89phqPEUiJ8iK47OrOBAJ_LgPsQlg87MicbVLhqVddxoEV4QzERyDo34CLh7H2bD0m62IFG-RR5iHtvScXq9fAf9MBGiuae60KE3UXUWCCYZxfCSlfZdu1b3p54y6w4gVQd41EuNsZ8k3QBiccfPr4540CQacuFbgGizEdPj1K2Wdf8bSBgQ2i0-7y3CY6Tza60RqgZn6d9PZruYAU_wPkhMOb5jSeqI_OUgw1K94KfWyiXincUfHwpEr2arS8AcEPYegLciQ2FrYICfxC9Ysf1KccmTNo0znmplL3uI28xAR5GNHr7kj4RnIDiNMxAYW25dgPM2lqLyOtIB4-QG7hLbd3dUR9gW89GIADty0YIc1d9_4ZU0DoSFyqpPRssui5OGfOeFeTmCei338bBWuSnT0tNTwo9034P-FGsCikGz2wN-9CnMK_bohgP0nxm21h5vdXb_8D-GPvI5KHnT6Pd1GnqctqG-aOCfvHTSVwNeaprgSF6p7vf12vlXLAafAFKiy-ciA";
        tokenBox.put('access_token', t);
        //tokenBox.delete('access_token');
      }
    } catch (error) {
      print("cant find .dev.env file: $error");
    }
    if (tokenBox.containsKey('access_token')) {
      print("token: ${tokenBox.get('access_token')}");
      print(JwtDecoder.decode(tokenBox.get('access_token')!));
      print(JwtDecoder.isExpired(tokenBox.get('access_token')!));
    }
  }
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  final client = await initClient(() async {
    final token = loggedIn();
    if (token != null) {
      return "Bearer $token";
    } else {
      // final state = navigatorKey.currentState;
      // if (state != null) {
      //   state.pushReplacementNamed("/welcome");
      // }
      return "";
    }
  });

  getIt.registerSingleton<Client>(client);
  getIt.registerSingleton<GlobalKey<NavigatorState>>(navigatorKey);

  runApp(MainApp());
}

class DebugObserver extends NavigatorObserver {
  @override
  void didPush(Route route, Route? previousRoute) {
    print("push: $route, $previousRoute");
    super.didPush(route, previousRoute);
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    print("push: $newRoute, $oldRoute");
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
  }
}

class MainApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey = GetIt.I<GlobalKey<NavigatorState>>();
  final tokenBox = GetIt.I<Box<String>>(instanceName: "token");

  MainApp() {
      initUniLinks();
  }

  void handleLink(Uri link) {
    print(link);
    if (link.path.startsWith("/category/")) {
      var pathSegment = link.pathSegments.last;
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        loadEpisodeSource(navigatorKey.currentState!.context, pathSegment, 1, exit: false);
      });
    } else if (link.scheme == "killer") {
      var uri = Uri.parse(link.toString().replaceFirst("#", "?"));
      tokenBox.putAll(uri.queryParameters).then((value) {
        navigatorKey.currentState!.pushReplacementNamed('/watching');
      });
    }
  }

  void initUniLinks() async {
    print("listen for events");
    final initialUri = await getInitialUri();
    if(initialUri != null) {
      handleLink(initialUri);
    }
    uriLinkStream.listen((link) {
      if(link != null) {
        handleLink(link);
      }
    }, onError: (error) {
      print("error: $error");
    }, cancelOnError: false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.dark,
      routes: {
        '/watching': (context) => CurrentlyWatching(title: "Currently Watching"),
        '/welcome': (context) => Welcome(title: "Welcome"),
      },
      navigatorKey: navigatorKey,
      initialRoute: loggedIn() != null ? '/watching' : '/welcome',
    );
  }
}
