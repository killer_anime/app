import 'package:ferry/ferry.dart';
import 'package:killer_anime/graphql/local/configuration.data.gql.dart';
import 'package:killer_anime/graphql/local/configuration.req.gql.dart';

void setConfig(Client client, String configId, GappConfigData data) {
  final piracyFragmentRequest = GappConfigReq((b) => b..idFields = { 'id': configId });
  client.cache.writeFragment(piracyFragmentRequest, data);
}

GappConfigData? getConfig(Client client, String configId) {
  final piracyFragmentRequest = GappConfigReq((b) => b..idFields = { 'id': configId });
  final data =client.cache.readFragment(piracyFragmentRequest);
  return data;
}