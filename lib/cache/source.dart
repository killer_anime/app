import 'package:ferry/ferry.dart';
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.data.gql.dart';
import 'package:killer_anime/graphql/fragments/MediaExternalLinks.req.gql.dart';
import 'package:killer_anime/graphql/fragments/MediaListProgress.data.gql.dart';
import 'package:killer_anime/graphql/fragments/MediaListProgress.req.gql.dart';

void setSource(Client client, int mediaId, String source, String url) {

  final piracyFragmentRequest = GMediaExternalLinksReq((b) => b..idFields = { 'id': mediaId });

  final piracyFragmentData = GMediaExternalLinksData((data) =>
  data
    ..externalLinks.addAll([GMediaExternalLinksData_externalLinks((b) =>
    b
      ..site = source
      ..url = url)
    ])
    ..id = mediaId);

  client.cache.writeFragment(piracyFragmentRequest, piracyFragmentData);

}

void clearSource(Client client, int mediaId) {

  final piracyFragmentRequest = GMediaExternalLinksReq((b) => b..idFields = { 'id': mediaId });

  final piracyFragmentData = GMediaExternalLinksData((data) =>
  data
    ..externalLinks.clear()
    ..id = mediaId);

  client.cache.writeFragment(piracyFragmentRequest, piracyFragmentData);

}

List<GMediaExternalLinksData_externalLinks> getSources(Client client, int mediaId) {
  final piracyFragmentRequest = GMediaExternalLinksReq((b) => b..idFields = { 'id': mediaId });
  return client.cache.readFragment(piracyFragmentRequest)?.externalLinks?.toList() ?? [];
}

GMediaExternalLinksData_externalLinks? getSource(Client client, int mediaId, String source) {
  final sourceList = getSources(client, mediaId).where((link) => link.site == source);
  if (sourceList.isNotEmpty) {
    return sourceList.first;
  }
}

GMediaExternalLinksData_externalLinks? getFirstSource(Client client, int mediaId) {
  final sourceList = getSources(client, mediaId);
  if (sourceList.isNotEmpty) {
    return sourceList.first;
  }
}

void updateProgressCache(Client client, int mediaListId, int progress) {
  final fragmentRequest = GMediaListProgressReq((b) => b..idFields = { 'id': mediaListId });

  final fragmentData = GMediaListProgressData((data) =>
  data
    ..progress = progress
    ..id = mediaListId);

  client.cache.writeFragment(fragmentRequest, fragmentData);
}