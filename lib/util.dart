import 'dart:async';
import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:ferry/ferry.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:killer_anime/cache/source.dart';
import 'package:killer_anime/elements/load_episode_source.dart';
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart';
import 'package:killer_anime/graphql/local/configuration.data.gql.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';

import 'cache/settings.dart';
import 'elements/bottomCard.dart';
import 'pages/settings.dart';
import 'pages/source_list.dart';
import 'pages/video.dart';

void Function(String) debounce(void Function(String) func, int milliseconds) {
  Timer? timer;
  return (String value) {
    // or (arg) if you need an argument
    if (timer != null) {
      timer?.cancel();
    }

    timer = Timer(Duration(milliseconds: milliseconds), () {
      func(value);
    }); // or () => func(arg)
  };
}

void openVideoPage(String url) async {
  final videoPlayerController = VideoPlayerController.network(url);

  await videoPlayerController.initialize();

  final GlobalKey<NavigatorState> navigatorKey = GetIt.I<GlobalKey<NavigatorState>>();

  navigatorKey.currentState!.push(
    MaterialPageRoute(
        builder: (context) => VideoPage(controller: videoPlayerController), fullscreenDialog: true),
  );
}

void openShow(String url) async {
  final client = GetIt.I<Client>();
  final config = getConfig(client, "main");
  if(config != null && config.external == true) {
   openVideoPage(url);
  }else{
    if (Platform.isAndroid) {
      AndroidIntent(action: 'action_view', data: url, type: 'video/mp4').launch();
    } else {
      launch(url, forceSafariVC: false);
    }
  }
}

Future<int?> loadEpisodeList(BuildContext context, Client client, int id, int episode, List<String> titles,
    {bool exit = true, bool openEpisodes = false}) async {
  String? urlID = getFirstSource(client, id)?.url;
  print("urlId: $urlID");
  if (urlID != null) {
    return loadEpisodeSource(context, urlID, episode, openEpisodes: openEpisodes, exit: exit);
  } else {
    var url = await loadSourceShows(context, titles);
    if (url != null) {
      setSource(client, id, "gogoanime", url);
      return loadEpisodeList(context, client, id, episode, titles);
    }
  }
}

List<String> getMediaTitles(GMediaDisplay media) {
  return <String?>[media.title!.romaji, media.title!.english, ...(media.synonyms?.asList() ?? [])]
      .where((element) => element != null)
      .map((e) => e!)
      .toList();
}

Future<String?> loadSourceShows(BuildContext context, List<String> titles) async {
  return await openBottomModal<String>(
    context,
    (_, controller) => SourceAnimeList(
        titles: titles,
        onSelect: (uri) {
          Navigator.pop(context, uri.pathSegments[1]);
        },
        scrollController: controller),
  );
}

Future openSettings(BuildContext context) async {
  return await openBottomModal(context, (_, controller) => Settings(controller: controller));
}
