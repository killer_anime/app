import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CustomListItem extends StatelessWidget {
  const CustomListItem({Key? key, required this.thumbnail, required this.title}) : super(key: key);

  final Widget thumbnail;
  final Widget title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          thumbnail,
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 2.0, 0.0),
              child: Center(child: title),
            ),
          )
        ],
      ),
    );
  }
}

class AnimeCard extends StatelessWidget {
  const AnimeCard({
    Key? key,
    required this.url,
    required this.title,
    required this.episodes,
    this.onTap,
  }) : super(key: key);

  final String url;
  final String title;
  final String episodes;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: CustomListItem(
        thumbnail: Container(
          width: 95,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: CachedNetworkImageProvider(url),
            fit: BoxFit.cover,
          )),
        ),
        title: Column(children: [
          Expanded(
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
                maxLines: 2,
              ),
            ),
          ),
          Text(episodes),
        ]),
      ),
    );
  }
}
