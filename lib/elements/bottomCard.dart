import 'package:flutter/material.dart';

Future<T?> openBottomModal<T>(BuildContext context, Function(BuildContext, ScrollController) builder) async {
  return await showModalBottomSheet<T>(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) {
      return GestureDetector(
  //      onTap: () => Navigator.of(context).pop(),
        child: Container(
          color: Color.fromRGBO(0, 0, 0, 0.001),
          child: DraggableScrollableSheet(
            initialChildSize: 0.5,
            maxChildSize: 0.5,
            /*
                        initialChildSize: 0.4,
            minChildSize: 0.2,
            maxChildSize: 0.75,
             */
            builder: (context, controller) {
              return Material(
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(25.0),
                    topRight: const Radius.circular(25.0),
                  ),
                  child: builder(context, controller));
            },
          ),
        ),
      );
    },
  );
}
