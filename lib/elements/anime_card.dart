import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension Interlieve<T> on Iterable<T> {
  List<T> interleave(T separator) {
    var iterator = this.iterator;
    if (!iterator.moveNext()) return [];
    List<T> result = [];

    result.add(iterator.current);
    while (iterator.moveNext()) {
      result.add(separator);
      result.add(iterator.current);
    }
    return result;
  }
}

class CustomListItem extends StatelessWidget {
  const CustomListItem(
      {Key? key, required this.thumbnail, required this.title, this.direction = Axis.horizontal})
      : super(key: key);

  final Widget thumbnail;
  final Widget title;
  final Axis direction;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Padding(
        padding: EdgeInsets.all(7),
        child: Flex(
          crossAxisAlignment: CrossAxisAlignment.start,
          direction: direction,
          children: <Widget>[
            ClipRRect(
                clipBehavior: Clip.antiAlias, borderRadius: BorderRadius.circular(10), child: thumbnail),
            Expanded(child: title)
          ],
        ),
      ),
    );
  }
}

class CardTag {
  String title;
  int size;

  CardTag(this.title, this.size);
}

class AnimeCard extends StatelessWidget {
  const AnimeCard(
      {Key? key,
      required this.url,
      required this.title,
      required this.tags,
      this.subtitle,
      this.onTap,
      this.onLongPress,
      this.direction = Axis.horizontal})
      : super(key: key);

  final String url;
  final String title;
  final String? subtitle;
  final List<CardTag> tags;
  final Axis direction;
  final Function()? onTap;
  final Function()? onLongPress;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      clipBehavior: Clip.antiAlias,
      borderRadius: BorderRadius.circular(15),
      child: Material(
        child: InkWell(
          onTap: this.onTap,
          onLongPress: this.onLongPress,
          child: Container(
            child: CustomListItem(
              direction: direction,
              thumbnail: Container(
                width: 75,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: CachedNetworkImageProvider(url),
                  fit: BoxFit.cover,
                )),
              ),
              title: Padding(
                padding: EdgeInsets.only(left: 7),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                              maxLines: 2,
                            ),
                            if (subtitle != null)
                              Text(subtitle!,
                                  style: TextStyle(
                                      color: Colors.white54, fontWeight: FontWeight.normal, fontSize: 12))
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: tags
                            .map<Widget>(
                              (element) => Expanded(
                                flex: element.size,
                                child: Material(
                                  clipBehavior: Clip.antiAlias,
                                  borderRadius: BorderRadius.circular(10),
                                  color: Theme.of(context).cardColor,
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                    child: Center(
                                      child: Text(element.title),
                                    ),
                                  ),
                                ),
                              ),
                            )
                            .interleave(Container(width: 7))
                            .toList(),
                      ),
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
