import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:killer_anime/elements/bottomCard.dart';
import 'package:killer_anime/pages/episode_list.dart';

import '../util.dart';

Future<int?> loadEpisodeSource(BuildContext context, String pathSegment, int episode,
    {bool exit = true, bool openEpisodes = false}) async {
  return await openBottomModal(
    context,
    (_, controller) => EpisodeList(
      urlID: pathSegment,
      episode: episode.toString(),
      scrollController: controller,
      onSelect: (uri, episode) {
        if (uri != null) {
          openShow(uri);
        }
        if (exit) {
          Navigator.pop(context, episode);
        }
      },
      openEpisodes: openEpisodes,
    ),
  );
}
