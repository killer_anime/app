import 'package:edit_distance/edit_distance.dart';
import 'package:flutter/material.dart';
import 'package:killer_anime/source/gogoanime/AnimeParser.dart';

final domain = Uri.parse('https://gogoanime.pe/');

Uri search(String keyword) {
  Uri local =
      Uri(scheme: "https", host: "gogoanime.pe", pathSegments: ["search.html"], query: 'keyword=$keyword');
  print(local);
  return local;
}

class SourceAnimeList extends StatefulWidget {
  const SourceAnimeList({
    Key? key,
    required this.titles,
    this.onSelect,
    this.scrollController,
  }) : super(key: key);

  final List<String> titles;
  final Function(Uri link)? onSelect;
  final ScrollController? scrollController;

  @override
  State<SourceAnimeList> createState() => SourceAnimeListState();
}

class SourceAnimeListState extends State<SourceAnimeList> {
  List<Source> shows = [];
  String? title;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    load(widget.titles[0]);
  }

  void load(String title) async {
    var url = search(title);
    var parser = AnimeParser(url);
    var document = await parser.downloadHTML();
    final downloadedShows = parser.parseHTML(document);
    final j = CombinedJaccard(ngram: 15, usePadding: true);

    downloadedShows.sort((a, b) =>
        j.normalizedDistance(a.title ?? "", title).compareTo(j.normalizedDistance(b.title ?? "", title)));

    setState(() {
      shows = downloadedShows;
      this.title = title;
      this.loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Center(child: CircularProgressIndicator());
    }
    return Center(
        child: Column(children: [
      Container(
          child: Row(children: [
        Expanded(child: Text(this.title ?? "??", textAlign: TextAlign.center)),
        PopupMenuButton<String>(
            onSelected: (String result) {
              setState(() {
                load(result);
              });
            },
            itemBuilder: (BuildContext context) => widget.titles
                .map((title) => PopupMenuItem<String>(
                      value: title,
                      child: Text(title),
                    ))
                .toList())
      ])),
      Expanded(
        child: ListView.builder(
            controller: widget.scrollController,
            itemCount: shows.length,
            itemBuilder: (context, index) {
              var show = shows[index];

              return ListTile(
                title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(show.title ?? "??"),
                  ...show.tags.map((tag) => Chip(
                      label: Text(tag),
                      labelStyle: TextStyle(fontSize: 12),
                      visualDensity: VisualDensity(vertical: -4, horizontal: -4),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap))
                ]),
                onTap: () {
                  if (widget.onSelect != null) {
                    widget.onSelect!(show.link);
                  }
                },
              );
            }),
      ),
    ]));
  }
}
