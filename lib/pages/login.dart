import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:killer_anime/api/anilist_auth.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:killer_anime/elements/anime_card.dart';
import 'package:killer_anime/pages/episode_list.dart';
import 'package:killer_anime/pages/source_list.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => LoginState();
}

class LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).cardColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("You will be redirected to login through AniList."),
            ElevatedButton(
                onPressed: () async {
                  await openAuth();
                },
                child: Text("Login in"))
          ],
        ),
      ),
    );
  }
}
