import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get_it/get_it.dart';
import 'package:killer_anime/cache/source.dart';
import 'package:killer_anime/elements/anime_card.dart';
import 'package:killer_anime/graphql/fragments/MediaDisplay.data.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.data.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.req.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.var.gql.dart';
import 'package:killer_anime/util.dart';

class GlobalSearch extends StatefulWidget {
  GlobalSearch({Key? key, required this.onSelect}) : super(key: key);

  final _scrollController = ScrollController();
  final searchTextController = new TextEditingController();
  final Function(GMediaDisplay) onSelect;

  @override
  State<StatefulWidget> createState() => GlobalSearchState();
}

class GlobalSearchState extends State<GlobalSearch> {
  final client = GetIt.I<Client>();
  String search = "";
  bool searching = false;

  var page = 1;
  var loading = false;

  final GGetPageReq request = GGetPageReq((q) => q
    ..fetchPolicy = FetchPolicy.CacheAndNetwork
    ..vars.page = 1
    ..requestId = 'GlobalPage');

  late GGetPageReq searchRequest = GGetPageReq((q) => q
    ..vars.page = 1
    ..vars.search = ""
    ..requestId = 'GlobalSearchPage');

  GlobalSearchState() {}

  Widget buildCard(GMediaDisplay media, {Axis direction = Axis.horizontal}) {
    final source = getFirstSource(client, media.id);
    final hasSource = source != null;

    return Container(
      height: double.infinity,
      padding: EdgeInsets.all(5),
      color: Theme.of(context).cardColor,
      child: AnimeCard(
        direction: direction,
        title: media.title!.userPreferred!,
        subtitle: media.nextAiringEpisode != null
            ? "Next episode in ${Duration(seconds: media.nextAiringEpisode!.timeUntilAiring).inDays} days"
            : null,
        url: media.coverImage!.large!,
        tags: [if (hasSource) CardTag(source!.site, 7)],
        onTap: () async {
          widget.onSelect(media);
        },
        onLongPress: () async {
          clearSource(client, media.id);
        },
      ),
    );
  }

  @override
  void didUpdateWidget(covariant GlobalSearch oldWidget) {
    super.didUpdateWidget(oldWidget);
    print(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).cardColor,
        appBar: searching
            ? AppBar(
                // title: Text("${widget.title} - $viewer"),
                title: PreferredSize(
                preferredSize: Size.fromHeight(48),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14),
                  child: TextField(
                    autocorrect: false,
                    autofocus: true,
                    controller: widget.searchTextController,
                    onChanged: debounce((value) {
                      setState(() {
                        this.page = 1;
                        this.searchRequest = searchRequest.rebuild((b) => b
                          ..vars.page = 1
                          ..vars.search = value);
                        this.search = value;
                      });
                      client.requestController.add(this.searchRequest);
                    }, 250),
                    style: TextStyle(color: Colors.white, fontSize: 16),
                    decoration: InputDecoration(
                      hintText: 'Enter an anime title',
                      hintStyle: TextStyle(color: Colors.white.withOpacity(0.6), fontSize: 16),
                      // border: InputBorder.none,
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            this.searching = false;
                            this.page = 1;
                          });
                        },
                        icon: Icon(Icons.clear),
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ))
            : null,
        body: Operation(
          client: client,
          operationRequest: this.searching ? searchRequest : request,
          builder:
              (BuildContext context, OperationResponse<GGetPageData, GGetPageVars>? result, Object? error) {
            Widget listView = Visibility(
                visible: result == null || result.loading, child: Center(child: CircularProgressIndicator()));

            if (error != null || result?.loading == true || result?.hasErrors == true) {
              print(error);
              print(result?.linkException);
              print(result?.graphqlErrors);
            } else {
              final list = result!.data!.Page!.media!;

              listView = NotificationListener<ScrollEndNotification>(
                child: ListView.builder(
                  itemExtent: 120,
                  itemBuilder: (BuildContext context, int index) {
                    final info = list[index];
                    return this.buildCard(info);
                  },
                  itemCount: list.length,
                  controller: widget._scrollController,
                ),
                onNotification: (notification) {
                  final scrollPercentage = widget._scrollController.position.pixels /
                      widget._scrollController.position.maxScrollExtent;
                  if (scrollPercentage > 0.85 && loading == false && result.loading == false) {
                    final usedRequest = (searching ? searchRequest : request);
                    final nextPage = this.page + 1;
                    final nextPageRequest = usedRequest.rebuild((b) => b
                      ..vars.page = nextPage
                      ..updateResult = (previous, result) {
                        setState(() {
                          loading = false;
                        });
                        return previous?.rebuild((b) => b..Page.media.addAll(result!.Page!.media!)) ?? result;
                      });
                    setState(() {
                      loading = true;
                      page = nextPage;
                    });
                    client.requestController.add(nextPageRequest);
                  }
                  return true;
                },
              );
            }
            return listView;
          },
        ),
        floatingActionButton: !this.searching
            ? FloatingActionButton(
                onPressed: () {
                  setState(() {
                    searching = true;
                    page = 1;
                  });
                },
                child: const Icon(Icons.search),
              )
            : null);
  }
}

class GlobalSearchHome extends StatelessWidget {
  final client = GetIt.I<Client>();

  @override
  Widget build(BuildContext context) {
    return GlobalSearch(onSelect: (media) {
      loadEpisodeList(context, client, media.id, 1, getMediaTitles(media), openEpisodes: true);
    });
  }

}