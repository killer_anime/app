import 'dart:async';

import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:killer_anime/graphql/queries/getPage.data.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.req.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.var.gql.dart';
import 'package:killer_anime/pages/global_search.dart';
import 'package:killer_anime/pages/login.dart';
import 'package:killer_anime/pages/settings.dart';

import 'package:killer_anime/pages/source_list.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class Welcome extends StatefulWidget {
  Welcome({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Welcome> createState() => WelcomeState();
}

class WelcomeState extends State<Welcome> with TickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, initialIndex: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(children: [
        Expanded(child: TabBarView(controller: tabController, children: [Login(), GlobalSearchHome(), Settings(controller: ScrollController())])),
        SizedBox.fromSize(
          size: Size.fromHeight(53),
          child: TabBar(
            controller: tabController,
            tabs: [
              Tab(
                text: 'Login',
                icon: Icon(Icons.next_plan),
                iconMargin: EdgeInsets.all(0),
              ),
              Tab(
                text: 'Trending',
                icon: Icon(Icons.remove_red_eye),
                iconMargin: EdgeInsets.all(0),
              ),
              Tab(
                text: 'Settings',
                icon: Icon(Icons.settings),
                iconMargin: EdgeInsets.all(0),
                
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
