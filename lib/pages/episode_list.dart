import 'dart:convert';
import 'dart:math';

import 'package:encrypt/encrypt.dart' as Encrypt;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:killer_anime/source/gogoanime/EpisodeCountParser.dart';
import 'package:killer_anime/source/gogoanime/EpisodeListParser.dart';
import 'package:killer_anime/source/gogoanime/OneEpisodeParser.dart';

final domain = Uri.parse('https://gogoanime.pe/');

class VideoSource {
  late String label;
  late String file;

  VideoSource(this.label, this.file);

  VideoSource.fromJson(Map<String, dynamic>? json) {
    if (json == null) return;
    this.label = json['label'];
    this.file = json['file'];
  }

  Map<String, dynamic> toJson() {
    return {
      'label': this.label,
      'file': this.file,
    };
  }

  @override
  String toString() {
    return 'label: $label, file: $file';
  }
}

Uri episodeLink(String pathElement, String episode) {
  Uri local = Uri(
    scheme: "https",
    host: "gogoanime.pe",
    pathSegments: ["${pathElement}-episode-${episode.replaceAll(".", "-")}"],
  );
  return local;
}

Uri animeLink(String pathElement) {
  Uri local = Uri(
    scheme: "https",
    host: "gogoanime.pe",
    pathSegments: ["category", pathElement],
  );
  return local;
}

Uri episodeListLink(String pathElement, String start, String end, String id) {
  Uri local = Uri(
      scheme: "https",
      host: "ajax.gogo-load.com",
      pathSegments: ["ajax", "load-list-episode"],
      queryParameters: {'ep_start': start, 'ep_end': end, 'id': id, 'default_ep': '0'});
  return local;
}

class EpisodeList extends StatefulWidget {
  EpisodeList(
      {Key? key,
      required this.urlID,
      required this.episode,
      this.scrollController,
      this.onSelect,
      this.openEpisodes = false})
      : super(key: key);

  final String urlID;
  final String episode;
  final ScrollController? scrollController;
  final Function(String?, int?)? onSelect;
  final bool openEpisodes;

  @override
  State<EpisodeList> createState() => EpisodeListState();
}

class EpisodeListState extends State<EpisodeList> with SingleTickerProviderStateMixin {
  late TabController tabController;

  List<VideoSource> sources = [];
  List<String> episodes = [];
  String title = "";
  String episode = "";
  bool loading = true;

  @override
  void initState() {
    episode = widget.episode;
    super.initState();

    tabController = TabController(length: 2, initialIndex: widget.openEpisodes ? 1 : 0, vsync: this);

    load(widget.episode);
    try {
      loadEpisodeList();
    } catch(e) {
      print(e);
    }
  }

  void loadEpisodeList() async {
    var episodeCount = EpisodeCountParser(animeLink(widget.urlID));
    var episodeCountBody = await episodeCount.downloadHTML();

    episodeCount.parseHTML(episodeCountBody);

    var episodeListParser = EpisodeListParser(episodeListLink(
        widget.urlID, episodeCount.firstEpisode, episodeCount.lastEpisode, episodeCount.animeId!));

    var episoideListBody = await episodeListParser.downloadHTML();
    var episodeList = episodeListParser.parseHTML(episoideListBody);

    setState(() {
      episodes = episodeList;
    });
  }

  void loadStreamani(String server) async {
    var response = await get(Uri.parse("https://" + server.substring(2, server.length)));

    setState(() {
      sources = [
        ...sources,
        ...RegExp("{(.+?:.+?)(,\w*?(.+?:.+?))*?}")
            .allMatches(parse(response.body)
                .getElementsByTagName("script")
                .where((element) => element.attributes['type'] == "text/JavaScript")
                .first
                .text)
            .toList(growable: false)
            .reversed
            .map((match) {
          print("$server, ${match[0]}");
          return VideoSource.fromJson(
            json.decode(
              match[0]!
                  .replaceAll("file:", "\"file\":")
                  .replaceAll("label:", "\"label\":")
                  .replaceAll("'", "\""),
            ),
          );
        })
      ];
    });
  }

  void loadDoodLa(String server) async {
    var response = await get(Uri.parse(server.replaceAll("/d/", "/e/")));
    var content = response.body;
    if (response.body.contains("'/pass_md5/")) {
      var md5 = content.split("'/pass_md5/")[1].split("',")[0];
      var token = md5.split("/").last;

      var rng = new Random();
      var randomString = new List.generate(10, (_) => rng.nextInt(10).toString()).join("");

      var expiry = DateTime.now().millisecondsSinceEpoch + 10;
      var videoUrlStartResp =
          await get(Uri.parse("https://dood.ws/pass_md5/$md5"), headers: {'Referer': server});

      var videoUrlStart = videoUrlStartResp.body;
      setState(() {
        sources = [
          ...sources,
          VideoSource("dood.ws", "$videoUrlStart$randomString?token=$token&expiry=$expiry")
        ];
      });
    }
  }

  void loadStreamaniStreaming(String server) async {
    final uri = Uri.parse("https://" + server.substring(2, server.length));

    final id = uri.queryParameters["id"];
    final title = uri.queryParameters["title"];
    if (id == null || title == null) return;

    final key = Encrypt.Key.fromBase64('MjU3NDY1Mzg1OTI5MzgzOTY3NjQ2NjI4Nzk4MzMyODg=');
    var rng = new Random();
    var full = new List.generate(20, (_) => rng.nextInt(10).toString());

    final iv = Encrypt.IV.fromUtf8(full.skip(2).take(16).join());

    final encrypter = Encrypt.Encrypter(Encrypt.AES(key, mode: Encrypt.AESMode.cbc));
    final encryptedId = encrypter.encrypt(id, iv: iv);
    print(encryptedId.base64);

    final encryptedUri = Uri(
        scheme: "https",
        host: "gogoplay1.com",
        path: "encrypt-ajax.php",
        query: 'id=${encryptedId.base64}&title=$title&refer=${"none"}&time=${full.join()}');

    print(encryptedUri);

    var response = await get(encryptedUri, headers: {
      'Referer': 'https://gogoplay1.com/streaming.php?id=MTY2NjI5&title=Sonny+Boy+Episode+3',
      'x-requested-with': 'XMLHttpRequest'
    });

    var jsonResponse = json.decode(response.body);
    print("StreamaniStreaming: $jsonResponse");

    var data = jsonResponse["source"];
    if (jsonResponse["source"] != null) {
      for (var source in data) {
        setState(() {
          sources = [...sources, VideoSource.fromJson(source)];
        });
      }
    }
  }

  void loadAnimeOwl(String server) async {
    final uri = Uri.parse("https://" + server.substring(2, server.length));
    final id = uri.queryParameters["id"];
    if (id == null) return;

    var response = await get(Uri.parse("https://animeowl.net/direct-link/$id"));

    var jsonResponse = json.decode(response.body);
    //print("AnimeOwl: $jsonResponse");

    for (var key in ['luffy', 'vidstream']) {
      if (jsonResponse[key] != null && jsonResponse[key].toString().isNotEmpty) {
        setState(() {
          sources = [...sources, VideoSource("Anime Owl", jsonResponse[key])];
        });
      }
    }
  }

  void loadEmbedstio(String server) async {
    var response = await post(
        Uri.parse("https://embedsito.com/api/source/" + Uri.parse(server).pathSegments.last),
        headers: {
          'User-Agent':
              'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
        });

    var jsonResponse = json.decode(response.body);
    //print("Embedsito: $jsonResponse");

    var data = jsonResponse["data"];
    if (jsonResponse["success"] == true) {
      for (var source in data) {
        setState(() {
          sources = [...sources, VideoSource.fromJson(source)];
        });
      }
    }
  }

  void loadMP4Upload(String server) async {
    var response = await get(Uri.parse(server), headers: {
      'User-Agent':
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
    });

    print(response.body);
  }

  Future<void> load(String episode) async {
    var link = episodeLink(widget.urlID, episode);
    var parser = OneEpisodeParser(link);
    var body = await parser.downloadHTML();
    var _info = parser.parseHTML(body);
    setState(() {
      this.title = _info.currentEpisode?.replaceFirst(RegExp("[Ee]pisode .+? "), "") ?? "";
      this.episode = episode;
      this.sources = [];
    });

    for (var server in _info.servers) {
      try {
        if ((server.contains('gogoplay1.com') || server.contains('streamani.io')) &&
            server.contains('embedplus')) {
          loadStreamani(server);
        }

        if ((server.contains('gogoplay1.com') || server.contains('streamani.io')) &&
            server.contains('streaming')) {
          loadStreamaniStreaming(server);
          //loadAnimeOwl(server);
        }

        if ((server.contains('embedsito'))) {
          loadEmbedstio(server);
        }

        if ((server.contains('dood.ws'))) {
          loadDoodLa(server);
        }
      } catch (error) {
        print(error);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance?.addPostFrameCallback((_) => widget.scrollController?.jumpTo(
    //     widget.scrollController?.position.maxScrollExtent ??
    //         widget.scrollController?.initialScrollOffset ??
    //         0));

    return Center(
      child: Column(children: [
        Container(
            padding: EdgeInsets.all(15),
            child: Row(children: [
              Expanded(child: Text(this.title, textAlign: TextAlign.center)),
            ])),
        Expanded(
          child: TabBarView(controller: tabController, children: [
            ListView.builder(
                controller: widget.scrollController,
                itemCount: sources.length,
                itemBuilder: (context, index) {
                  var show = sources[index];

                  return ListTile(
                    title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Text(show.label),
                      Chip(
                          label: Text(Uri.parse(show.file).host),
                          labelStyle: TextStyle(fontSize: 12),
                          visualDensity: VisualDensity(vertical: -4, horizontal: -4),
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)
                    ]),
                    trailing: IconButton(
                      icon: Icon(Icons.star_border),
                      // icon: Icon(box.get(widget.urlID) == show.label ? Icons.star : Icons.star_border),
                      onPressed: () {
                        // if (box.get(widget.urlID) == show.label) {
                        //   box.delete(widget.urlID);
                        // } else {
                        //   box.put(widget.urlID, show.label);
                        // }
                      },
                    ),
                    onTap: () {
                      int? episodeNumber = null;
                      if (!episode.contains(".")) {
                        episodeNumber = int.tryParse(episode);
                      }
                      widget.onSelect != null ? widget.onSelect!(show.file, episodeNumber) : {};
                    },
                  );
                }),
            ListView.builder(
                controller: widget.scrollController,
                itemCount: episodes.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text("Episode: ${episodes[index]}"),
                    onTap: () async {
                      var task = load(episodes[index]);
                      tabController.animateTo(0);
                      await task;
                    },
                  );
                }),
          ]),
        ),
        TabBar(
          labelColor: Theme.of(context).textTheme.button!.color,
          controller: tabController,
          tabs: [
            Tab(icon: Text("Episode ${episode}")),
            Tab(icon: Text("Episode list")),
          ],
        ),
      ]),
    );
  }
}
