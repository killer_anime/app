import 'package:android_intent/android_intent.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:killer_anime/api/anilist_auth.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:killer_anime/elements/anime_card.dart';
import 'package:killer_anime/pages/episode_list.dart';
import 'package:killer_anime/pages/source_list.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:video_player/video_player.dart';

class VideoPage extends StatefulWidget {
  const VideoPage({Key? key, required this.controller}) : super(key: key);
  final VideoPlayerController controller;

  @override
  State<VideoPage> createState() => VideoPageState();
}

class VideoPageState extends State<VideoPage> {
  late ChewieController chewieController = ChewieController(
    videoPlayerController: widget.controller,
    autoPlay: true,
  );

  @override
  void dispose() {
    chewieController.dispose();
    widget.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Chewie(
        controller: chewieController,
      ),
    );
  }
}
