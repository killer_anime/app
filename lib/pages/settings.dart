import 'dart:async';

import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:killer_anime/cache/settings.dart';
import 'package:killer_anime/graphql/local/configuration.data.gql.dart';

import 'package:killer_anime/graphql/queries/getPage.data.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.req.gql.dart';
import 'package:killer_anime/graphql/queries/getPage.var.gql.dart';
import 'package:killer_anime/pages/global_search.dart';
import 'package:killer_anime/pages/login.dart';

import 'package:killer_anime/pages/source_list.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class Settings extends StatefulWidget {
  Settings({Key? key, required this.controller}) : super(key: key);

  final ScrollController controller;
  final client = GetIt.I<Client>();

  @override
  State<Settings> createState() => SettingsState();
}

class SettingsState extends State<Settings> with TickerProviderStateMixin {
  late GappConfigData config;

  @override
  void initState() {
    super.initState();

    var localConfig = getConfig(widget.client, "main");
    if (localConfig == null) {
      localConfig = GappConfigData((b) => b
        ..id = "main"
        ..external = false);
    }
    config = localConfig;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(children: [
          Container(
              padding: EdgeInsets.all(15),
              child: Row(children: [
                Expanded(child: Text("Settings", textAlign: TextAlign.center)),
              ])),
          SwitchListTile(
            title: const Text('Use Internal Player'),
            value: config.external ?? false,
            onChanged: (bool value) {
              setState(() {
                config = config.rebuild((b) => b.external = value);
                setConfig(widget.client, "main", config);
              });
            },
          )
        ], controller: widget.controller),

    );
  }
}
