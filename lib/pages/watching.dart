import 'dart:math';

import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:killer_anime/cache/source.dart';
import 'package:killer_anime/elements/anime_card.dart';
import 'package:killer_anime/graphql/mutations/updateProgress.req.gql.dart';
import 'package:killer_anime/graphql/queries/getListId.req.gql.dart';
import 'package:killer_anime/graphql/queries/getWatching.data.gql.dart';
import 'package:killer_anime/graphql/queries/getWatching.req.gql.dart';
import 'package:killer_anime/graphql/queries/getWatching.var.gql.dart';
import 'package:killer_anime/graphql/schema.schema.gql.dart';

import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:fuzzy/fuzzy.dart';
import 'package:killer_anime/pages/global_search.dart';
import 'package:killer_anime/util.dart';
import 'package:time_ago_provider/time_ago_provider.dart' as timeAgo;

class CurrentlyWatching extends StatefulWidget {
  CurrentlyWatching({Key? key, required this.title}) : super(key: key);

  final client = GetIt.I<Client>();
  final String title;

  @override
  State<CurrentlyWatching> createState() => CurrentlyWatchingState();
}

class CurrentlyWatchingState extends State<CurrentlyWatching> {
  final ScrollController controller = new ScrollController();

  int currentSection = 0;
  Box<String> tokenBox = Hive.box("token");
  String search = "";
  bool searching = false;

  Map<String, GlobalKey> sections = {
    'Watching': new GlobalKey(),
    'Planning': new GlobalKey(),
    'Paused': new GlobalKey()
  };

  late GGetWatchingReq query;

  CurrentlyWatchingState() {
    final Map<String, dynamic> tokenData = JwtDecoder.decode(tokenBox.get('access_token')!);
    query = GGetWatchingReq((b) => b
      ..requestId = 'CurrentlyWatching'
      ..vars.id = int.parse(tokenData['sub'])
      ..fetchPolicy = FetchPolicy.CacheAndNetwork);
  }

  void onScroll() {
    var index = 0;
    for (var entry in sections.entries) {
      var value = entry.value;
      var subListContext = value.currentContext!;
      var subListRenderObject = subListContext.findRenderObject();
      var viewport = RenderAbstractViewport.of(subListRenderObject)!;

      if (viewport.getOffsetToReveal(subListRenderObject!, 0.0).offset > controller.position.pixels) {
        break;
      } else {
        index += 1;
      }
    }

    setState(() {
      this.currentSection = max(index - 1, 0);
    });
  }

  @override
  void initState() {
    super.initState();
    if (!tokenBox.containsKey("access_token") || tokenBox.get('access_token') == null) {
      Navigator.pushReplacementNamed(context, '/welcome');
    }

    controller.addListener(this.onScroll);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void refetch() {
    widget.client.requestController.add(query);
  }

  void setAnimeStatus(
      {required int id, required int progress, required GMediaListStatus status, bool update = false}) async {
    final mutation = GUpdateProgressReq((b) => b
      ..vars.id = id
      ..vars.status = status
      ..vars.progress = progress
      ..optimisticResponse.SaveMediaListEntry.id = id
      ..optimisticResponse.SaveMediaListEntry.progress = progress
      ..optimisticResponse.SaveMediaListEntry.status = status);

    await widget.client
        .request(mutation)
        .firstWhere((response) => response.dataSource != DataSource.Optimistic);
    if (update) {
      refetch();
    }
  }

  Widget buildCard(GGetWatchingData_MediaListCollection_lists_entries info,
      {Axis direction = Axis.horizontal}) {
    void setAnimeStatusUpdate(
        {required int id, required int progress, required GMediaListStatus status}) async {
      setAnimeStatus(id: id, progress: progress, status: status, update: info.status != status);
    }

    var nextAiring = "";
    if (info.media!.nextAiringEpisode != null) {
      final now = DateTime.now();
      final durationUntilAir = now.add(Duration(seconds: info.media!.nextAiringEpisode!.timeUntilAiring));
      nextAiring =
          "Next episode is in ${timeAgo.format(durationUntilAir, enableFromNow: true).replaceAll(" from now", "")}";
    }

    final source = getFirstSource(widget.client, info.media!.id);
    final hasSource = source != null;

    return Container(
      height: double.infinity,
      padding: EdgeInsets.all(5),
      color: Theme.of(context).cardColor,
      child: Slidable(
        key: Key("${info.media!.id}"),
        endActionPane: ActionPane(
          extentRatio: 0.8,
          motion: BehindMotion(),
          children: [
            SlidableAction(
              flex: 1,
              onPressed: (ctx) {
                setAnimeStatusUpdate(
                  id: info.id,
                  progress: info.progress!,
                  status: GMediaListStatus.PAUSED,
                );
              },
              backgroundColor: Colors.green,
              foregroundColor: Colors.white,
              icon: Icons.pause,
              label: 'Paused',
            ),
            SlidableAction(
              flex: 1,
              onPressed: (ctx) {
                setAnimeStatusUpdate(
                  id: info.id,
                  progress: 0,
                  status: GMediaListStatus.PLANNING,
                );
              },
              backgroundColor: Colors.blue,
              foregroundColor: Colors.white,
              icon: Icons.arrow_downward,
              label: 'Planning',
            ),
            SlidableAction(
              onPressed: (ctx) async {
                clearSource(widget.client, info.media!.id);
              },
              backgroundColor: Colors.deepOrange,
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Source',
            ),
          ],
        ),
        child: AnimeCard(
          direction: direction,
          title: info.media!.title!.userPreferred!,
          subtitle: nextAiring,
          url: info.media!.coverImage!.large!,
          tags: [
            CardTag(
                info.media!.nextAiringEpisode == null
                    ? "Ep. ${info.progress} of ${info.media?.episodes ?? '?'}"
                    : "Ep. ${info.progress} of ${info.media!.nextAiringEpisode!.episode - 1}",
                4),
            if (hasSource) CardTag(source!.site, 7)
          ],
          onTap: () async {
            var nextEpisode = 1;
            if (!(info.media!.status == GMediaStatus.FINISHED
                ? info.progress! >= (info.media!.episodes ?? 24)
                : info.progress! >= info.media!.nextAiringEpisode!.episode - 1)) {
              nextEpisode = info.progress! + 1;
            } else if (info.media!.status == GMediaStatus.RELEASING) {
              nextEpisode = info.media!.nextAiringEpisode!.episode - 1;
            }

            var episode = await loadEpisodeList(
              context,
              widget.client,
              info.media!.id,
              nextEpisode,
              getMediaTitles(info.media!),
            );
            print(episode);
            if (episode != null) {
              setAnimeStatusUpdate(id: info.id, status: GMediaListStatus.CURRENT, progress: episode);
            }
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget empty = Center(child: Text("No Lists"));

    return Operation(
      client: widget.client,
      operationRequest: query,
      builder: (BuildContext context, OperationResponse<GGetWatchingData, GGetWatchingVars>? result,
          Object? error) {
        var listView = empty;
        if (result == null || result.loading == true) {
          return Center(child: CircularProgressIndicator());
        } else if (result.hasErrors) {
          return empty;
        } else if (result.loading == false) {
          final all = Map.fromEntries(
            result.data!.MediaListCollection!.lists!.where((element) => element != null).map(
                  (listGroup) => MapEntry(
                    listGroup.name!,
                    listGroup.entries!
                        .where((element) =>
                            [GMediaStatus.FINISHED, GMediaStatus.RELEASING].contains(element.media!.status))
                        .where((element) => element != null)
                        .toList(),
                  ),
                ),
          );

          final searched = (search.isEmpty || !searching)
              ? all
              : all.map((key, list) => MapEntry(
                  key,
                  Fuzzy<GGetWatchingData_MediaListCollection_lists_entries>(list,
                      options: FuzzyOptions(tokenize: true, threshold: 0.35, keys: [
                        WeightedKey(
                            name: "title", getter: (item) => item.media!.title!.userPreferred!, weight: 1)
                      ])).search(search).map((result) => result.item).toList()));

          listView = CustomScrollView(
            controller: controller,
            slivers: [
              if (this.searching)
                SliverToBoxAdapter(
                  child: Center(
                    child: InkWell(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 25),
                        child: Text("Open global search"),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (subContext) => GlobalSearch(onSelect: (media) async {
                              var episode = await loadEpisodeList(
                                  context, widget.client, media.id, 1, getMediaTitles(media),
                                  openEpisodes: true);
                              Navigator.pop(context);
                            }),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ...sections.entries.expand((section) {
                final list = searched[section.key]!;

                if (list.length == 0) {
                  return <Widget>[];
                }
                return [
                  SliverStickyHeader(
                    key: section.value,
                    header: Container(
                        height: 50,
                        color: Theme.of(context).bottomAppBarColor,
                        child:
                            Row(mainAxisAlignment: MainAxisAlignment.center, children: [Text(section.key)])),
                    sliver: SliverFixedExtentList(
                      itemExtent: 120,
                      key: Key("${list[0].status!.name}"),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          final info = list[index];
                          return this.buildCard(info);
                        },
                        childCount: list.length,
                      ),
                    ),
                  ),
                ];
              }),
            ],
          );
        }

        return Scaffold(
          backgroundColor: Theme.of(context).cardColor,
          appBar: searching
              ? AppBar(
                  automaticallyImplyLeading: false,
                  title: PreferredSize(
                    preferredSize: Size.fromHeight(48),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 14),
                      child: TextField(
                        autocorrect: false,
                        autofocus: true,
                        onChanged: debounce((value) {
                          setState(() {
                            this.search = value;
                          });
                        }, 250),
                        style: TextStyle(color: Colors.white, fontSize: 16),
                        decoration: InputDecoration(
                          hintText: 'Enter an anime title',
                          hintStyle: TextStyle(color: Colors.white.withOpacity(0.6), fontSize: 16),
                          // border: InputBorder.none,
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                this.searching = false;
                                this.search = "";
                              });
                            },
                            icon: Icon(Icons.clear),
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ))
              : null,
          body: SafeArea(
            child: Stack(
              children: [
                listView,
                Visibility(
                    visible: result == null || result.loading == true,
                    child: Center(child: CircularProgressIndicator()))
              ],
            ),
          ),
          floatingActionButton: !this.searching
              ? FloatingActionButton(
                  onPressed: () {
                    setState(() {
                      searching = true;
                      Scrollable.of(sections.values.first.currentContext!)!.position.jumpTo(0);
                    });
                  },
                  child: const Icon(Icons.search),
                )
              : null,
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.remove_red_eye),
                label: 'Watching',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.next_plan),
                label: 'Planning',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.pause),
                label: 'Paused',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                label: 'Settings',
              ),
            ],
            currentIndex: this.currentSection,
            unselectedItemColor: Color.fromARGB(179, 255, 255, 255),
            selectedItemColor: Theme.of(context).accentColor,
            onTap: (int index) {
              if (index == 3) {
                openSettings(context);
                return;
              }
              final key = sections.values.toList()[index];
              Scrollable.ensureVisible(key.currentContext!, duration: Duration(milliseconds: 750));
            },
          ),
        );
      },
    );
  }
}
